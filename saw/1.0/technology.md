# Technology

- [Introduction](#introduction)
- [Backend Technologies](#backend_technologies)
    - [Laravel](#laravel)
    - [php Dependencies](#php_dependencies)
    - [RabbitMQ](#rabbitmq)
- [Frontend Technologies](#frontend_technologies)


## Introduction

The Simplified Analytics Workbench (SAW) was the first module in DCP, not relying on Angluar for Frontend and C# in Backend. Instead, SAW is utilizing Vue (v.3) for Frontend and php with the Laravel Framework for the Backend.


## Backend Technologies

SAW relies on php and builds on top of Laravel. Additionally, a messenging queue is used.


### Laravel

The system operates on top of [Laravel 10.x](https://laravel.com/docs/10.x) and requires:

* PHP 8.1+ with following extensions installed:
  (_To make the links bellow useful the resources.zip archive should be extracted to resources folder_)
    * [pdo_sqlsrv](https://pecl.php.net/package/sqlsrv) ([dll](resources/php_pdo_sqlsrv_81_ts_x64.dll)) - SQL Server
      connections support
    * oci8 ([dll](resources/php_oci8_19.dll)) - Oracle Database 19 connections support
    * [pdo_snowflake](https://docs.snowflake.com/en/developer-guide/php-pdo/php-pdo-driver) ([dll](resources/php_pdo_snowflake.dll), [cacert](resources/snowflake_cacert.pem)) -
      Snowflake
      connections support
        - extension=php_pdo_snowflake.dll
        - pdo_snowflake.cacert=path/to/snowflake_cacert.pem
    * pdo_pgsql - PostgreSQL connections support
    * pdo_mysql - MySQL connections support
    * [svm](https://pecl.php.net/package/svm) ([dll](resources/php_svm.dll)) - RubixML dependency
    * [tensor](https://github.com/takielias/php-tensor-for-windows/tree/main) ([dll](resources/php_tensor.dll), [libopenblas](resources/libopenblas.dll)) -
      RubixML dependency
* Apache/Nginx Server
* SQL Server
* Composer


### php Dependencies

The system requires the following composer packages:

* `php-amqplib/php-amqplib` - RabbitMQ support (dataset/mutation requests and core batch events subscriptions)
* `yajra/laravel-oci8` and `yoramdelangen/laravel-pdo-odbc` - Snwoflake support (connections)
* `rubix/ml` and `rubix/tensor` - RubixML support (mutations)
* `php-parallel-lint/php-parallel-lint` - PHP Scripts (mutations)
* `darkaonline/l5-swagger` - Swagger documentation
* `firebase/php-jwt` - JWT authorization
* `google/apiclient` - Google Sheets support (connections)
* `rtconner/laravel-tagging` - Tagging (connections, datasets)


### RabbitMQ

As the other modules of DCP, also SAW interacts with a central instance of RabbmitMQ.


## Frontend Technologies

Project is based on the [Vue 3](https://vuejs.org/)

The project having couple of main dependencies

- [Kendo UI for Vue](https://www.telerik.com/kendo-vue-ui/components/)
    * We are using a couple of components that Kendo provide. For example for all the grids in the application we are
      using [Kendo Grid](https://www.telerik.com/kendo-vue-ui/components/form/).
    * **Working with forms** - we are using [Kendo Forms](https://www.telerik.com/kendo-vue-ui/components/form/) and the
      way that
- [Vuetify](https://www.telerik.com/kendo-vue-ui/components/)
- [Axios](https://axios-http.com/docs/intro)
- [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
- [Vue3DraggableResizable](https://www.npmjs.com/package/vue3-draggable-resizable)
    * We are using **modified local version** of this library for the Analyses elements - drag/resize elements and
      formatting guidelines
- [VNetworkGraph](https://dash14.github.io/v-network-graph/)
    * Library is used for the Mutation Network and Automation graphs
- [MomentJS](https://momentjs.com/)
- [Lodash](https://lodash.com/)
- [Pinia](https://pinia.vuejs.org/)
    * Store Management tool for Vue 3
- [Plotly](https://plotly.com/javascript/)
    * Graphing library - mainly for the Analysis Charts
- [Vue3ContextMenu](https://github.com/imengyu/vue3-context-menu)
    * Used for displaying a custom context menu.
* [Vue Prism Editor](https://github.com/koca/vue-prism-editor/tree/feature/next)
  * Code editor library that we are using for our custom code editor component
* [VueTribute](https://www.npmjs.com/package/vue-tribute)
  * We are using a local version of this library. Its help us to work with constants across the application