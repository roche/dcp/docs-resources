# Connectivity

- [Introduction](#introduction)
- [Supported Connections](#suported_connections)
    - [MS SQL](#ms_sql)
    - [MySQL](#mysql)
    - [PostgreSQL](#postgresql)
    - [Oracle](#oracle)
    - [OSIsoft PI](#osisoft_pi)
    - [Google Sheets](#google_sheets)


## Introduction

The vitality of the SAW module highly depends on its connectivity. The more connections are supported out of the box, the easier it will become for users to load data and transform it into knowledge.


## Supported Connections

In the following, the supported connections are described as well as the required installation procedures.


### MSSQL (Microsoft SQL Server)

<img src="/assets/connections/ms_sql.png" width="30" />


### MySQL (MySQL & MariaDB)