# Getting Started

- [Introduction](#introduction)
- [Backend Installation](#backend_installation)
- [Backend Setup](#backend_setup)
    - [RabbitMQ Consumers](#rabbitmq_consumers)
    - [Laravel Queues](#laravel_queues)
    - [Crontab](#crontab)
- [Frontend Installation & Setup](#frontend_installation_and_setup)


## Introduction


## Backend Installation

Having a running server and PHP with all extensions installed browse to SAW sources folder and:

* Require Laravel and all dependencies, defined in composer.json and composer.lock files:

```
composer install
```

* Configure environment variables by copying the .env.example template to .env:

```
cp .env.example .env
``` 

and either adjust settings manually or run the command bellow to update them automatically.
In order this command to work you should possess and place a `hasicorp.pem` certificate in HASHICORP_VAULT_CERT_PATH
folder.

```
php artisan update:env
```

* Setup database - having a valid connection to SQL Server database, specified by `DB_*` environment variables, run the
  command bellow to sync all migrations defined in [migrations folder](../database/migrations/):

```
php artisan migrate
```

* Setup storage - in order uploaded resources to be made accessible by web - storage should be linked to public folder
  by executing:

```
php artisan storage:link
```


## Backend Setup

Once the above steps are completed and a working instance available all required background processes should also be configured to run behind the scenes:


### RabbitMQ Consumers

- Process all dataset requests
```
php artisan rmq:dataset
```

- Process all mutation requests
```
php artisan rmq:mutation
```

- Receive batch events from Core DCP
```
php artisan rmq:batch-events
```


### Laravel Queues

- Process all events from default queue:
```
php artisan queue:work
```

- Process all events from notifications queue:
```
php artisan queue:work --queue=notifications
```

- Process all events from automation queue:
```
php artisan queue:work --queue=automation
```


### Crontab

Crontab should also be setup to schedule processes defined in [Kernel](../app/Console/Kernel.php) class, like analyses automation, storage cleanup, etc.:

```
php artisan schedule:run
```