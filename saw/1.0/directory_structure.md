# Directory Structure

- [Introduction](#introduction)
- [Backend](#backend)
    - [Backend Structure](#backend_structure)
- [Frontend](#frontend)
    - [Frontend Structure](#frontend_structure)


## Introduction


## Backend


## Backend Structure

```
project_root
    ├── app
    │   ├── Console
    │   ├── Core
    │   ├── Events
    │   ├── Exceptions
    │   ├── Guard
    │   ├── Http
    │   │   ├── Controllers
    │   │   ├── Middleware
    │   │   └── Requests
    │   └── Jobs
    │   └── Listeners
    │   └── Models
    │   │   └── Base
    │   └── Providers
    └── config
    └── database
    └── public
    └── routes
    └── storage
    └── vendor
```

* **app/** - Default laravel application files
    * **app/Console** - Console commands (run by `php artisan ...`)
    * **app/Core** - Contains libraries to support business logic for SAW application (i.e. abstraction layers for
      Connections, Datasets, Mutations, Items, Notifications, etc. helpers/DTOs/services/contracts/etc.)
    * **app/Events** - All events triggered by SAW application and handled by Laravel queue
    * **app/Guard** - Handles user authorization
    * **app/Http** - Controllers, Requests, Middleware definitions, responsible for handling all requests and responses
        * **app/Http/Controllers** - Process requests defined in routes/
        * **app/Http/Middleware** - Provide a mechanism for inspecting and filtering HTTP requests entering your
          application.
        * **app/Http/Requests** - Validate request data before sending it to controllers.
    * **app/Jobs** - Definitions of jobs (similarly to Events) that need to be processed by Laravel queue
    * **app/Listeners** - Listeners of events defined in app/Events that are executed by Laravel queue
    * **app/Models** - Definition of all database models as defined by Eloquent specification. To prevent models
      becoming too big they are split in two parts - a Base model which describes the basic structure of a database
      table and a main model which adds additional methods and behaviour where needed.
    * **app/Providers** - Default Laravel service providers to configure booting and dependencies of SAW application.
* **config/** - Configuration files
* **routes/** - Default Laravel routes files. All endpoints are configured in `routes/api.php` file
* **storage/** - Storage folder (contains logs and resources such as uploaded files, datasets, etc.).
* **vendor/** - All external libraries, managed by composer


## Frontend


## Frontend Structure

````
project_root
├───src
│   ├───analyses
│   ├───analysis-constants
│   ├───analysis-items
│   │   ├───editors
│   │   ├───elements
│   │   ├───info-icon
│   │   ├───item-comments
│   │   ├───item-comments-btn
│   │   ├───item-editor
│   │   └───trace-editor
│   ├───application-view
│   ├───applications
│   ├───assets
│   ├───automation
│   ├───connections
│   ├───datasets
│   ├───events
│   ├───i18n
│   ├───libs
│   ├───mutations
│   ├───plugins
│   ├───router
│   ├───saw-administration
│   ├───shared
│   ├───store
│   └───views
└───tests
````
* **src/** main code folder
    *  [analysis](modules/01_analyses.md) - All components used to Preview/Create/Update/Share/Manage Access and setup different settings for the analyses
    *  [analysis-constants](modules/05_constants.md) - Each analysis contains group of constants that we can add/remove and use across the analysis.
    *  **analysis-items** - The folder contains all elements used in the analysis alongside with their editors ( settings components)
    *  **application-view** - Contains all the components used to represent an Application
    *  [application](modules/08_applications.md) - All the logic and components needed to set up an Application is in this folder.
    *  **assets** - DCP Assets Submodule
    *  [automation](modules/07_automation.md) - It`s part of the analysis and its represent the Datasets relationships and their recalculation settings
    *  [connections](modules/02_connections.md) - Components used to create/edit connections
    *  [datasets](modules/03_datasets.md) - Contains components that are responsible for listing,editing and requesting a dataset. Also operations related with dataset columns and displaying dataset changes.
    *  [events](modules/06_events.md) - Events are related with the datasets too. Components for set/edit and list are placed here.
    *  [i18](04_translations.md) - Folder holds the project specific translations
    *  **libs** - All submodules are placed here.
    *  [mutations](modules/04_mutations.md) - Mutations are related with a particular Analysis and this analysis datasets. All the components that are used for configure,edit and display mutations are placed here.
    *  **plugins** - Folder for plugins
    *  **router** - Here we are storing the main file responsible for the routing
    *  **saw-administration** - SAW Administration module
    *  **shared** - This folder contains components that are shared across the modules in the applications. Also contain helpers and some base components that we are extending across the application.
    *  **view** - Contains the entry points for some of the routes ( pages )
* tests - folder containing tests
***