# Expanding Capabilities

- [Introduction](#introduction)
- [Create new Connection](#create-new-connection)
- [ToDos](#todos)

## Introduction

In this section, we embark on a journey of empowerment, guiding you through the process of expanding the capabilities of DCP or a module. As technology evolves and user needs grow, the ability to customize and add new features becomes paramount. Here, we provide a roadmap for you to unlock the full potential of the tool by seamlessly integrating new functionalities. Delve into the art of innovation and start shaping the future of your tool.

## Create new Connection

In order to define a new connector a new class extending Connector should be placed in Connectors folder. Class name should end with the suffix Connector. A new image icon corresponding to the new connector should be placed in `public/images/connectors` folder.

```php

<?php

namespace App\Core\Connectors;

use App\Core\Definitions\TypeCasts;

class TestConnector extends Connector
{
    /**
     * Here you should define all parameters needed to connect the remote source.
     * These fields would be presented while creating new connection using this connector
     */
    protected array $parameter = [
        'username' => [
            'type'     => 'string',
            'optional' => false,
        ],
        'password' => [
            'type'     => 'password',
            'optional' => false,
            'hide'     => true,
        ],
    ];

    /**
     * Define all possible steps when requesting a dataset from this connector
     */
    protected array $navigatorLevels = [
        'spreadsheets' => [
            'details' => null,
            'title'   => 'Spreadsheets',
            'next'    => 'sheets',
        ],
        'sheets'       => [
            'title'   => 'Sheets',
            'details' => ['spreadsheet'],
            'next'    => 'columns',
        ],
        'columns'      => [
            'title'   => 'Columns',
            'details' => ['spreadsheet', 'sheet'],
            'next'    => 'COMPLETE',
        ],
    ];

    public function establish(): bool
    {
        /**
         * Here you should place some logic to establish connection to the remote source, if needed
         * Return true on success/false on failure
         */
        return true;
    }

    public function data(array $messageData): array
    {
        /**
         * Here you should pull data from the remote source using filters defined by $messageData
         * Return dataset array on success, throw exception on failure.
         *
         * Gathered data should fit to the example structure bellow,
         * where head property defines a list of columns with given type and name
         * while body brings all the records (each record is list with same length as defined columns)
         */
        return [
            'head' => [
                [
                    'name' => 'Column1',
                    'type' => TypeCasts::INTEGER->value
                ],
                [
                    'name' => 'Column2',
                    'type' => TypeCasts::TEXT->value
                ]
            ],
            'body' => [
                [1, 'Example text 1'],
                [2, 'Example text 2'],
            ],
        ];
    }

    public function navigator(string $level, array $navigatorDetails = []): array
    {
        /**
         * For examples, see other implementations
         *
         * The navigator is used when requesting a dataset via connection that uses this connector.
         * Each step defines custom fields for user input which depend on previous selections.
         */
        return [];
    }

    public function prepareRequestMessageData(array $fullList): array
    {
        /**
         * Sanitize user input while defining a connection.
         * Message data is stored in datasets.query_definition db field and is later on passed to data() method above to fetch the data
         */
        return [];
    }

    public function getType(): string
    {
        /**
         * Connection type, tag
         * Could be used to filter connections by type i.e. GET api/analyses/:analysisId/connections?type=sql
         */
        return 'rest';
    }
}
```

## ToDos
- [ ] Messaging formats 
- [ ] Adding new mutation operation 