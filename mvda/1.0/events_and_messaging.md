# Event Lifecycle

- [Introduction](#introduction)
- [Active events](#active-events)
- [Event Bus](#event-bus)
- [Clean Up Service](#clean-up-service)
- [Internal Events](#internal-events)

## Introduction
Following the general pattern of events in DCP, the MVDA module listens to two types of asynchronous events in DCP:

* Batch subscription (from the external data source) marking when something "interesting" happens. The definition is made by SME on the external datasource and could capture a specific unit operation, a phase, a certain temperature range, etc.
* User interactions in framework which needs/can be implemented in the MVDA module/database as well (e.g. site deletion)

The section gives an overview of the message subscriptions and internal messages omitted by the DCP MVDA module. It does not implement any additional queue or exchangers which should be consumed by other modules.

## Detection of currently active events

> {info} For historic reasons the terminology between batch and events might be misleading. DCP uses them interchangeable. A batch (or event) is defining a time context, when something happens. Moreover, a batch can be identified by a unique Id, and may be further described by meta data attributes. There is no direct relation to a manufacturing batch, as the definition can be applied for batches, specific operations, phases, process steps, etc.

Event are observed by DCP Framework and broadcasted via Messaging Que to the Modules. MVDA subscribes to the following messages: Batch [Start](/framework/1.0/event_lifecycle#messages) and [End](/framework/1.0/event_lifecycle#messages) and [Value change](/framework/1.0/event_lifecycle#messages) messages. In order to receive those messages, MVDA signs up for batch events based on model devices. A model device is added to the signup if any of the following condition is fulfilled: 

* a user configured dashboard tile is present
* a notification is configured
* a [model output vector]({{section}}/{{version}}/mathematical_models#Model-vectors) for the running batch is calculated and the intermediate result saved in the database

The MVDA service binds with the *MVDA* header to the *ActiveBatchExchanger*. The logic for exposing a device (which is equivalent for subscribing to events from the device) is placed in the *BatchEventController*.

The messages are received by binding the *MVDACacheServiceQueue* to the Exchanger. The logic for handling events can be found in the *CacheHandlerWorker* which is deployed as a Windows service.

While an event is active (no defined end) it is reflected/living in the signup table. An event can be evaluated by multiple models and/or multiple output types per model. A detailed description of the real-time data cache can be found in the [caching]({{section}}/{{version}}/caching) section. The different states an event can have within the DCP module and the transition conditions are illustrated below:


```mermaid
stateDiagram
    [*] --> Running
    Running --> Pending: BatchEnd Event

    Pending -->  TrueNegative : Recalculation - violations below threshold
    TrueNegative --> [*]
    TrueNegative --> FalseNegative: User overwrite
    FalseNegative --> [*]

    Pending -->  Open : Recalculation - violations above threshold
    Open --> InProgress: User comment
    InProgress --> TruePositive: User overwrite 
    InProgress --> FalsePositive: User overwrite 
    Open --> FalsePositive: User overwrite 
    TruePositive --> [*]
    FalsePositive --> [*]
```


> {info} the term negative/positive  inside the MVDA module is used in the *Testing perspective* - where the term *positive* refers to the presence of an artifact and the term *negative* to the absence of an artifact. Translated to the MVDA context an artifact is a statistical outlier while negative refers to an event being from the same distribution as the modelling dataset.

## Event bus

The MVDA module subscribed to the following events - distributed by the DCP Framework.

* messages for [importing](/framework/1.0/dashboard#import-and-export), [updating](/framework/1.0/dashboard#updating-the-layout) or [deleting](/framework/1.0/dashboard#deleting-a-dashboard-tab) a dashboard  
* messages for the detection of a [deleted device](/framework/1.0/events_and_messaging#event-bus) or a [deleted sensor](/framework/1.0/events_and_messaging#event-bus)
* messages for a [deleted site](/framework/1.0/administration#site-management)

While the processing of the dashboard related messages is straight foreword and only persisting state in the database, handling the deletion  scenarios is more complicated. In cases of the detection of a deleted sensor or device all related entities which could create a batch signUp (dashboards, notifications, etc.) are deleted. This is required to stop a flood in the error logs of the service layer. Deletion is based on models where the sensor/device is used by the model, as a single missing sensor leads to wrong calculations of all model outputs. The MVDA model is not delete, so that it can be edited/updated by the user.

## Clean Up Service

The clean up service has the single responsibility for cleaning orphaned records in order to comply with core map requirements. The service executes timed loops, so that different record types can be deleted using a different frequency. The clean-up service uses the Worker service template from dotnet and is deployed as a windows service.

The following records are deleted:

| Records             | Conditions                     | Execution Frequency |
|---------------------|--------------------------------|---------------------|
| Datasets            | not linked to any workset      | daily               |

## Internal events

> {danger} The messages in this section should not be used for any subscription in other modules, there is no guarantee for format or other changes during the deployment lifecycle.

The MVDA module has a single internal event, which is emitted on the release of a sign up, derived from a batch level model (BLM). The message is sent via a direct publish (no routing, etc.) into the *BLMNotificaionsQueue*. Messaging is used at this place to decouple cache and notification worker, utilizing the single responsibility pattern. The message has the following format:

```json
{
    "BatchId": "TestBatch",
    "CalculationType": 1,
    "Tags": [
        {
            "TagName": "Comp",
            "TagValue": "1",
            "TagType": 1
        }
    ],
    "DataPoint": {
        "XValue": 91313,
        "YValue": 1234,
        "ObsId": "781e55a3-833f-11ec-90fd-005056a7fed6",
        "DistanceMetric" : 0.12434,
        "SpecialValueType" : null
    },
    "EventFrameId" : "781e55a3-833f-11ec-90fd-005056a7fed6",
    "ModelId" : 1,
    "BatchStartTime": "2024-02-26T12:05:44Z",
    "IsGxp": true
}
```

The properties of the message are explained below:

| Field           | Datatype  | Description |
|-----------------| ----------|-------------|
| EventFrameId    | String    | The unique identifier of the event as specified on the data source|
| BatchStartTime  | String    | The start time of the event in UTC time (ISO-8601 format) |
| BatchId         | String    | The display Id used for identification by the user *(not guaranteed to be unique)* |
| ModelId         | Integer   | The modelId used for the calculation  |
| IsGxp           | Boolean   | Indicator whether this message was emitted by a GxP source |
| CalculationType | Byte      | The calculation type from the mathematical model |
| Tags            | Object[]  | The tags further describing the mathematical calculations |
| DataPoint       | Object    | The datapoint, containing the result of the mathematical calculation (avoid resource intensive calculations) |