# Notifications

- [Introduction](#introduction)
- [Batch Level Notifications](#batch-level-notifications)
- [Batch Evolution Notifications](#batch-evolution-notifications)
- [UI Flow](#ui-flow)
- [Records](#records)


## Introduction
The MVDA module implements notifications. The notification behavior differs mainly based on the model class. Notifications for **B**atch **L**evel **M**odel (BLM) are checked once (when the sign-up is released). Opposite notifications on **B**atch **E**volution **M**odels (BEM) are constantly checked while the signup is active.

## Batch Level Notifications
As the values for batch level models can be calculated only at the end of the batch, notifications need to be checked only once (at the end of the batch). This is implemented by hooking into the release process of the [cache service]({{section}}/{{version}}/caching). This process emits a **BLMNotificationCheckRequest** message. The message format can be found in the [messaging section]({{section}}/{{version}}/events_and_messaging).

All notifications which need to be checked are loaded into memory and grouped by the modelId, calculationType and the model output tags.  Inside the loop the [BLMModelFactory]({{section}}/{{version}}/mathematical_models) is used to resolve to the model type specific model. The method `CheckBatchViolations` of the `IBLMModelInterface` is used to check weather a data point is within the model specification. This is normally done comparing the DistanceMetric property against the defined threshold (see below). If the values exceeds the thresholds a notification is triggered.

```mermaid
stateDiagram-v2
    s1: load notifications
    s2: Check message data-point
    s3: Send Notification


    [*] --> s1: BLMNotificationMessage
    s1 --> s2
    s2 --> if_state
    if_state --> s3: if n < threshold
    if_state --> [*] : if n >=  threshold
    s3 --> [*]
```

Triggering a notifications starts:

*  the share link generation
* rendering of the data preview image using the calculation engine
* populating the e-mail template

All information are rendered and then send as `EMailDTO` message to the [e-mail processor service](/framework/1.0/user_messaging), which will deliver the messages to the users via the configured message channel. 

## Batch Evolution Notifications
BEM-notifications heavily relays on the [cache service]({{section}}/{{version}}/cache) to check ongoing manufacturing batches/events against the model conditions. All active notifications are gathered as a sign up source. Active batches are detected by the [Active Batch Service](/framework/1.0/event_lifecyle), active events will be cached by the MVDA [real time cache]({{section}}/{{version}}/cache). The notification service loop then is pretty simple, and is illustrated in the diagram below:

```mermaid
stateDiagram-v2
    s1: load notifications
    s2: evaluate key metrics
    s3: Check Against Notification Thresholds
    s4: Send Notification
    s5: Update Notification Status Check

    state if_state <<choice>>
    [*] --> s1
    s1 --> s2
    s2 --> s3
    s3 --> if_state
    if_state --> s5: if n < threshold
    if_state --> s4 : if n >=  threshold
    s4 --> s5
    s5 --> s1
```

Notifications are based on sign ups therefore batch conditions do not need to be evaluated, as this is already done in the cache service. In the first step the notifications to be checked. All notifications which need to be checked are loaded into memory and grouped by the modelId, calculationType and the model output tags. For each matching signup the cached observations are loaded as a list of `ExtendedBEMDataPoint` into memory. Inside the loop the [BEMModelFactory]({{section}}/{{version}}/mathematical_models) is used to resolve to the model type specific model. The method `CheckDataPointViolationsAsync` of the `IBEMModelInterface` is used to check weather a data point is within the model specification. This is normally done comparing the DistanceMetric property against the defined threshold. 

> {info} The DistanceMetric property is a model limit setting independent measure on how far an observation is away from the model expected range. This has the advantage that intense calculations only need to be performed once and can be re-used to compare against multiple different thresholds to determine if a data point is inside the limit specification.

After the evaluation if a data point is inside the model limit specification defined for the notification the key metrics total number of violations and consecutive number of violations are gathered. The metrics are compared against the thresholds defined in the notification settings. If the values exceeds the thresholds a notification is triggered.

Triggering a notifications starts:

*  the share link generation
* rendering of the data preview image using the calculation engine
* populating the e-mail template

all information are rendered and then send as `EMailDTO` message to the [e-mail processor service](/framework/1.0/user_messaging), who will deliver the messages to the users via the configured message channel. 

## UI Flow
As described in the notification section of the DCP framework, MVDA notifications are integrated into the system using the aggregator concept at the gateway layer. The MVDA backend provides the required endpoints, `notification/options` and `notification/list`, which are mapped in the internal gateway aggregator configuration to be displayed in the notification list view. All edit screens are implemented within the MVDA microfrontend.


## Notification Records

```mermaid
erDiagram

Notification {
 uniqueidentifier ID ""
 nvarchar Title "The name of the notification"
 nvarchar Description "The description of the notification"
 tinyint Type "The notification type description"
 nvarchar Filter "The global filter to the device where the model is linked to"
 tinyint Status "Is the notification currently being checked"
 datetime2 ActivatedAt "The timestamp in UTC - when the notification has been activated"
 nvarchar DeviceWebId "The equipment identifier on the datasource"
 int SiteId ""
 tinyint NotificationContext ""
 tinyint NotifcationCreationType ""
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}

NotificationModel {
 int ID ""
 uniqueidentifier NotificationId "FK to the notification table containing the meta data"
 int ModelID "The linked model from which the calculation was based on"
 tinyint CalculationType "The calculation type - describing the type of the model output"
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}
NotificationSubscribers {
int ID ""
 uniqueidentifier NotificationId "The notification to be received"
 int UserId "UserId of the user receiving the notification"
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}

NotificationModelCheckStatus {
 int ID ""
 int NotificationModelId "The notification to be checked"
 datetime2 LastCheckAt "The timestamp in UTC when the notification check was last performed"
}

NotificationModelTags {
 int ID ""
 int NotificationModelID ""
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 nvarchar TagName "The name/identifier of the tag - describing the model output"
 nvarchar TagValue "The value of the tag - describing the model output"
 tinyint TagType "The type of the tag (calculation or limit related) - describing the model output"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}

Notification ||--|| NotificationModel: has
NotificationModel ||--|| NotificationModelCheckStatus: "stores state"
NotificationModel ||--o{ NotificationModelTags: "described by"
Notification ||--|{ NotificationSubscribers: "notifies"
```


**Records classification and audit trail**

For the NotificationModelCheckStatus table:

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  The state of the notification, based on active sign ups  |
|Data classification    |  Cache only                                   |
|Change Tracking        |  No                                           |
|Audit Trail            |  No                                           |
|Retention period       | N/A                                           |

For all other notification tables:

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  State and definitions of mvda notifications                                                           |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |
