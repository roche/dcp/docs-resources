# Directory Structure

- [Introduction](#introduction)
- [Backend](#backend)
    - [Backend Structure](#backend-structure)
- [Frontend](#frontend)
    - [Frontend Structure](#frontend-structure)


## Introduction
The directory structure below gives an overview for navigating around the project. Both frontend
and backend follow the DCP standard patterns.

## Backend
The backend structure used for DCP is based on the [.NET webAPI template](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-new-sdk-templates#webapi) for the API project. The shared DCP functionality is imported using the DCP.Framework Nuget package. The projects for worker services are based on the [worker template](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-new-sdk-templates#web-others). 

## Backend Structure

```
project_root
 |- DCP.MVDA.API
 |  |- Controllers
 |  |- Program.cs
 |  |- StartUp.cs
 |- DCP.MVDA.Constants
 |- DCP.MVDA.Core
 |  |- Factories
 |  |- Interfaces
 |  |- Services
 |  |- Configuration
 |- DCP.MVDA.DAL
 |  |- Entities
 |  |- Interfaces
 |  |- Migrations
 |  |- DbContext.cs
 |- DCP.MVDA.DataSources
 |  |- Datasources
 |  |- Interfaces
 |- DCP.MVDA.Models
 |- DCP.MVDA.WorkerService.EventBus
 |  |- Program.cs
 |  \- Worker.cs
 |- DCP.MVDA.WorkerService.Notifications
 |- DCP.MVDA.WorkerService.CleanUp
 |  |- Program.cs
 |  \- Worker.cs
 |- DCP.MVDA.WorkerService.Cache
 \- DCP.MVDA.WorkerService.BatchDatabase
    |- Program.cs
    \- Worker.cs
```

* **API Folder** - This contains the C# webAPI project used/consumed by the frontend or the background worker services.
* **Constants Folder** - 	The collection of module specific string constant/enumerations.
* **Core Folder** - This contains the interfaces and services implementing the business logic of the module. 
* **DAL Folder** - This is the section containing the data access layer based on the Entity Framework. The code first pattern is used, see section [data design]({{section}}/{{version}}/data_storage) for entity relationship diagrams.
* **DataSources Folder** - 	In case of dedicated data supply needed by the module, this section defines the interface and the interface implementations for the data type.
* **Models/** - 	This is the section containing the data transfer objects (DTOs) used for data transfer between the subroutines.
* **DCP.MVDA.WorkerService.EventBus Folder** - This is the worker service implementing the event subscription in the module.
* **DCP.MVDA.WorkerService.Notifications Folder** - This is the worker service implementing the MVDA notifications
* **DCP.MVDA.WorkerService.CleanUp Folder** - This is the worker service implementing the cleaning of orphaned/obsolete records
* **DCP.MVDA.WorkerService.Cache Folder** - This is the worker service implements the real-time caching logic
* **DCP.MVDA.WorkerService.BatchDatabase Folder** - this service handles the calculations and checks at the end of an event moving it into the batch database



## Frontend

## Frontend Structure

The application is built based on the AngularCLI template. In addition to the base template a few folders and files required by the single-SPA library are added (`./extra-webpack.config.js`, `./src/single-spa`, `./src/main.single-spa.ts` and `./src/app/empty-route`).

Frontend modules are configured as git submodules inside the applications. 
Except for the asset module, all others are installed inside `./src/libs/` folder. 

````
project_root
 \- src
    |- app
    |  |- Module
    |  |  |- Module.component
    |  |  |- store
    |  |  |- Module.service
    |  |  |- Module.module
    |  |  \- Module.routing
    |  |- app.component
    |  |- app.module
    |  \- app.routing
    |- assets
    |- libs
    |  |- dc-shared
    |  |  \- ...
    |  \- dc-filter
    |     \- ...
    |- single-spa
    \- translations
````
The **src** folder is the main code folder and contains:

  *  **app** folder - Contains all the components used to represent an Application
  *  **assets** folder - Contains DC Assets library that is used by all applications for styling purposes and shared translations
  *  **environment** folder - Contains all environment files 
  *  **libs** folder - Contains all used libraries like DcShared, DcFilter etc.
  *  **translations** folder - Contains translations files that are used only by current application

Inside `./src/app`  the main code for the application like routing and pages is located. In order to use AngularCLI 
App Module, App Component and App Routing files are always present.

The application is divided into angular modules. Each angular module has its own folder. The structure of folders is the same for most of 
the angular modules. The only difference is the presence of the store folder. If the angular module uses NGRX, the store folder is present.

The MVDA module uses a side menu and the layout Angular module, there is a layout folder, that contains `layout.MVDA.module.ts` and related files that extend the Base Components from the layout Angular module. 

> {warning} In all Applications AppModule, LayoutModule, LayoutRoutingModule and LayoutComponents need to have unique names like AppMvdaModule, LayoutMvdaModule etc. and for App and Layout Components also unique selector (selector: 'app-mvda-root'), in order to be sure that SingleSPA will not load the wrong file during the switch between applications on the production server.

All other files are organized in Angular Module folders, are separated logically or based on entities structure. Inside Angular module folders all files that are related to this Angular module like entity.module, entity.service, entity-routing.module, Components, Models and NGRX Module, if applicable, can be found.

The MVDA Module contains NGRX files inside the `./store` folder. In the same folder files for Actions, Effects, Reducers, State, Facade and Entity or Logical Structure Store Module (like BatchStoreModule or DashboardStoreModule) are present.
