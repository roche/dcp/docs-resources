# Dashboard
- [Introduction](#introduction)
- [Implementation](#implementation)
- [Creating and Editing](#creating-and-editing)
- [Records](#records)


## Introduction
The MVDA module provides some dashboard tiles. The tiles are imported using the [entity aggregator](/framework/1.0/dashboard#entity-aggregator). MVDA dashboards data are based on the data populated by the [cache service](/{{section}}/{{version}}/cache). Loading the dashboard titles is separated into two independent phases, as described in the entity aggregator concept by the DCP framework.

* the layout call contains the settings of the tile and all model limits (which are static)  
* for ongoing batches the calculated model output vectors and the resulting violations are fetched from the real-time cache and added to the presentation using the batch loading strategy.

Both phases are always grouped based on the dashboard tab selected by the user. Disabled batches are not pushed to the user browser.

## Implementation

The MVDA Dashboard module empowers users to craft dashboard tiles, designed for integration across multiple dashboards. 
When users initiate the creation or modification of an MVDA presentation within the [Framework dashboard](framework/1.0/dashboard), they are seamlessly redirected to the MVDA Dashboard module. Here, a form is presented, enabling users to configure various aspects of the dashboard tile, including its Name, Description, Model, and Model Configuration.

Furthermore, the dashboard incorporates an independent component, termed the [ModelPresentationViewComponent](core_concepts/1.0/web_components#dashboard-model-presentation), which plays a crucial role in its functionality.

## Creating and Editing

To initiate the creation of a model presentation, the form dispatches a request to the `mvda/Dashboard/CreateModel` endpoint. 
Conversely, to modify an existing model presentation, the form targets the `mvda/Dashboard/UpdateModel` endpoint.

The format for these requests is as follows:
```jsx
   {
    ID: "" // Sent only when updating a presentation
    "Title": "",
        "Description": "",
        "DeviceWebId": "",
        "Type": 0,
        "WebIdsLinksToCurrentDevice":[],
        "Configuration":{},
    "ModelID": 0,
        "DashboardTabId": "",
        "GridLayout":{"rowStart":"1","columnStart":1,"columnEnd":"span 4"}
    }
   ```

## Dashboard Records

```mermaid
erDiagram

Dashboard {
 uniqueidentifier ID ""
 nvarchar Title "The title of the dashboard tile"
 nvarchar Description "The description of the dashboard tile"
 tinyint Type ""
 nvarchar DeviceWebId "The equipment identifier on the data source"
 uniqueidentifier DashboardTabId "The dashboard tab where the tile is placed on"
 nvarchar Filter "The global filter to the device where the model is linked to"
 nvarchar Layout ""
 int Version "The version number of the dashboard element"
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}

DashboardModel {
 int ID ""
 uniqueidentifier DashboardId ""
 int ModelID "The linked model from which the calculation was based on"
 tinyint CalculationType "The calculation type - describing the type of the model output"
 datetime2 Reset ""
 int Version "The linked model version from which the calculation was based on"
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
}

DashboardModelTags {
 int ID ""
 int DashboardModelID ""
 int LastModifiedBy "The userId, who performed the last change - used for audit trail"
 int OwnerUserId "The userId, who is owning the record - may have special privileges"
 datetime2 SysEndTime ""
 datetime2 SysStartTime ""
 nvarchar TagName "The name/identifier of the tag - describing the model output"
 nvarchar TagValue "The value of the tag - describing the model output"
 tinyint TagType "The type of the tag (calculation or limit related) - describing the model output"
}

Dashboard||--|| DashboardModel: has
DashboardModel ||--o{ DashboardModelTags: "described by"


```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  State and definitions of mvda dashboard tiles                                                        |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |
