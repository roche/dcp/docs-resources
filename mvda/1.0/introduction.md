# Introduction
- [Introduction](#introduction)
- [Technology](#technology)
    - [Backend Technologies](#backend-technologies)
    - [Frontend Technologies](#frontend-technologies)
    - [Analytics Technologies](analytics-technologies)

## Introduction
The MVDA module is an addition to the Data Computation Platform (DCP) and introduces multivariate statistical methods for process monitoring to DCP. The MVDA module is focused on batch process data which is analyzed by applying PLS and PCA to the datasets. 

## Technology

In the current implementation MVDA is based on .NET (v. 8) in the backend and Angular (v.11) in the frontend.


### Backend Technologies

MVDA relies on .NET and builds on top of the [ASPNET Core MVC](https://learn.microsoft.com/en-us/aspnet/core/mvc/overview?view=aspnetcore-8.0) template for providing an API. Services are implemented using the [Background Worker]() as a base template. As for DCP framework the MVDA module is using a Message Broker for module integration and a Secret Configuration Provider.


**.NET**
The system operates on top of [.NET 8.0](https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-8) and requires:


* [DCP.Framework](https://gitlab.com/digitalclone/platform/be2/dcp-framework) the package providing the shared functionality e.g. models, etc.
* [Entity Framework Core](https://docs.microsoft.com/ef/core/) as an ORM utilizing the code first pattern for interacting with the database layer
* [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) to provide developer documentation
* [MathNET](https://numerics.mathdotnet.com/) for the minimal set of statistical calculation performed in the backend layer
* [Serilog](https://github.com/serilog/serilog-aspnetcore) for writing to the different log channels - log channels can be adjusted via configuration.

**Message Broker**

As the other modules of DCP, also MVDA interacts with a central instance of a [Message Broker](/framework/1.0/events_and_messaging). The Message Broker functionality is encapsulated in an abstraction layer provided by the *DCP.Framework* NuGet package. The default registered MessageBroker instance is utilizing [RabbitMQ](https://rabbitmq.com/). In cases the message broker should be replaced - the interface needs to be implemented and the component registration via dependency injection should be changed.

\newpage

**Secret configuration storage**

As the other modules of DCP, also MVDA interacts with a central secret configuration storage. This functionality is encapsulated in an abstraction layer provided by the *DCP.Framework* NuGet package. The default registered SecretConfigurationStorage instance is utilizing [Hashicorp Vault](https://www.vaultproject.io/). In cases the vault should be replaced - the interface needs to be implemented and the component registration via dependency injection should be changed. The replacement could use any kind of storage (file based or other providers e.g. Azure Key-Vault).


### Frontend Technologies

The project is based on [Angular 11](https://angular.io/) as a framework, [NGRX](https://ngrx.io/) as a state manager and as UI libraries use 
[Kendo UI](https://www.telerik.com/kendo-angular-ui/) and [Angular Material](https://material.angular.io/).

The project has a couple of main dependencies:

**NPM libraries**

MVDA as many of the others DCP Frontend Applications is using [NPM](https://www.npmjs.com/) as a package manager. 
The main dependencies are:

* [@angular/material](https://material.angular.io/) - Angular Material UI components
* [@angular/router](https://angular.io/api/router) - Angular Router
* [@ngrx/store](https://ngrx.io/guide/store) - Angular state management library
* [@ngrx/effects](https://ngrx.io/guide/effects) - Angular side effects library
* [@ngx-translate/core](https://github.com/ngx-translate/core) - Angular internationalization library
* [@progress/kendo-ui](https://www.telerik.com/kendo-angular-ui/) - Kendo UI components
* [bootstrap](https://getbootstrap.com/) - CSS framework
* [moment](https://momentjs.com/) - Date and time manipulation library
* [lodash](https://lodash.com/) - JavaScript utility library
* [rxjs](https://rxjs.dev/) - Reactive programming library
* [single-spa](https://single-spa.js.org/) - Micro frontend library
* [plotly.js](https://plotly.com/javascript/) - JavaScript charting library

**Web Components**

MVDA is using couple of web components, also used by other DCP Frontend Applications:

* [Time Range Picker](/framework/1.0/web_components#time-range) - ready to use time range Web Component
* [Rich Text Editor](/framework/1.0/web_components#quill-text-editor) - ready to use rich text editor Web Component
* [User Banners](/framework/1.0/web_components#user-banners) - ready to use user info banners Web Component

**Frontend modules**

The shared logic between DCP Frontend Applications are separated in a couple of modules, 
that are used as git submodules inside the applications. Here is the list of used modules:

* [Asset](/framework/1.0/frontend_modules#assets) - Library that contains  styles, images, translations and fonts
* [App](/framework/1.0/frontend_modules#app) - Library that contains the base application component
* [Batch](/framework/1.0/frontend_modules#batch) - Library that contains the batch related components and logic
* [Core](/framework/1.0/frontend_modules#core) - Library that contains the core config, services and interceptors
* [Filter](/framework/1.0/frontend_modules#filter) - Library that contains the global filter related components and logic
* [GXP Info](/framework/1.0/frontend_modules#gxp-info) - Library that contains the GXP info components and logic
* [Layout](/framework/1.0/frontend_modules#layout) - Library that contains the base layout components
* [Pages](/framework/1.0/frontend_modules#pages) - Library that in MVDA is used only for local development login
* [Sensor](/framework/1.0/frontend_modules#sensor) - Library that contains some sensor related components and logic
* [Shared](/framework/1.0/frontend_modules#shared) - Library that contains shared components, models, services and utils
* [Site Store](/framework/1.0/frontend_modules#site-store) - Library that contains the sites NGRX store
* [Store](/framework/1.0/frontend_modules#store) - Library that contains the global NGRX store
* [User](/framework/1.0/frontend_modules#user) - Library that contains the user related components and logic

### Analytics Technologies

MVDA uses the Analytical Core of Digital Clone ([AC-DC](/calculation_engine/1.0/introduction)) to perform mathematical calculations. The acdcMvda package implements all statistical methods (e.g. Graham-Schmid normalization, NIPLAS during the fit from scratch without external dependencies). However the package relies on some DCP internal packages:

* [acdcDataFactory](/core_concepts/1.0/data_integration) is used to access the third party data sources
* [acdcClient](/calculation_engine/1.0/introdcution) is an imported dependency used for accessing credentials and cache configuration
* [acdcReport](/core_concepts/1.0/data_integration) is used for report generation and formatting (uses [rmarkdown](https://rmarkdown.rstudio.com/lesson-1.html), [LATEX](https://www.latex-project.org/) and [pandoc](https://pandoc.org/) internally)

Other important third party packages used:

* [plotly](https://plotly.com/r/) for generating the plotly configuration strings - used for interactive charting (e.g. contribution plot)
* [ggplot](https://ggplot2.tidyverse.org/) for static image generation
* [dplyr](https://dplyr.tidyverse.org/) for data manipulation in the tidyverse syntax using [magrittr](https://magrittr.tidyverse.org/) pipes
