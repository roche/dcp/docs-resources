# Installation and setup
- [Introduction](#introduction)
- [R packages](#r-packages)
- [Backend](#backend)
    - [Windows Services](#windows-services)
    - [API](#api)
    - [Virtual Directories](#virtual-directories)
- [Frontend](#frontend)
    - [Micro Frontend](#micro-frontends)
    - [Web components](#web-components)
- [Module specific steps](#module-specifics-steps)

## Introduction
MVDA uses the [standard installation path](/framework/1.0/installation_and_setup), described in the DCP Framework. For the variables use the sections below.

## R packages

DCP MVDA functionality is implemented in a single package, which depends on other acdc packages which are installed via the package dependencies.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| packageName                   | acdcMvda                                 |


## Backend

### Windows Services

**Batch Database Service**
This service is used to transfer and recalculate batches in the batch database.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | MVDABatchDatabaseDCPService              |
| ModuleName                    | MVDA                                     |
| FolderName                    | BatchDatabaseService                     |
| DisplayName                   | DCP MVDA BatchDatabase Service           |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.MVDA.WorkerService.BatchDatabase.exe |

**Cache Service**
This service implements the real-time cache layer used by notifications, dashboards, etc.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | MVDACacheDCPService                      |
| ModuleName                    | MVDA                                     |
| FolderName                    | CacheService                             |
| DisplayName                   | DCP MVDA Cache Service                   |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.MVDA.WorkerService.Cache.exe         |


**Clean Up Service**
This service implements the information compliance requirements to remove old records.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | MVDACleanUpDCPService                    |
| ModuleName                    | MVDA                                     |
| FolderName                    | CleanUpService                           |
| DisplayName                   | DCP MVDA CleanUp Service                 |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.MVDA.WorkerService.CleanUp.exe       |

**Event Bus Service**
This service is used to wire against the events emitted from dcp framework and have an implementation in the MVDA module.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | MVDAEventBusDCPService                   |
| ModuleName                    | MVDA                                     |
| FolderName                    | EventBusService                          |
| DisplayName                   | DCP MVDA Event Bus Service               |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.MVDA.WorkerService.EventBus.exe      |

**Notifications Service**
This service is used to check real-time batches/events against the user specification and informs on model violations via e-mail.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | MVDANotificationsDCPService              |
| ModuleName                    | MVDA                                     |
| FolderName                    | NotificationsService                     |
| DisplayName                   | DCP MVDA Notifications Service           |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.MVDA.WorkerService.Notifications.exe |

### API
The MVDA uses the .NET Core API path, all module specific functionality is provided by a single IIS site:

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | MVDA                                     |
| ModuleName                    | MVDA                                     |
| HTTP-Port                     | 8088                                     |
| HTTPS-Port                    | 8188                                     |
| NET CLR Version               | No managed code                          |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-gxp.domain.com, for prod dcp-gxp.domain.com   |


### Virtual Directories

|    Alias                      | Physical Path                            |
|-------------------------------|------------------------------------------|
| DCPData                       | E:/DCPData/                              |


### PHP extensions
N/A - using .NET Core route therefore this section does not apply.

### Scheduled Tasks
N/A - no scheduled tasks required

## Frontend

### Micro frontends
All MVDA functionality is bundled in a single microfrontend, which uses the following configuration:

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | MVDA                                     |
| Hostname                      | dc-{env}-gxp.domain.com, for prod dcp-gxp.domain.com   |

### Web components

The MVDA module, provides one additional web-components

* the `dashboard-model-presentation` is read by the DCP framework and provides the dashboard tile implementation.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Modulename                    | MVDA                                     |
| Name                          | dashboard-model-presentation             |

> {info} MVDA uses web-components provided by DCP Framework, therefore the framework components need to be installed prior to the MVDA module installation.

## Module specific steps
MVDA does not require any module specific installation steps.