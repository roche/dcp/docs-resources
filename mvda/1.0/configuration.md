# Configuration
- [Introduction](#introduction)
- [Frontend](#frontend)
- [Backend](#backend)
    - [Configuration Sources](#configuration-sources)
    - [WebConfig Transformers](#configuration-sources)

## Introduction

## Frontend

Frontend configuration is using the angular default configuration set-up, which is outlined in the [getting started](/framework/1.0/configuration.md) section.

## Backend

### MVDA - API

The API is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| CleanHistoryTables | Boolean | Flag if the deletion from history tables is allowed (which would remove the audit trail)| false |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCPBasicBaseUrl | String | The deploy URL of the basic microservice URL |  |
| Error sanitization | 
| DevelopmentMode | Boolean | Should the app, been started on localhost | false |
| ActiveLogDebug | Boolean | ToDo | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| EnableSwagger | Boolean | Should the swagger UI with all endpoints should be accessible | false |
| DCP Business Constants |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| SendFeedbackTo | String | The e-mail address used as sender for the e-mail notifications | |
| TimeoutModelLockMinutes| Int | The time (in minutes) until a model is automatically unlocked by the system | 10 |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\Messages.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |


### Batch Database Worker

The batch database worker is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCP Business Constants |
| ReCheckViolationsInMinutes | Int | The time (in minutes) until a loop should be re-executed | 1 |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\WorkerBatchDatabase.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |

### Cache Worker

The cache worker is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|--------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCP Business Constants |
| CacheRealDataInSeconds | Int | The loop time - which is used to scheduled the real time cache | 1 |
| CheckBatchDurationExeededInSeconds | Int | The loop time - which is used to check for long running batches | 20 |
| FixBadValuesInMinutes | Int | The loop time - which is used to schedule the bad value update attempts | 2 |
| ModelValidationStatusCacheInMinutes | Int | The loop time - which is used to scheduled the validation cache updates | 60 |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\WorkerBatchDatabase.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |

### CleanUp Worker

The clean up worker is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|--------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCP Business Constants |
| DeleteWorksetRelatedEntitiesInDays | Int | The loop time (days) - which is used to schedule the workset clean-up | 1 |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\WorkerBatchDatabase.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |


### EventBus Worker

The event bus worker is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|--------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCPMVDABaseUrl| String | The deploy URL of the mvda microservice URL | |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\WorkerBatchDatabase.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |


### Notification - Worker Service

The notification worker is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|--------------:|--------------------|----------------|
|ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDataSourceMountPoint | String | The mount point of the vault kv2 secret engine storing the external data source secrets | ext-datasource/ |
| VaultDataSourcePath| String | The path to the external datasource secrets, using the datasource ID as a secret name | dev |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultAuthMethod | String | The authentication method used for accessing vault | Certificate |
|General|
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| MachineName | String | DNS name used to read the certificate from the store (also searches SANs) | true |
| DCPCoreBaseUrl| String | The deploy URL of the core microservice URL | |
| DCBaseUrl | String | The deploy URL of the DCP frontend | |
| DCP Business Constants |
| GetModelNotificationInSeconds | Int | The loop time (seconds) - which is used to scheduled the notification check | 1 |
| DCPPAPage | String | The URL route used to open the process analytics page | #siteID#?url=app,#siteID#,mvda,digital-clone-analytics, |
| DCPLogoUrl | String | The URL to the DCP logo | "/app-framework/assets/images/dcp-logo.png" |
| DCPNotifcationEditPage | String | The URL to the edit page of the notification | #siteID#?url=app,#siteID#,mvda,notifications,edit, |
| DCPShareLinkPage | String | The URL used for the generated share link | /share-link/ |
| Headers |
| OriginHeader | String | The origin header added to external requests for CSRF configuration | |
| DCPMicroserviceHeaderName | String | The header providing the microservice secret (added on the gateway layer) required to use tokens issued for server to server communication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The header to be added to all external (data source) request for CSRF configuration | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The header providing the secret for accessing the swagger files via the gateway | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The header providing the gateway secret (added on the gateway layer) required to use tokens issued for the gateway | X-DCPGateway-Key |
| MemoryCache |
| AudienceDetailsExpireDays | Double | The cache ttl time (in days) for the audience details | 1 |
| ConnectionStringDetailsExpireDays | Double | The cache ttl time (in days) for the connection string | 1 |
| HeadersDetailsExpireDays | Double | The cache ttl time (in days) for the header details | 1 |
| AuthorizationServerDetailsExpireDays | Double | The cache ttl time (in days) for the authorization server details | 1 |
| RabbitMQMessageBusDetailsExpireDays | Double | The cache ttl time (in days) for the message broker settings | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debig level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\MVDA\\WorkerBatchDatabase.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |

### Configuration Sources
In addition to the `appsettings.json` the MVDA module uses the following configuration objects:

The MVDA section inside the `ConfigurationItems.json` file inside the core project, has the following values. With the configuration, MVDA requires the following mandatory datatypes:

* Equipment Data (DataTypes.Type: 1)
* EDMS Data (DataTypes.Type: 4)

And supports resources, for:

* Audit trail implementation (Resources.Type: 2)
* Admin actions (Resources.Type: 3)
* Signing up for [batch subscription]({{setion}}/{{version}}/events_and_messaging) (Resources.Type: 1)

```json
{
        "Name": "MVDA",
        "Host": "https://#Env#-#IsValidated#.domain.com",
        "IsValidated": false,
        "DataTypes": [
            {
                "Type": 1,
                "Mandatory": true,
                "AllowMultiselection": false
            },
            {
                "Type": 4,
                "Mandatory": true,
                "AllowMultiselection": false
            }
        ],
        "Port": "8188",
        "Resources": [
            {
                "ResourceURL": "api/BatchEvent/GetBatchEvents",
                "Type": 1
            },
            {
                "ResourceURL": "api/AuditTrail/GetModuleAuditTrail",
                "Type": 2
            },
            {
                "ResourceURL": "/mvda/administration/actions/create",
                "Type": 3
            }
        ],
        "RPackages": [
            "acdcMvda"
        ]
    }
```

The following admin actions, are available in the MVDA module, configured by `AdminActions.json` at the root level in the project. All admin actions are implemented in the `SiteController`, the intended business implementation can be taken from the requirement specification.

```json
[
  {
    "Path": "mvda-batchDatabase",
    "DisplayText": "Batch Database Backfill",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "get-all-pi-web-ids-per-site",
    "DisplayText": "Get All PI Web Ids Per Site",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-all-global-filters",
    "DisplayText": "Replace All Global Filters",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-all-web-ids",
    "DisplayText": "Replace All Web Ids",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-sensor-names",
    "DisplayText": "Replace All Sensor Names",
    "Permissions": [ "GLOBAL_ADMIN" ]
  }
]


```
