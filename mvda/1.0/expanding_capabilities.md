# Expanding Capabilities

- [Introduction](#introduction)
- [ToDos](#todos)

## Introduction

In this section, we embark on a journey of empowerment, guiding you through the process of expanding the capabilities of DCP or a module. As technology evolves and user needs grow, the ability to customize and add new features becomes paramount. Here, we provide a roadmap for you to unlock the full potential of the tool by seamlessly integrating new functionalities. Delve into the art of innovation and start shaping the future of your tool.

## ToDos
- [ ] Add additional model type