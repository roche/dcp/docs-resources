# Installation and setup
- [Introduction](#introduction)
- [R packages](#r-packages)
- [Backend](#backend)
    - [Windows Services](#windows-services)
    - [API](#api)
    - [Virtual Directories](#virtual-directories)
- [Frontend](#frontend)
    - [Micro Frontend](#micro-frontends)
    - [Web components](#web-components)
- [Module specific steps](#module-specifics-steps)

## Introduction
Using the [standard installation path](/framework/1.0/installation_and_setup), described in the DCP Framework. For the variables use the sections below.

## R packages

DCP Framework functionality is implemented in a single package, which depends on other acdc packages which are installed via the package dependencies.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| packageName                   | acdcStudio                               |


## Backend

### Windows Services

**Message Delivery Service**
This service is used for end user message delivery via SMTP.

|    Variable                   |                 Value                                        |
|-------------------------------|--------------------------------------------------------------|
| ServiceName                   | CoreEmailProcessorDCPService                                 |
| ModuleName                    | Core                                                         |
| FolderName                    | EmailProcessorService                                        |
| DisplayName                   | DCP Core Email Processor Service                             |
| Startup Type                  | Automatic                                                    |
| ExecutableName                | DigitalClonePlatform.Core.WindowsServices.EmailProcessor.exe |

**Event subscription Service**
This service is responsible for checking the status of batches and the distribution of Batch Events to other components:

|    Variable                   |                 Value                                        |
|-------------------------------|--------------------------------------------------------------|
| ServiceName                   | CoreActiveBatches                                            |
| ModuleName                    | Core                                                         |
| FolderName                    | ActiveBatchesService                                         |
| DisplayName                   | DCP Core Active Batches Service                              |
| Startup Type                  | Automatic                                                    |
| ExecutableName                | DigitalClonePlatform.Core.WindowsServices.ActiveBatches.exe  |

**Core Event Bus Service**
This service is used for handling message emitted to core, e.g. audit trail messages, etc.

|    Variable                   |                 Value                                  |
|-------------------------------|--------------------------------------------------------|
| ServiceName                   | CoreEventBusDCPService                                 |
| ModuleName                    | Core                                                   |
| FolderName                    | EventBusService                                        |
| DisplayName                   | DCP Core Event Bus Service                             |
| Startup Type                  | Automatic                                              |
| ExecutableName                | DigitalClonePlatform.Core.WindowsServices.EventBus.exe |

**Core CleanUp Service**
Service for managing retention periods of records and removing orphaned items.

|    Variable                   |                 Value                                        |
|-------------------------------|--------------------------------------------------------------|
| ServiceName                   | CoreCleanUpDCPService                                        |
| ModuleName                    | Core                                                         |
| FolderName                    | CleanUpService                                               |
| DisplayName                   | DCP Core CleanUp Service                                     |
| Startup Type                  | Automatic                                                    |
| ExecutableName                | DigitalClonePlatform.Core.WindowsServices.CleanUp.exe        |

**Basic Event Bus Service**
This service is used Service for handling cross service events, e.g. site deletion, device deletion.

|    Variable                   |                 Value                                   |
|-------------------------------|---------------------------------------------------------|
| ServiceName                   | BasicEventBusDCPService                                 |
| ModuleName                    | Basic                                                   |
| FolderName                    | EventBusService                                         |
| DisplayName                   | DCP Basic Event Bus Service                             |
| Startup Type                  | Automatic                                               |
| ExecutableName                | DigitalClonePlatform.Basic.WindowsServices.EventBus.exe |

**Basic Notification Service**
This service is used for checking notifications based on single parameters or equations.

|    Variable                   |                 Value                                        |
|-------------------------------|--------------------------------------------------------------|
| ServiceName                   | BasicNotificationsDCPService                                 |
| ModuleName                    | Basic                                                        |
| FolderName                    | NotificationsService                                         |
| DisplayName                   | DCP Basic Event Bus Service                                  |
| Startup Type                  | Automatic                                                    |
| ExecutableName                | DigitalClonePlatform.Basic.WindowsServices.Notifications.exe |


### API
The framework uses the booth .NET Core and .NET Framework (4.8) API path, functionality is provided by the following IIS sites:

**Core API**
This API provides core functionality for authentication, administration.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | Core                                     |
| ModuleName                    | Core                                     |
| HTTP-Port                     | 8087                                     |
| HTTPS-Port                    | 8187                                     |
| NET CLR Version               | .NET CLR Version v4.xx                   |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com   |

**Basic API**
This API provides the business functionality for the framework, this includes: Continuous Data Monitoring, Batch Data Studio

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | Basic                                    |
| ModuleName                    | Basic                                    |
| HTTP-Port                     | 8084                                     |
| HTTPS-Port                    | 8184                                     |
| NET CLR Version               | .NET CLR Version v4.xx                   |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com  |

**Internal API Gateway**
This gateway is used by the frontend to access the different backend microservices. This implements load balancing
and some entity aggregators.


|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | InternalGateway                          |
| ModuleName                    | InternalGateway                          |
| HTTP-Port                     | 8099                                     |
| HTTPS-Port                    | 8199                                     |
| NET CLR Version               | No managed code                          |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com   |

**External API Gateway**
This gateway is used by external third party applications and implements security and rate limiting for additional safety.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | ExternalGateway                          |
| ModuleName                    | ExternalGateway                          |
| HTTP-Port                     | 80                                       |
| HTTPS-Port                    | 443                                      |
| NET CLR Version               | No managed code                          |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-api.{domain}.com, for prod go-dcp-api.{domain}.com   |

### Virtual Directories

|    Alias                      | Physical Path                            |
|-------------------------------|------------------------------------------|
| DCPData                       | E:/DCPData/                              |


### PHP extensions
N/A - using .NET Core route therefore this section does not apply.

### Scheduled Tasks
N/A - no scheduled tasks required

## Frontend

### Micro frontends
All framework functionality is bundled in into multiple microfrontends, which uses the following configuration:

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | login                                    |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com   |

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | administration                           |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com    |

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | api-service                              |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com    |

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | framework                                |
| Hostname                      | dc-{env}-gxp.{domain}.com, for prod dcp-gxp.{domain}.com    |


### Web components

The dcp framework provides the following web-components

* the `dcp-user-info-banners` is used to provide user banners for end user information on downtimes, releases, etc.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Modulename                    | shared                                   |
| Name                          | dcp-user-info-banners                    |

* the `dcp-time-range` is used as an input element for capturing time ranges from end users.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Modulename                    | shared                                   |
| Name                          | dcp-time-range                           |


## Module specific steps
The DCP framework uses the R library sf for spatial transformation. This library has a set of dependencies on the operating system level - which need to be installed. The following steps describe the installation of the operating system requirements on an AC-DC server version 3.x assume Red Head Enterprise Linux (RHEL) 8.x.:

1. Copy the gdal folder from the dependency repo: 
    `https://code.roche.com/digitalclone/platform/build-dependencies`
    to the AC-DC server and ensure that the destination directory has at least 2 GB of free disk space.

2. Login to the instance and open a sudo shell

    ```sh
    sudo /home/adm/bin/suroot
    ```

    Change the working directory to the cloned folder.

3. Make the installation script executable by running:

    ```sh
    chmod +x install-sf.sh
    ```

4. Run the installation script, by running:

    ```sh
    bash install-sf.sh
    ```

    *Note: based on the current automation scripts used some libraries are missing, in this case install them manually and list the installed rpm in the execution protocol.*