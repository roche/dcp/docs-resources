# Frontend Modules

- [Introduction](#introduction)
- [Available modules](#available-modules)
    - [App](#app)
    - [Assets](#assets)
    - [Batch](#batch)
    - [Core](#core)
    - [Filter](#filter)
    - [GxP Info](#gxp-info)
    - [Pages](#pages)
    - [Layout](#layout)
    - [Store](#store)
    - [Site Store](#site-store)
    - [Shared](#shared)
    - [Sensor](#sensor)
    - [User](#user)

## Introduction
Microfrontends itself are packaged using different components. To keep consistency within the application and allow faster development of new applications, the reusability of the components is engaged. These components are packaged as modules. Modules are maintained using their own git repository. Frontend-apps with a dependency are importing the files using git submodules. This way every module contains its own copy of the source code which allows individual builds and deployments while the consistency across the entire platform is kept.
When a change to a shared module is made, not all application needs to be rebuilt. Git sub-modules keep a mapping between commits to the main app and commits to the submodule. Deploying multiple versions of the same sub-module inside different apps is possible, this enabled a decoupling on a time axis (when deployments are made) while keeping a logical bridge.

## Available modules

### App
The App module contains the InfoModalComponent (showing if the user uses a browser different from Chrome) and the reusable AppBaseComponent which is used as a base component for every AppComponent in all applications.
The AppBaseComponent contains some basics, e.g.:

* Subscriptions, e.g. triggering refresh of permissions on site change
* Triggering alerts labels.

### Assets
The Assets module contains all styles, images, translations and fonts that are shared in every frontend application. The translations files that are in the Assets module contain only strings that are shared between applications.

### Batch 
The Batch module contains batch related NGRX Store, routing and components. Routing and components related to it are used in the MVDA application. NGRX Store and some components like one of the most important grid in the system - the BatchGrid are used in several other applications.

### Core
The Core module contains a few HTTP interceptors, DcHttpService, an AppConfig constant that contains data about API module routes and ports, used graph colors, etc. and api.json file containing API URLs used in different environments.
DcHttpService is a wrapper of Angular HttpClient that is used to build URI based on AppConfig data, create Authorization header, handle API calls errors and make POST, PUT, GET, DELETE API calls.

GxpInterceptor is used to correct API calls server paths (-gxp.com /-nongxp.com) based on used :port.

### Filter
The Filter module contains Global Filter-related NGRX Store, Services and Components. Part of DcFilter Store is AdditionalFilters object that is used in almost all pages with Share functionality, to store additional data from page, such as Time range, selected Batches, Models, Sensors, Active tab etc.

The Filter module contains also contains TimeRange Filter Components, Model and Sensor Selector components. They are used in some pages that have a Time filtering or Share button. Like in DcShared and DcStore we have multiple modules files that we replace with tsconfig setting during the build process for different Applications, based on what the Application needs from the module.

### GxP Info
The GxpInfomodule contains all code related to GxpInfo label, icons and grid column components, services, interceptors and NGRX Store.
GxpLabel component is used in DcLayout layout component and the Icon is used in some places like dashboard widgets.
Gxp Grid column component is used in some grids that need to display if entity is Gxp validated or not e.g. BCA list overview.

### Layout
The Layout module contains reusable components and HTML templates for applications layout, such as top navigation, main (side) navigation, feedback and help.

### Pages
The Pages module contains components used for Login and SSO authentication. In production, it is used only by the login application, but for local development it is used by all applications localhost login.

### Shared
Like in the Filter and Shared module multiple modules files are replaced by tsconfig settings during the build process for different applications, based on what the application needs from the module.
Also multiple AppState model and Store Reducers files needs to be loaded into the store, depending on application needs.

### Site Store
The SiteStore module contains NGRX store for sites.

### Store
This shared module contains the main NGRX Store module and AppState model that are used by all the applications. Also contains UIStore State, Actions, Reducers and Effects. The UIStore is used for managing some UI tasks like, Alerts, Menu Toggle, Loading indicators etc.

### Sensor
The Sensor module contains NGRX Store for sensor state, some services and routing and components related to Continuous Data Monitoring pages.

### User
The User module contains the user settings popup and NGRX store and service related to user login and data.