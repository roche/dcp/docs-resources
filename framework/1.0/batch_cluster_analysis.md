# Batch Cluster Analysis
- [Introduction](#introduction)
- [Database structure](#database_structure)

## Introduction

The batch cluster analysis (BCA) is designed to perform ad-hoc analysis on a given dataset. BCA addresses two main types of problems:

* Classification problems: The input data is screened for clusters, and differences between various observations are visualized.
* Regression problems: In addition to the data attributes, an additional Y-variable (regression variable) is provided. The method aims to establish the relationship between the input dataset and the regression variable.

Within these groups, different analytical approaches are available [^2]

* Classification problems:
    * Principal Component Analysis (PCA)

* Regression problems:
    * Multiple Linear Regression (MLR)
    * Principal Component Regression (PCR)
    * Partial Least Squares Regression (PLS)
    * Karsten Luettgen Pattern Matching
    * z-Test Analysis

[^2]: For details see the statistical appendix

Regression variables can be provided using one of the following methods:

* PI Tag: The result from a sensor, along with the retrieval method (start, min, max, etc.), specifying a single value per batch used as a regression variable.
* Event frame attribute: An attribute provided at the batch level from the equipment data source.
* Manual data: If the variable is not directly available in DCP, the values can be manually entered and uploaded for analysis.
* Attribute from an additional system: Analytical data provided by an additional system, e.g., a yield value from LIMS.
* Duration: The duration of the event frame itself.

### Batch Cluster Analysis Records

```mermaid
erDiagram
    BatchClusterAnalysis ||--o{ BatchClusterAnalysisAttributeValues : owns

    BatchClusterAnalysis  {
        uniqueidentifier(16) ID "UniqueId"
        nvarchar(500) Name "The user defined name if the analysis"
        nvarchar Configuration "JSON object storing the analysis configuration used by R"
        bit(1) IsPublic "Flag if the analysis is only visible for the creator or all users"
        bit(1) InProgress "Flag if the set-up has been completed"
        nvarchar Filter "The global filter path to the element to which the analysis is assigned to"
        int(4) SiteId "Relation to the site where the analysis is linked to"
        nvarchar TimeFilter 
        datetime2(8) SysStartTime 
        datetime2(8) SysEndTime 
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }

    BatchClusterAnalysisAttributeValues  {
        uniqueidentifier(16) ID "UniqueId"
        nvarchar EventFrameId "Unique id on the datasource for the eventframe"
        nvarchar(500) Name "Attribute name of the manual user input"
        decimal(9) Value "Manual entered attribute value for the corresponding eventframe"
        uniqueidentifier(16) BatchClusterAnalysisId "Relation to the analyis, where the attribute is used"
        datetime2(8) SysStartTime 
        datetime2(8) SysEndTime 
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  Definitions of performed batch cluster analysis and manually created attributes and their values                            |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |