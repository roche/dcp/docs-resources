# Dashboard

- [Introduction](#introduction)
- [Entity aggregation](#entity-aggregation)
- [Updating the layout](#updating-the-layout)
- [Import and Export](#import-and-export)
- [Deleting a dashboard tab](#deleting-a-dashboard-tab)
- [Carousel](#carousel)

## Introduction


## Entity aggregation
In order to combine entities from multiple microservices, GUIDs are used instead of integers as unique IDs for the database. The workflow for entity aggregation is defined as follows:  

Every microservice providing functionality exposes one or more endpoints delivering different dashboard tiles. These endpoints are aggregated using the internal gateway, allowing the frontend to access a single API endpoint for retrieving the required data across all active modules.  

The dashboard display process is split into two steps:  

- **Layout**: In this first step, static/non-changing parts are loaded, including labels, units of measure, positions, etc.  
- **Data**: This call fetches and updates the dynamic parts of the dashboard tiles. The framework application fetches the data and adds it to the `NGRX` store. From the store, module-specific web components handle data presentation. The data loading process supports two strategies:  

  - **Delta**: The received data is appended to the existing elements in the store.  
  - **Batch**: The existing data is replaced with the new data received from the API.  

Subsequent API calls to update a dashboard layout only perform data calls, providing the last updated timestamp as a property for each tile. The backend is responsible for filtering the relevant data if the dashboard is configured in append/delta mode.  

For both calls, APIs must implement a model inherited from a base model. Shared base models, along with required constants and other definitions, are managed within a shared NuGet package called `DCP.Framework.Dashboard`. A detailed description of the properties can be found in the related package definition.  


## Updating the layout
Updating the layout of a dashboard happens centrally inside DCP framework, however the records are persisted in the database layers of the owning microservice. In order to achieve this the basic web-api omits an event to the eventbus that a particular update needs to be performed. 

The message is sent with the routing key *UpdateDashboardLayoutsRoutingKey* to the EventBusExchanger using the following format:

```json
{
    "DashboardTabId": "638c6c53-6a7f-ec11-90fe-005056a7688d",
    "DashboardLayoutDetails": [{
        "ID": "b9c10216-7998-ec11-90fe-005056a7688d",
        "GridLayout": {
            "columnStart": 2,
            "columnEnd": "span 1",
            "rowStart": 1
        }
    }],
    "SiteId": 0
}
```

## Import and Export
To enable sharing of dashboards, an import/export functionality is implemented. On Dashboard export a GUID is generated. This is saved with the exported dashboard tab, the version and the export date to the database. The generated GUID can be  distributed by the user and used for further imports. Importing a dashboard always imports the state when the GUID was generated and any changes (additional elements, removals, reordering, etc.) are not reflected. This allows *"forking"* of dashboards, which all users can adapt to their needs. To achieve this, the following import logic is implemented:

Based on the GUID the right version for the dashboard tab is queried from the main or the history table

The related dashboard elements are queried with the where clause on the updated/created date less than the export date. They are grouped by the dashboard element, where inside the group the elements are order by data updated in ascending order and only the first (latest) element is selected and will be imported

A message to the RabbitMQ eventbus that the dashboard should be imported, all dependent microservices can implement their own import method for restoring/importing the dashboard. 

The message is sent with the routing key *DashboardImportRoutingKey* to the EventBusExchanger using the following format:
```json
[{
	"ID": "985670bd-7995-ec11-90fe-005056a7688d",
	"Name": "test 1",
	"SiteId": 16,
	"DateCreated": null,
	"DateUpdated": null,
	"UserIDCreated": 8,
	"UserIDUpdated": null
}]
```
## Deleting a dashboard tab

## Carousel

For carousel and presentation shared widgets routing that is outside of AuthGuard scope is used. This allows easy embedding in other systems, e.g. info screens. The communication with API for these dedicated endpoints do not require an authentication token.

To create a presentation widget a URL is build. It contains the dashboard widget GUID and the widget type enumeration, in the format SomeGUID–WidgetTypeEnum. The PresentationComponent chooses the API endpoint to use based on the enumeration type, routing to different microservices.

The CarouselComponent is based on Kendo scroll view with multiple presentation widgets. The widgets receive as an input  the GUID–Type format and perform an API call to create the chart/presentation.

For the carousel widget  some URL query parameters (?autorotate=true&showArrows=false&interval=10) to change carousel behavior are available.
* autorotate: boolean, default value: true, if set to false, the autorotation of carousel will be disabled
* showArrows: boolean, default value: false, if set to true, carousel will display navigation arrows for previous and next.
* interval: number, default value: 10, it represents the time for change to the next slide in seconds. Not applicable if the autorotate is set to false.


### Dashboard Records

```mermaid
erDiagram
    Dashboard ||--o{ DashboardMultipleValues : owns
    Dashboard||--o{ DashboardSingleParameter : owns
    DashbaordTabs ||--o{ Dashboard : "consists of"
    DashbaordTabs ||--o{ DashboardExport : "may has"

    DashboardTabs  {
        uniqueidentifier(16) ID "UniqueId"
        nvarchar(500) Name "User defined name of the dashboard tab"
        int(4) SiteId "Relation to the site where the dashbaord tab is linked to"
        int(4) Version "Version identifier, used in import/export to load correct version"
        datetime2(6) SysStartTime 
        datetime2(6) SysEndTime 
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }   

    DashboardSingleParameter  {
        int(4) ID "UniqueId"
        uniqueidentifier(16) DashboardId "Relation to dashbaord for title, description and other basic information"
        nvarchar(500) SensorWebId "UniqueId of the sensor on the datasource"
        int(4) Version "Version identifier, used in import/export to load correct version"
        datetime2(6) SysStartTime 
        datetime2(6) SysEndTime 
        tinyint(1) Axis "Identifier of the axis to assign the data to"
        tinyint(1) AxisSide "The side of the axis (right, left)"
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }

    DashboardMultipleValues  {
        int(4) ID "UniqueId"
        uniqueidentifier(16) DashboardId "Relation to dashboard for title, description and other basic information"
        nvarchar Expression "Expression evalated during the check, sensor references in @, numbers in #"
        int(4) Version "Version identifier, used in import/export to load correct version"
        datetime2(6) SysStartTime 
        datetime2(6) SysEndTime 
        tinyint(1) Axis "Identifier of the axis to assign the data to"
        tinyint(1) AxisSide "The side of the axis (right, left)"
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }

    DashboardExport  {
        int(4) ID "UniqueId"
        uniqueidentifier(16) DashboardExportGuid "System generated GUID used for sharing"
        int(4) UserId "Relation to the user performing the export"
        int(4) SiteId "Relation to the site from where the dashboard is exported"
        uniqueidentifier(16) DashboardTabsId "Unique id of the dashboard tab which was exported"
        int(4) DashboardTabVersion "The version"
        datetime(8) ExportDate "Datetime of the export in UTC"
        datetime2(6) SysStartTime 
        datetime2(6) SysEndTime 
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }

    Dashboard  {
        uniqueidentifier(16) ID "UniqueId"
        nvarchar(500) Title "User defined title for better identification"
        nvarchar Description "User definied detailed description"
        tinyint(1) Type "Enumeration for the type"
        nvarchar(500) DeviceWebId "DeviceId on the datasource, used for retriving the batch context"
        uniqueidentifier(16) DashboardTabId "Relation to the dashboard tab, where the element is located"
        nvarchar Filter "Global filter path to the notification to which the element is assigned"
        nvarchar Layout "JSON object with position and size of dashboard tile"
        int(4) Version "Version identifier, used in import/export to load correct version"
        datetime2(6) SysStartTime 
        datetime2(6) SysEndTime 
        int(4) LastModifiedBy 
        int(4) OwnerUserId 
    }
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  Definitions of basic (single and equation) dashboard elements                                                                |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |