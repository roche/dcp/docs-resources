# Directory Structure

- [Introduction](#introduction)
- [Backend](#backend)
    - [Backend Structure](#backend_structure)
- [Frontend](#frontend)
    - [Frontend Structure](#frontend_structure)


## Introduction


## Backend


## Backend Structure

```
project_root
    ├── API
    │   ├── Controllers
    │   ├── Program.cs
    │   ├── StartUp.cs
    └── Constants
    └── Core
        ├── Factories
        ├── Interfaces
        ├── Services
    └── DAL
        ├── Entities
        ├── Interfaces
        ├── Migrations
        ├── DbContext.cs
    └── DataSources
        ├── Datasources
        ├── Interfaces
    └── Models
    └── WindowsServices.EventBus
    └── WindowsServices.Notifications
    └── WindowsServices.CleanUp
    └── WindowsServices.ActiveBatches
```

* **API/** - This contains the C# webAPI project used/consumed by the frontend or the Windows service.
* **Constants/** - 	The collection of string constant/enumerations.
* **Core/** - This contains the interfaces and services implementing the business logic of the module. 
* **DAL/** - This is the section containing the data access layer based on the Entity Framework. The code first pattern is used, see section data design for entity relationship diagrams.
* **DataSources/** - 	In case of dedicated data supply needed by the module, this section defines the interface and the interface implementations for the data type.
* **Models/** - 	This is the section containing the data transfer objects (DTOs) used for data transfer between the subroutines.
* **WindowsServices.EventBus/** - This is the Windows service implementing the event subscription in the module.
* **WindowsServices.Notifications/** - This is the Windows service implementing the notification behavior inside DCP.



## Frontend


## Frontend Structure

The application is built based on the AngularCLI template. In addition to the base template a few folders and files required by the single-SPA library are added (e.g. ./extra-webpack.config.js, ./src/single-spa, ./src/main.single-spa.ts and ./src/app/empty-route).

Frontend modules are configured as git submodules inside the applications. Except for the asset module, all others are installed inside ./src/libs/ folder. 

````
project_root
├───src
│   ├───app
│   ├───environment
│   ├───libs
│   ├───single-spa
│   ├───translations
└───tests
````
* **src/** main code folder
    *  **app** - The folder contains all elements used in the analysis alongside with their editors ( settings components)
    *  **environment** - Contains all the components used to represent an Application
    *  **libs** - Contains all the components used to represent an Application
    *  **translations** - Contains translations files that are used only by current application
***

Inside ./src/app  the main code for the application like routing and pages is located. In order to use AngularCLI App Module, App Component and App Routing files are always present. 

If the application uses a side menu and the layout module (such as all applications except Login), there is a layout folder, that contains layout.{{APP_NAME}}.module.ts and related files that extend the Base Components from the layout module. 

__Important__: In all Applications AppModule, LayoutModule, LayoutRoutingModule and LayoutComponents need to have unique names like AppMvdaModule, LayoutMvdaModule etc. and for App and Layout Components also unique selector (selector: 'app-mvda-root'), in order to be sure that SingleSPA will not load the wrong file during the switch between applications on the production server.

All other files are organized in Module folders, are separated logically or based on entities structure. Inside module folders all files that are related to this module like entity.module, entity.service, entity-routing.module, Components, Models and NGRX Module, if applicable, can be found.

If the Module folder contains NGRX files, they are inside the ./store folder. In the same folder files for Actions, Effects, Reducers, State, Facade and Entity or Logical Structure Store Module (like BatchStoreModule or DashboardStoreModule) are present.