# UI Frame

- [Introduction](#introduction)
- [Header](#header)
    - [User Settings](#user-settings)
    - [Help](#help)
    - [Translations](#translations)
- [Navigation Menu](#navigation-menu)

## Introduction
The main menu can be provided in two forms:

* angular applications can import the component directly and populate the configuration
* non-angular applications can import a dedicated app **dcp-sidemenu** which provides the sidebar - the app developer only needs to implement the inner-part of the frame. 

The HTML structure of the Main menu is inside of DC Layout’s SidebarBaseItems component. During the initialization, the component is reading the configuration object from session storage and uses it to display the main menu.

### User settings
User settings like *TimeFormat and DateFormat* are received in the token API response. The information is saved in the NGRX Store, from where it can be accessed from all components.

Inside the components, user preferences are applied by:

* a format string in the MomentJS Format method
* or with DatePipe in the HTML template

In addition, the timezone for every site is received from the API during GetUserSite or SetUserSite calls. The timezone is saved in the NGRX Store, and used to correct dates received from API (like CreatedAt, UpdatedAt for Models, Notifications etc.) with MomentJS TimeZone library.


### Help
The help functionality is based on pages URLs. The implementation follows the following 3 steps:

Every URL providing a section into the Help, a translation string is added to the current application translation files, e.g. HELP.mvda/model-manager/list, HELP.mvda/model-manager/new/PCA/BLM, HELP.mvda/model-manager/new/PLS/BEM
The DcLayout module checks if the current URL has a matching key in the translation files. If a key is found, the button (?) opening the help modal component is shown.
In the opened help modal  the string (HTML) from the translation files  for the current URL is shown. 
In addition, the help modal contains a search functionality, which searches the loaded application translation files.


### Translations
The DCP UI supports multiple languages, currently offering German and English. Additional languages can be added easily. Texts in the UI are represented as keys, which are replaced by their corresponding values from the translation files. To avoid handling multiple languages within the API, errors and warnings are returned as status codes, which correspond to specific keys in the Presentation Tier implementation. Keys for errors are defined in the static classes ErrorMessages.cs and Warnings.cs.

The DCP UI has three types of translation files:

 * The files en.json, de.json, etc. are used for translation of the main menu in the base app, so when we change it to be populated to every app.
 * Some shared translations inside the DcAssets module, which is used in every app - for shared/overarching translations
 * Every app has his own translation file following the {language}-{module}.json pattern (e.g. en-mvda.json) for app specific translations.

## Navigation Menu
Two types of entities can be configured. The type is based on the isModule flag inside the configuration. Supported are:

 * Simple buttons (isModule:: false) which perform a simple redirection to the corresponding app scope.
 * Menu buttons (isModule: true) those are expandable and show additional sub-items.

An example configuration can be found below:
```js
    [{
        angularticsAction: 'Click on go to dashboard',
        iconClass: 'icon-dashboard',
        isModule: false,
        dividerBefore: false,
        ngxPermissionsOnly: ['ADMIN', 'MVDADEVELOPER', 'MVDAUSER', 'VIEWER'],
        url: '/dashboard',
        text: 'SIDE_MENU.DASHBOARD'
        }, {
        isModule: true,
        name: 'MVDA',
        menuModule: 'mvda',
        ngxPermissionsOnly: ['ADMIN', 'MVDADEVELOPER', 'MVDAUSER'],
        parent: {
        angularticsAction: 'Click on MVDA',
        ngxPermissionsOnly: ['ADMIN', 'MVDADEVELOPER', 'MVDAUSER'],
        tooltip: 'SIDE_MENU.MVDA_FULL',
        textShort: 'MVDA',
        textLong: 'MVDA',
        },
        subMenu: [...]
    }]
```

### Simple buttons
Simple button - can be on first level or as a subMenu element inside the Module element. A simple button has the following configuration:

* **angularticsAction** - name of the action that will be displayed in Google Analytics
* **iconClass** - used for css class that will display button Icon
* **isModule** - should be False
* **dividerBefore** - it will display `mat-divider` before element if true
* **ngxPermissionsOnly** - list of permissions that will be applicable for displaying the button

### Menu buttons
Are expandable buttons, which are used for indicating the different modules. A menu button has the following configuration:

* **isModule** - should be True
* **name** - used for checking if module is active
* **menuModule** - used for toggle the buttons. Should be some of values defined in DCStore / ui.actions.ts / type menuModules (currently - 'mvda' | 'chromeTa' | 'complianceCheck' | 'ppm')
* **ngxPermissionsOnly** - list of permissions that will be applicable for displaying the menu
* **parent** - data for primary/toggle menu button
* **angularticsAction** - name of the action that will be displayed in Google Analytics
* **ngxPermissionsOnly** - list of permissions that will be applicable for displaying the button
* **tooltip** - translation string for tooltip that shows when the Name is not visible
* **textShort** - translation string that is used when the Sidebar menu is in collapse state
* **textLong** - translation string that is used when the Sidebar menu is not in collapse state or on mobile devices
* **subMenu** - List of Simple buttons that are shown when Module menu is expanded
