# Installation and Setup

- [Introduction](#introduction)
- [R Packages](#r-packages)
- [Backend](#backend)
  - [Windows Services](#windows-services)
  - [API](#api)
  - [Virtual Directories](#virtual-directories)
- [Frontend](#frontend)
  - [Micro Frontend](#micro-frontends)
  - [Web Components](#web-components)
- [Calculation Node](#calculation-node)
- [Module-Specific Steps](#module-specific-steps)

## Introduction

This section outlines the standard installation path for all DCP modules. Some steps are module- or language-specific and may not apply to a particular component. Please refer to the *Installation Verification Variables* documentation for the relevant steps in this execution path.

## App Server

### Web Server

1. **Create Digital Clone Data Folder**
   
   Create the data folder:
   
   ```plaintext
   E:\DigitalClonePlatformData\${Module_Name}
   ```

   Create the subfolders listed in the *DCP - IV Variables* under the backend API section.

   IIS User requires full access to the `DigitalClonePlatformData` folder:
   - Right-click | Properties | Security | Edit
   - Assign the `IIS_IUSRS` group full access to the folder.

2. **Add Maximum URL Length Configuration**

   - Open `regedit`
   - Navigate to:
     
     ```plaintext
     HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\HTTP\Parameters
     ```
   - Create a new DWORD with name `UrlSegmentMaxLength` and value `0`:
     - Right-click | New | DWORD (32-bit) Value

3. **Install SSL Certificates**

   - Install SSL certificates ensuring all required FQDNs are included in the Subject Alternative Names (SANs).
   - Grant `IIS_IUSRS` access to the private key:
     - Open `certlm.msc`
     - Locate the certificate, right-click | All Tasks | Manage Private Keys

4. **Configure Single Sign-On**

   - Configure Single Sign-On infrastructure.
   - Ensure `Web.config` settings match the configuration.

5. **Install OpenSSH Server**

   - Install the `OpenSSH Server` Windows feature.
   - Configure the service:
     - Open `services.msc`
     - Right-click on `OpenSSH Server`
     - Change **Startup Type** to **Automatic**
   - Copy the private key used in the CI/CD pipeline into:
     
     ```plaintext
     C:/ProgramData/ssh/administrators_authorized_keys
     ```
   - Modify SSH configuration in `C:/ProgramData/ssh/ssh_config`:
     
     ```conf
     StrictModes No
     ```
   - Restart the OpenSSH Server service.

6. **Install Server Features**

   - Open *Server Manager* | Add Roles and Features.
   - Select the following **Server Roles**:
     - **Web Server (IIS)**
   - Select the following **Web Server Roles**:
     - **Basic**
       - Default Document
       - HTTP Errors
       - Static Content
     - **Health and Diagnostics**
       - HTTP Logging
       - Request Monitor
       - Tracing
     - **Performance**
       - Static Content Compression
       - Dynamic Content Compression
     - **Security**
       - Request Filter
       - Windows Authentication
     - **Application Development** (Select All)
     - **Management Tools**
       - IIS Management Console
   - Click **Install**.

### RabbitMQ Service Installation

1. **Install RabbitMQ**
   
   - Execute the RabbitMQ installer with default values.
   - Select the following components:
     - RabbitMQ Server
     - RabbitMQ Service
     - Start Menu

2. **Configure RabbitMQ Environment Variables**
   
   - Add `RABBITMQ_BASE` and `RABBITMQ_HOME` environment variables pointing to:
     
     ```plaintext
     C:\ProgramData\RabbitMQ
     ```
   - Modify `rabbitmq-env` file (Administrator mode) located at:
     
     ```plaintext
     C:\Program Files\RabbitMQ Server\rabbitmq_server-3.9.6\sbin
     ```
   - Append the `homedrive` section at the end.

3. **Configure RabbitMQ TLS**
   
   - Create `rabbitmq.conf` (if not exists) at:
     
     ```plaintext
     C:/ProgramData/RabbitMQ
     ```
   - Add the following lines:
     
     ```conf
     listeners.ssl.default = 5671
     ssl_options.cacertfile = c:/certs/dc-{env}-gxp.pem
     ssl_options.certfile = c:/certs/dc-{env}-gxp.pem
     ssl_options.keyfile = c:/certs/dc-{env}-gxp.pem
     ssl_options.verify = verify_peer
     ssl_options.fail_if_no_peer_cert = false
     listeners.tcp = none
     management.ssl.port = 15671
     management.ssl.cacertfile = c:/certs/dc-{env}-gxp.pem
     ```

4. **Apply Changes**
   
   - Run the following commands in `RabbitMQ Command Prompt (sbin\dir)`:
     
     ```sh
     rabbitmq-service.bat stop
     rabbitmq-service.bat remove
     rabbitmq-service.bat install
     rabbitmq-service.bat start
     ```

5. **Enable Management UI**
   
   - Run:
     
     ```sh
     rabbitmq-plugins enable rabbitmq_management
     ```

6. **Create RabbitMQ User**
   
   - Open `https://localhost:15671` in a web browser.
   - Log in using default credentials:
     - **User:** `guest`
     - **Password:** `guest`
   - Navigate to **Admin Section**:
     - Add `${DEPLOY_USER}` as admin user.
     - Remove the `guest` user.
     - Re-login using the new user.
     - Set vhost permissions (`/`) for `${DEPLOY_USER}`.

### PHP Installation

**Variables**
Note the actual values of variables used in the proceeding steps.

| Variable name                   |                Actual value                           |
|---------------------------------|-------------------------------------------------------|
| MODULE_NAME                     |                                                       |
| DEPLOY_USER                     |                                                       |
| PHP_VERSION                     |                                                       |
| HOSTNAME                        |                                                       |
| HTTP_PORT                       |                                                       |
| HTTPS_PORT                      |                                                       |


**Steps**

1. **Create folder**

    Create a folder 
    
    ```plaintext
    E:\dcp\${MODULE_NAME}\
    ```
    Share it with the `${DEPLOY_USER}`

2. **Copy the installation binaries to the server**

    Transfer the required installation binaries to the server

3. **Set Up PHP Directory**

    Create a folder for the PHP version:

    ```plaintext
    C:/Program Files/php/${PHP_VERSION}/
    ```
    Copy the PHP binaries to the folder.

4. **Install composer binaries**

    Copy the composer binary to the the following location: 

    ```plaintext
    C:/Program Files/php/${PHP_VERSION}/composer/composer.phar
    ```

5. **Create the application pool** 

    In the IIS Manager do the following step: Expand the server | Right click on Application Pool | Add Application Pool.

    For the application pool use the settings from the variables sections.

    * **Name**:  DigitalClonePlattform_BE_${MODULE_NAME}
    * **.NET CLR version**: ${NET CLR Version}
    * **Managed pipeline module**: Integrated

6. **Create a new IIS Site**

    Open the IIS Manager | Right click on Sites | Add Site. 

    * **Site name**: DigitalClonePlatform_BE_${MODULE_NAME}
    * **Application pool**: *Select the application created in the previous step*
    * **Physical Path**: 
    * **Host name**: ${HOSTNAME}

7. **Add additional binding(s)**

    Open the IIS Manager | Select the site in step 4 |  Right click | Edit bindings 

    Add the following bindings:

    **Http Binding**
    * Type: http
    * Host Name: ${HOSTNAME}
    * Port: ${HTTP_PORT}
    * IP Addresses: '*'

    **Https Binding**
    * Type: https
    * Host Name: ${HOSTNAME}
    * Port: ${HTTPS_PORT}
    * IP Addresses: '*'

8. **Configure the php extensions**

    Open the php.ini file (`C:/Program Files/php/${PHP_VERSION}/php.ini`) in the directory. 
    For each extension listed in the IV variables uncomment or add the following line:

    ```conf
    extension=${EXTENSION_NAME}
    ```

9. **Import scheduled tasks**

    Open the task scheduler and create a new folder (if not existing) DCP. While having the DCP Folder selected
    Import all task | Right clicking | Import task. Select the xml files from /storage/win-task-templates | insert
    the Password and click Open

10. **Configure the php temp directory**

    Open the php.ini file and search for the temp files configuration, change to the following: 

    ```conf
    ; Directory where the temporary files should be placed. 
    ; Defaults to the system default (see sys_get_temp_dir) 

    sys_temp_dir = "E:\dcp\tmp"
    ```

11. **Update the certificate**

    Update the certificate used by curl/API clients for TLS

    Open the php configuration file C:/php/php.ini and search for the curl section. Modify the following line: 

    ```conf
    [curl] 
    ; A default value for the CURLOPT_CAINFO option. 
    ; This is required to be an ; absolute path. 
    curl.cainfo = "C:\php\cacert.pem"
    ```

12. **Create app folder and copy files**

    Create a new folder `${app-path}` and copy the content of the repository to the directory.

13. **Install dependencies**

    
    To install all dependencies using composer, open an admin `CMD` and execute the following command 

    ```sh
    pushd ${app-path} & "C:/php/${PHP_VERSION}/composer/composer.phar install"
    ```

14. **Update environment file (.env)** 

    Navigate to the ${app-path} folder and save the file `.env.example` as `.env` in the same directory. 

    Open an admin CMD in ${app-path} as working directory and run the 
    following command 

    ```sh
    php artisan dcp:manage-env \
        -d hashicorp \
        -u "https://$CONSUL_SERVER" \
        -t "$CONSUL_TOKEN" \
        -a "${DCP_SERVER}:${MODULE_PORT}" \
        -f
    ```
15. **Register the service in the service registry** 

    In an admin CMD window, navigate to the `${app-path}` folder and run the following command:

    ```sh
    php artisan dcp:manage-service register
    ```

16. **Generate the swagger documentation**

    In an admin CMD window, navigate to the `${app-path}` folder and run the following command:

    ```sh
    php artisan artisan l5-swagger:generate
    ```


17. **Create/Update database - run migrations** 

    In an admin CMD window, navigate to the `${app-path}` folder and run the following command:

    ```sh
    php artisan migrate --force
    ```

18. **Link the storage**

    In an admin CMD window, navigate to the `${app-path}` folder and run the following command:

    ```sh
    php artisan storage:link
    ```

19. **Restart the server**


### .NET Installation

#### Core

The steps marked as GxP need to be performed on the GxP infrastructure node, where the other steps need to be executed on both nodes (GxP/non-GxP).

1. Install the IIS rewrite module

2. Install the .NET Core SDK and the ASP.NET Hosting bundle

3. Install the Entity framework command-line tools:

    ```sh
    dotnet tool install --global dotnet-ef
    ```

#### Microservice API

1. **Folder Creation**

    Create the folder
    
     ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_BE/${MODULE_NAME}\
    ```
    and ensure that the permissions for the `IIS_IUSRS` groups is set according to the variable definition.

2. **Create the application pool**

    Open IIS Manager | Expand the server | Right click on Application Pool | Add Application Pool

    In the dialog use the following settings for configuration:

    * **Name**: *see variables document*
    * **.NET CLR version**: *see variables document*
    * **Managed pipeline mode**: Integrated

2. **Create an IIS Site**

    Open IIS Manager | Expand the server | Right click on Sites | Add Site

    In the dialog use the following settings for configuration:

    * **Site Name**: `DigitalClonePlatform_BE_${MODULE_NAME}`
    * **Application Pool**: use the pool created in the previous step
    * **Physical Path**: `C:/inetpub/wwwroot/DigitalClonePlatform_BE/${MODULE_NAME}`
    * **Host Name**: *see variables document - Hostname*

3. **Create additional bindings**

    IIS Manager | Select Site created in the previous Step | Right click | Edit Bindings

    In the dialog use the following settings for configuration:

    * **Site Name**: *see variables document*
    * **Application Pool**: *see variables document*

4. **Publish the solution**

    Open an administrator CMD and change the working directory to the solution. Then execute the following commands:

    ```sh
    dotnet nuget add source https://code.roche.com/api/v4/groups/5983/-/packages/nuget/index.json --name="code.roche.com" --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    dotnet nuget list source
    dotnet publish ${NET_PROJECT_NAME} -c ${ENVIRONMENT_SLUG} -o ./api
    ```

5. **Copy the build output**

    Copy the publish output `./api` to the static content folder (wwwwroot) folder of the created IIS Site `C:/inetpub/wwwroot/DigitalClonePlatform_BE/${MODULE_NAME}`

6. **Perform database migrations**

    In an administrator CMD, change the working directory to the module folder and run the following command:

    ```sh
    dotnet ef database update
    ```

#### Windows Service

1. **Stop the service**

    If the service is already installed and needs to be updated, stop the service by running:

    ```sh
    net stop ${SERVICENAME}
    ```

2. **Copy the files**

    Copy the build files from {{RepoPath}}/{{Service Name}}/bin/{{Env}} to the  following folders (see folder name), and overwrite already existing files: 

    ```plaintext
    E:/DigitalClonePlatformServices/${Module Name}/${Service Name}/
    ```


3. **Install the service**

    Install the service, by executing the following command in a Command Prompt (as admin): 

    ```sh
    SC CREATE "${SERVICENAME}" binpath= "E:\DigitalClonePlatformServices\${MODULENAME}\${Service Folder Name}\${ExecutableName}.exe" DisplayName= "${DisplayName}"
    ```

4. **Start the service** 

    Start the service (and all related dependencies - if any) by running:

    ```sh
    net stop ${SERVICENAME}
    ```

5. **Configure the service**

    Open Windows | Services (`services.msc`) | Right click on the service and select "Properties"

    Use the following settings:

    * **General | StartUp type**: Automatic
    * **Recovery | First failure**: Restart the service 
    * **Recovery | Second failure**: Restart the service 
    * **Recovery | Subsequent failures**: Restart the service 


## Frontend

### Frontend
Deployment is performed either Manual or Automatic using the GitLab CI/CD. The steps below  describe the manual deployment process. If the software should be deployed automatically, follow the steps described in Appendix C of this document.

1. **Folder creation** 

    Create the folder 
    
    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/Main
    ```
    
    and the folder

    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/app-${APP_NAME}
    ```

    ensure that the permissions for the `IIS_IUSRS` groups is allowing read access to the folder and the content

2. **Create the application pool**

    Open IIS Manager | Expand the server | Right click on Application Pool | Add Application Pool

    In the dialog use the following settings for configuration:

    * **Name**: DigitalClonePlatform_FE
    * **.NET CLR version**: .NET CLR Version v4.0.xxxxx
    * **Managed pipeline mode**: Integrated

3. **Create an IIS Site**

    Open IIS Manager | Expand the server | Right click on Sites | Add Site

    In the dialog use the following settings for configuration:

    * **Site Name**: `DigitalClonePlatform_FE`
    * **Application Pool**: use the pool created in the previous step
    * **Physical Path**: `C:/inetpub/wwwroot/DigitalClonePlatform_FE/Main`
    * **Port**: 443
    * **Host Name**: *see variables document - Hostname*

4. **Create additional bindings**

    IIS Manager | Select Site created in the previous Step | Right click | Edit Bindings

    In the dialog use the following settings for configuration:

    * **Site Name**: *see variables document*
    * **Application Pool**: *see variables document*

5. **Add applications**

    IIS Manager | Select Site created in Step 4 | Right click | Add applications

    In the dialog use the following settings for configuration:

    * **Application Pool**: DigitalClonePlatform_FE
    * **Alias**: `app-${app-name}`
    * **Physical Path**: `C:/inetpub/wwwroot/DigitalClonePlatform_FE/app-${app-name}`

6. **Build the application**

    In order to build the application, use a computer (container) with node `${NODE_BUILD_VERSION}` installed and run the following commands: 

    ```sh
    npm ci --cache .npm --prefer-offline 
    npm run build:${environment}
    ```
    
    Use dev (development), stage (stage), test (test) or prod (production), when building the application.


7. **Copy build output** 

    Copy the build output `./dist` 
    
    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/${app-name}
    ```

8. **Update the version number**

    Open the files indexing the versions 

    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/main/fe-module-versions.json
    ```

    Search for the application key name and update with the version number to be release, if the key is not present add the key.

9. **Register the application in consul**

    Register the application as micro frontend in consul, using the following `curl` request:

    ```sh
    payload=$(cat <<EOF
      {
        "ID": "${SERVICE_ID}",
        "Name": "${SERVICE_NAME}",
        "Address": "${DCP_SERVER}",
        "Port": ${SERVICE_PORT},
        "Tags": ${SERVICE_TAGS},
        "Meta": ${SERVICE_METADATA}
      }
      EOF
      )
      curl -k --request PUT \
          --header "X-Consul-Token: $CONSUL_TOKEN" \
          --header "Content-Type: application/json" \
          --data "$payload" \
          https://${CONSUL_SERVER}/v1/agent/service/register
    ```

    The values used for the service registration are part of the module specific *IV variables*.

10. **Enable CORS**

    Enable the CORS policy, by adding the following section to the `web.config` file at 

    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/${app-name}
    ```

    ```xml
   <httpProtocol> 
        <customHeaders> 
            <!--Enable debugging and testing from localhost--> 
            <add name="Access-Control-Allow-Origin" value="*" /> 
            <add name="Access-Control-Allow-Methods" value="POST, PUT, DELETE, GET, OPTIONS" /> 
            <add name="Access-Control-Allow-Headers" value="content-Type, accept, origin, X-Requested-With, Authorization, name, Origin, Cookie" /> 
            <add name="Access-Control-Allow-Credentials" value="true" /> 
            <add name="Access-Control-Allow" value="POST, PUT, DELETE, GET, OPTIONS" /> 
        </customHeaders> 
    </httpProtocol>
    ```
### Web-components

 1. **Folder creation**
 
    Create the folder 
    
    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/main/web-componnets/${module-name}
    ```
    
    and the folder

    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/app-${APP_NAME}
    ```

    Ensure that the permissions for the `IIS_IUSRS` groups is allowing read access to the folder and the content

 2. **Build the component**

    On a computer with the correct node version from the installation verification document installed ()`${NODE_VERSION}`), run the following commands:

    ```sh
    npm ci --cache .npm --prefer-offline 
    npm run build:${environment}
    ```

3. **Copy the build**

    Copy the content of the `.\dist` folder to the folder location created in step 1:

    ```plaintext
    C:/inetpub/wwwroot/DigitalClonePlatform_FE/main/web-componnets/${module-name}
    ```

4. **Register the web-component**

    Register the application as micro frontend in consul, using the following `curl` request:

    ```sh
    payload=$(cat <<EOF
    {
        "ID": "${SERVICE_ID}",
        "Name": "${SERVICE_NAME}",
        "Address": "${DCP_SERVER}",
        "Port": ${SERVICE_PORT},
        "Tags": ${SERVICE_TAGS},
        "Meta": ${SERVICE_METADATA}
    }
    EOF
    )
    curl -k --request PUT \
        --header "X-Consul-Token: $CONSUL_TOKEN" \
        --header "Content-Type: application/json" \
        --data "$payload" \
        https://${CONSUL_SERVER}/v1/agent/service/register
    ```

    The values used for the service registration are part of the module specific *IV variables*.

## Calculation node
Depending on the DCP Component, none or multiple packages may be deployed. This section needs to be executed for each package listed in the component specific variable documentation (DCP - IV Variables {{DCP Component}}) on all instances. 

1. **Install AC-DC** 

    Execute *Installation Verification Blueprint and Usage Agreement Analytical Core of Digital Clone* (EDMS ID: TEC-0178716).

2. **Configure Certificates for authentication**

    Set the environment variables to enable vault as external credential storage. 

    For usage in AC-DC edit the file at `/etc/opencpu/Renviron`

    Set the following environment variables by adding the following lines to the file 
    
    ```conf
    VAULT_ADDRESS = "vault.service.roche.com"
    VAULT_NAMESPACE = "dcp"
    ACDC_CA_PATH = ""
    ```

3. **Package Deployment**

    The package can be deployed by executing the following command in a sudo shell:

    ```sh
    R -e 'install.packages(
        "{packageName}", 
        type = "source",
        repos = list(
            "https://repository-ng.intranet.roche.com/artifactory/AC-DC-Universe",
            "https://repository-ng.intranet.roche.com/artifactory/acdc-local"
            )
        )'
    ```

    Then restart the web server using:

    ```sh
    systemctl restart httpd
    ```