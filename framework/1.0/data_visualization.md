# Data Visualization 

- [Introduction](#introduction)
- [Formula Builder](#formula-builder)

## Introduction

## Formula Builder

The formula builder consists of two different implementations:

- **Advanced Formula Builder**: This uses free text input fields, allowing the user the freedom to create more complex formulas.
- **Quick Formula Builder**: This uses a set of predefined components. The suggested fields are based on the current input in the autocomplete and on previously added elements.

**Examples**:
- If the formula doesn't have any elements yet, the available autocomplete options are: sensors, "Add number" option, or an opening bracket.
- If the previous element in the formula is a number, sensor, or a closing bracket, the autocomplete will suggest operators.
- If there is an opening bracket that has not yet been closed, a closing bracket option will be available—only if the part of the formula between the two brackets is valid.

**Frontend Validations**:
Similar frontend validations are applied to both implementations:

- All open brackets need to be closed.
- There must be at least three elements inside the formula, with one of these elements being an operator (e.g., '@sensor1+2').
- If frontend validation for the Advanced Formula Builder passes, an API call is made to the backend for additional (syntactical) validation.

The formula editor is included using the <app-formula-builder></app-formula-builder> component located in the shared library. By default, only the Quick Formula Builder is included; the advanced formula switch can be enabled by setting the 'showAdvancedFormula' attribute to TRUE.