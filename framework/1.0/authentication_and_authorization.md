# Authentication & Authorization
- [Introduction](#introduction)
- [Token types](#token-types)
    - [Server to Server Tokens](#server-to-server-tokens)
    - [User Tokens](#user-tokens)
    - [Gateway Tokens](#gateway-tokens)
- [Token payload](#token-payload)
- [Permissions](#permissions)
- [Single-Sign-On Configuration](#single-sign-on_configuration)


## Introduction
The system provides an option for the users to be authenticated via single-sign-on. If necessary single sign on instance admins can configure second-factor authentication, this is an SSO topic and outside the DCP scope.

```mermaid
stateDiagram-v2
    User --> DCP: opens
    DCP --> SSO: Session expired or inactive
    SSO --> RADA: Fetch Group membership with Prefix
    SSO --> DCPCore: Redirect to Assertion Consumer URL
    Note left of SSO: Data fields are UserId, NameID, Firstname, Lastname, Email, RADA Groups 
    DCPCore --> DCP: Redirect to OriginHeader
    DCP --> DCPCore: Checking permissions based on RADA group membership
```

Authentication is implemented using the [OAuth protocol](https://oauth.net/2/). The token settings are configured outside the compiled source code. For all the apps the **DCP core** microservice is acting as an identity provider (IdP) which is issuing and signing tokens. Tokens are issued with a default lifetime of 14 minutes in a JSON web token format ([JWT](https://jwt.io/introduction)). This way all components backend web-apis and frontend components can decode the token payload, and perform role based permission checks.

The user frontend for login is organized as an individual app, without any further responsibility. After displaying the confidentially disclaimer (in order for the user to actively acknowledge the legal statement)

## Token types
DCP core issues three different types of tokens:

* **server-to-server** tokens are globally (not bound to the site scope) and are used for communication between the different backend apis (if needed)
* **user** tokens are used for the user interaction and added as bearer-token to every endpoint in the backend which is protected by permissions
* **gateway** tokens are used for accessing data from DCP via the gateway - they only allow access to the endpoints exposed on the API gateway.

### Gateway tokens

### Server to Server tokens

### User tokens
In order to implement early invalidation of tokens (logout - prior to token expiry), DCP core extends the token checking, all tokens issued by an interactive login are added to the PersitedGrants table. When performing a logout, the entires are actively deleted - otherwise the expired records are removed by the clean-up windows service. When the core service is receiving a token, it checks the signature, issuer and checks if an entry in persisted grants exists. If not entry can be found the request is aborted and a 401 Unauthorized Error is returned.


## Token payload
The token contains the information about site specific permissions, the sites the user (or identity) has access to, the token type and some user settings (e.g. time and date formats). Details can be found below:

```json
{
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier": "wehc",
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name": "wehc",
  "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider": "ASP.NET Identity",
  "permissions": [
    {
      "Permission": "MVDAUSER",
      "SiteId": 2,
      "SiteName": "Site 1",
      "Module": "MVDA"
    }
  ],
  "sites": [
    {
      "Name": "Site 1",
      "ShortName": "S1",
      "SiteId": 2,
      "TimeZone": "Europe/Berlin",
      "IsEnable": true,
      "SiteType": 0,
      "Modules": null,
      "DataSources": null
    }
  ],
  "roles": [
    "GLODigitalClone_DEV_VV_PPM_USER"
  ],
  "userName": "wehc",
  "timeDifference": "2",
  "isGlobalAdmin": "True",
  "isGlobalApiUser": "True",
  "timeFormat": "HH:mm:ss",
  "dateFormat": "MMM dd, yyyy",
  "currentLoggedUserId": "7",
  "displayName": "Christopher Weh",
  "persistedGrantsType": "1",
  "nbf": 1721797201,
  "exp": 1721798041,
  "iss": "https://dc-dev-gxp.roche.com:8187/",
  "aud": "d8cn7105p6152z02gum10q5710274ly4"
}
```
| Field        | Value                                         |
|-----------------------|-----------------------------------------------|
|permissions       |  An array of permissions added to the token        |
|sites      |  An array of sites the  current entity has access to      |
|userName       |  The user name identifying the user on the domain     |
|timeDifference    |  The time differences of the user to the UTC time                 |
|isGlobalAdmin      |  The claim indicating if the user is a global admin   |
|isGlobalApiUser            |  The claim indicating if the user has API access   |
|timeFormat       | The user setting for displaying the time format     |
|dateFormat       |  The user setting for displaying the date format    |
|currentLoggedUserId       |  The DCP internal userId of the current logged user |
|displayName       |  The display name used to present user information in DCP   |
|persistedGrantsType      |  The token type used                                    |
|nbf      |  The unix timestamp indicating not before time (first timestamp when the token is valid)  |
|exp     |  The unix timestamp indicating the expiry time of the token  |
|iss     |  The issuer of the token                                     |
|aud     |  The audience of the token to be accessed                    |

## Permissions
The DCP permission model is based on the user, site and role considerations. Permissions need to be manged outside the DCP application - the usage of Active directory is strongly recommended. Groupmembership needs to be returned from the authentication provider (SSO). The core service uses the following pattern to translate active directory groups into DCP specific permissions - which are used inside the application. DCP distinguishes between module dependent and independent roles. 

### Module independent permission
Module independent roles are used to control the access to manufacturing data and are a minimal requirement to access/see any data. Module independent permissions have the following pattern in the active directory {SITE_SHORT}_VIEWER and {SITE_SHORT}_ADMIN. The viewer is the minimal access level used for access to the data. The site amin permission is sufficient should be able to view and edit data at a manufacturing site. In order to comply with segregation of duties this excludes DCP configurations which are restricted to global administrators.

### Module dependent permissions


### Global permissions
There are two exceptions to the design pattern that permissions are always site based. Those exceptions are global roles. One role is the GLOBAL_ADMIN role which can be mapped to a specific active directory group via configuration. This roles enables the runtime configuration of DCP (audit trail access, configuring data source, etc.). The second role is the GLOBAL_APIUSER which allows access to the DCP resources using service tokens. This is a prerequisite for quiring a service token, the permission on the service token level is controlled from the application (see gateway tokens).

## Single Sign On Configuration

## Database representation

### Clients records

```mermaid
erDiagram

Clients  {
 	int(4) ID "UniqueId"
	nvarchar ClientId "ClientId used for the DCP server to server communication"
	nvarchar Secret "Secret used for the server to server authentication"
	nvarchar(400) Name "Name of the client/secret pair for debugging"
	tinyint(1) ApplicationType 
	bit(1) Active "Can this client/secret pair be used to aquire a token"
	int(4) RefreshTokenLifeTime 
	nvarchar AllowedOrigin 
	nvarchar(800) Description "Description of the client"
	nvarchar Permissions 
}


UserClients  {
 	int(4) ID "UniqueId"
	nvarchar ClientId "ClientId used for authentication for the REST access"
	nvarchar Secret "Hashed secret, for REST client authentication"
	nvarchar(400) Name "User defined client name for client identification by the user"
	nvarchar(800) Description "Optional further information for client description"
	tinyint(1) Status "Status of the clientIds, disable clients can't be used"
	nvarchar Permissions "The permissions granted by the user to this client"
	nvarchar(300) UserName "User name, created this cleint/secret pair"
	int(4) UserId "The userId to which th clientId is linked to"
	datetime(8) DateCreated "Creation datetime in UTC"
	datetime(8) DateUpdated "Update datetime in UTC"
	int(4) UserIDCreated "The userId who created the entity"
	int(4) UserIDUpdated "The userId who made this change"
}

PersistedGrants  {
 	int(4) ID "UniqueId"
	nvarchar(300) UserName "Username of the login user"
	datetime(8) CreationTime "Creation datetime in UTC"
	datetime(8) Expiration "Expiration datetime of the session token (14 min)"
	tinyint(1) Type 
}
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  User clients and associated permissions with access to DCP                                                    |
|Data classification    |  Convenience records                          |
|Change Tracking        |  No change tracking   |
|Audit Trail            |  N/A              |
|Retention period       |  3 years                                     |


### User records

```mermaid
erDiagram

Users  {
 	int(4) ID "UniqueId"
	nvarchar(300) UserName "Username by RADA"
	datetime(8) DateCreated "Creation datetime in UTC"
	datetime(8) DateUpdated "Update datetime in UTC"
	nvarchar(500) DisplayName "Displayname as provided by RADA"
	nvarchar(500) FirstName "Firstname by RADA"
	nvarchar(500) LastName "Lastname by RADA"
	nvarchar(500) Description 
	nvarchar(500) Email "Email address as provided by RADA"
	int(4) CurrentSiteID "Relation to the last used site"
	nvarchar(100) TimeFormat "Timeformat 24:00 vs. 12:00 time representation"
	nvarchar(100) DateFormat "Dateformat Europe vs US timeformat"
	nvarchar Roles "The roles the user has according to RADA"
	nvarchar LoginLink 
}
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  The user table is acting as a cache for information retrieved from RADA                                                      |
|Data classification    |  Official records                            |
|Change Tracking        |  No change tracking   |
|Audit Trail            |  N/A              |
|Retention period       |  10 years                                     |


### Roles and Permission Records

```mermaid
erDiagram

Site ||--|{ Roles : "has"
Roles ||--|| RolesToPermissions: "maps"
Permissions ||--|| RolesToPermissions: "maps"

Roles  {
 	int(4) ID "UniqueId"
	nvarchar(500) Name 
	int(4) SiteID 
	datetime(8) DateCreated "Creation datetime in UTC"
	datetime(8) DateUpdated "Update datetime in UTC"
	int(4) UserIDCreated "The userId who created the entity"
	int(4) UserIDUpdated "The userId who made this change"
}

RolesToPermissions  {
 	int(4) ID "UniqueId"
	int(4) PermissionID 
	int(4) RoleID 
}

Permissions  {
 	int(4) ID "UniqueId"
	nvarchar(500) Type 
}
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  The assignment of roles and permissions during a user session                                                     |
|Data classification    |  Cache                           |
|Change Tracking        |  No change tracking   |
|Audit Trail            |  N/A              |
|Retention period       |  N/A                                     |