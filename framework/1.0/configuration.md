# Configuration
- [Introduction](#introduction)
- [Frontend](#frontend)
- [Backend](#backend)
    - [Configuration Sources](#configuration-sources)
    - [WebConfig Transformers](#configuration-sources)

## Introduction

## Frontend
Managing environment variables is crucial during the build process, as they ensure the correct configuration and seamless execution of tasks. In this context, environment variables are consumed and set dynamically to tailor the build to specific requirements. The build task reads a settings.json file, which contains essential configuration details, allowing it to adapt and perform its functions accurately. The script below can be used to set variables during the CI build.

```sh
sed -i "s|CLI_DEPLOY_URL|https://${DCP_SERVER}/${APP_NAME}/|g" ${CI_PROJECT_DIR}/angular.json
sed -i "s|CLI_DEPLOY_URL|https://${DCP_SERVER}/${APP_NAME}|g" ${CI_PROJECT_DIR}/src/fonts-build.scss
jq -n --arg api "https://${DCP_SERVER}" \
    --arg baseapp "https://${DCP_FE_SERVER}/"  \
    --arg production false  \
    --arg apiEnv true  \
    --arg policy "https://www.roche.com/privacy_policy.htm" \
    --arg gateway "https://${DCP_GATEWAY_HOST}" \
    --arg portcheck "roche.com:" '{"apiUrl":$api, "baseAppUrl":$baseapp, "apiEnv": $apiEnv, "production": $production, "privacyPolicyUrl":$policy, "backendPortCheckUrl":$portcheck, "apiServiceUrl":$gateway}' > settings.json
npx kendo-ui-license activate
npm run build
```

The used variables have the following meaning:

| Variable   |   Data Type   | Description        | Default Value  |
|------------|---------------|--------------------|----------------|
| production | boolean       | Should the angular app be compiled in production mode| |
| apiUrl     | string        | The URL of the backend API where data can be consumed | |
| translationsUrl | string   | Path to assets folder of the app when is deployed |    |
|filterMicroservice | string | What microservice port to be used for loading global filter | |
| isAppValidated | boolean   | An indicator if the FE app is validated or not, used in the GxP indicator |
| appName     | string       | The name of the app used in URL, for builds, etc. | |
|version      |   string     | The version number of the frontend app | |
| modulesVersions | object   | Used only in login app | |
| domains     | string[]     | List of domains used only for localhost development login | |

## Backend

### Core - API
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
|General|
| CleanHistoryTables | Boolean | Flag if SQL temporal tables`data needs to be deleted | false
| MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| UserClientIdStringLength | Integer | Length of characters used by security algorithm in order to generate client id| 20 |
| UserClientSecretStringLength | Integer | Length of characters used by security algorithm in order to generate client secret| 40 |
| JWTEndpointPath | String | Host of the core service to acquire a token | /token |
| AccessTokenExpireMinutes | Integer | Access token expiry time in minutes | 14 |
| FieldsForRemove | String | Semi colon separated string of properties names that contains sensitive information | |
| RefreshAccessTokenExpireMinutes | Integer | Refresh access token expiry time in minutes | 14 |
| DevelopmentMode | Boolean | Should the app, been started on localhost | false |
| ActiveLogDebug | Boolean | | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| EnableSwagger | Boolean | Should a swagger with all endpoints should be supported by the system | false |
| GlobalAdminRole | String | RADA role with admin rights in DCP | |
| GlobalApiUserRole | String | RADA role with API Gateway rights in DCP | |
| SendFeedbackTo | String | The e-mail address of the DCP team | |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| BatchTotalCountOnPISingleRequest | Integer | OSI Soft PI max count of batches per single HTTP request | 3000 |
| SSOIssuer | String | Single sign on issuer | |
| SSOAssertionConsumerServiceUrl | String | Single sign on call back url in DCP | |
| SSOSamlEndpoint | String | Single sign on login url | |
| SLOSamlEndpoint | String | Single sign on logout url | |
| RADAPrefix | String | DCP RADA prefix | |
| OriginHeader | String | The origin header used | |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPAPIGatewayBaseUrl | String | Host of the DCP gateway service | |
| DCPAPIGatewayConfigurationPath | String | Host of the DCP gateway service configuration path | /configuration |
| DCPAPIGatewayAvailableConfigurationPath | String | Local directory DCP gateway service configuration path | E:\\DigitalClonePlatformData\\APIGateway\\ |
| DCPAPIGatewayActiveConfigurationPath | String | Local directory DCP gateway service active configuration path | C:\\inetpub\\wwwroot\\DigitalClonePlatform_BE\\APIGateway\\Configuration |
| Headers |
| DCPAPIGatewaySwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAuthorizationServerClientPath | String | The path to the authorization server| dev/authorizationServer/ |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| VaultTokenPath | String | Vault path containing the token secrets | dev/token |
| VaultEncryptKeyPath | String | Vault path containing the encryption keys | dev/encrypt |
| VaultActiveDirectoryPath | String | Vault path containing the RADA secret pairs | dev/ldap |
| VaultApiGatewayPath | String | Vault path containing the DCP gateway secret pairs | dev/apiGateway |

### Core - Email Processor Windows Service
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| Mailhost |
| SMTP | String | Base URL of the SMTP server |  |
| SMTPPORT | Integer | The port of the SMTP server | 25 |
| MailSender | String | The mail sender used for sending e-mails | |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |


### Core - Active Batches Windows Service
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| Service setup |
| CheckActiveBatches | Integer | Loop frequency of the check active batch service in milliseconds | 20000 |
| EnableServiceMails | Boolean | Should service e-mails (e.g. long running event frames) should be sent | false |
| EnvPrefix | String | The prefix used to build/construct system URLs | dc-dev | 
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| SendFeedbackTo | String | The e-mail address of the DCP team |  |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| Headers |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultEncryptKeyPath | String | Vault path containing the encryption keys | dev/encrypt |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |

### Core - Clean Up Windows Service
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| MachineName | String | DNS for the validated server node | |
| Service setup |
| RemoveEmailQueuesInterval | Integer | Loop frequency in milliseconds for cleaning e-mails | 86400000 |
| RemovePersistedGrantsInterval | Integer | Loop frequency in milliseconds for cleaning persisted grants | 86400000 |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultEncryptKeyPath | String | Vault path containing the encryption keys | dev/encrypt |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |

### Core - EventBus
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| MachineName | String | DNS for the validated server node | |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| DCPBasicBaseUrl | String | Host of the data visualization service | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |

### Basic - API
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| CleanHistoryTables | Boolean | Flag if SQL temporal tables`data needs to be deleted | false
| MachineName | String | DNS for the validated server node | |
| DigitalClonePlatformCoreDbContext | ConnectionString | Connection string skeleton to access the component database, secrets removed and stored in vault ||
| IsModuleValidated | Boolean | Flag if the backend code is validated or not | true |
| FieldsForRemove | String | Semi colon separated string of properties names that contains sensitive information | |
| FileUploadPath | String | The UNC to the storage on disk | E:\\DigitalClonePlatformData\\ |
| Error sanitization | 
| DevelopmentMode | Boolean | Should the app, been started on localhost | false |
| ActiveLogDebug | Boolean | | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| EnableSwagger | Boolean | Should a swagger with all endpoints should be supported by the system | false |
| DCP Business Constants |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| SendFeedbackTo | String | The e-mail address where feedback is sent to | |
| DCPShareLinkPage | String | URL of the DCP share link page | /share-link/ |
| DCPCDMPage | String | URL of the DCP continuous data monitoring page | |
| DCPBDSPage | String | URL of the DCP batch data studio page | |
| NotifcationPage | String | URL of the DCP notification page | |
| NotifcationListPage | String | URL of the DCP notification list page | |
| NotifcationEditPage | String | URL of the DCP notification edit page | |
| NotificationConditions | String | Comma separated string of notification conditions | =;!=;&gt;;&gt;=;&lt;;&lt;=;in;notin | 
| Operators | String | Comma separated string of supported operators | +;-;*;/ |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| OriginHeader | String | The origin header used | |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| DCBaseUrl | String | URL of the DCP | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultDataSourceMountPoint | String | The path to the source mount point | ext-datasource/ |
| VaultDataSourcePath | String | The path to the source path | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/basic |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/basic |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultTokenPath | String | Vault path containing the token secrets | dev/token |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |

### Basic - EventBus
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| MachineName | String | DNS for the validated server node | |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is comming from DCP API Gateway service | X-DCPGateway-Key |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| DCPBasicBaseUrl | String | Host of the data visualization service | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |

### Basic - Notifications
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
| General|
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| MachineName | String | DNS for the validated server node | |
| DCPCDMPage | String | URL of the DCP continuous data monitoring page | |
| DCPBDSPage | String | URL of the DCP batch data studio page | |
| SendFeedbackTo | String | The e-mail address of the DCP team |  |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| DCPShareLinkPage | String | URL of the DCP share link page | /share-link/ |
| NotifcationPage | String | URL of the DCP notification page | |
| NotifcationListPage | String | URL of the DCP notification list page | |
| NotifcationEditPage | String | URL of the DCP notification edit page | |
| NotificationFixedInterval | Integer | Loop frequency in milliseconds for checking notifications | |
| DCBaseUrl | String | URL of the DCP | |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGatewaySwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGatewayHeaderName | String | The name of the Header used for check if HTTP request is comming from DCP API Gateway service | X-DCPGateway-Key |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultServerUri | String | The address for the vault instance | |
| VaultClientPath | String | The path to the client ID/client secret pairs| dev/authorizationServer/core |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | dev/db/core |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | dev/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | dev/headers |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |


### Configuration Sources
Every DCP component has two main configuration locations:

* Module specific configuration object in ConfigurationItems.json file in the Core file
* Specific web.config file
* Service specific app config files
* ocelot.{moduleName}.json (see the [gateway filebuilder](/api_service/1.0/gateway_filebuilder) on how to easily generate this file for .NET projects)

### WebConfig Transformers
The DCP components uses publish profiles to transform the web.config files for the environments. For each environment, a dedicated build profile is created. Build profiles are automatically selected by the CI based on the branch. Per environment, the following transformations are created:

* {Web/App}.Dev.config
* {Web/App}.Stage.config
* {Web/App}.Test.config
* {Web/App}.Prod.config 

Web.config sections containing sensitive information are encrypted using a key container during the publish process. For encrypting aspnet_regiis utility is used. Every component has it's one web.config files. Inside the components there

### Configuration Items

ConfigurationItems is the mapping file, used by the core service, to enable a central maintenance and administration of different DCP components. This file is part of the core repository and only configured once, centrally. Each Module is configured using an object in the following format:
Example, MVDA configuration object:

```json
{
    "Name": "MVDA",
    "IsValidated": false,
    "DataTypes": [1],
    "Resources": [{
    "Port": "8188",
    "ResourceURL": "api/BatchEvent/GetBatchEvents",
    "Type": 1,
    }],
    "RPackages": ["acdcMvda"],
    "AdminActions": [{ "Value": "mvda-batchDatabase", "Text": "Batch Database Backfill" } ]
}
```
The modules in this file is used to provide the available modules, their names for administration purposes. The keys have the following context:

* **Name**: The name of the module used for reference in code and in RADA Groups, roles, etc.
* **IsValidated** Boolean validation status of the current module
* **DataType** An array of data types required by the module, based on this mappings need to be created by the user, used are the values from DataType.cs
Resources Sometimes it can't be avoided that shared services (e.g. ActiveBatches) require entities from other dependent services. In these cases Resources are mapped using this property, request URL is build dynamically based on Origin-Header , the fields Port and Resource URL: Origin-Header-{IsValidated ? gxp : nongxp}:{Port}{ResourceUrl}
Resources to be checked are filtered by the Type, property. This uses the MicroservicesMapType.cs enumeration
* **AdminActions** An array of available admin actions, all admin actions have two properties Value and Text, the Value contains the web component from the FE implementing the action and the Text the display string for the user
* **RPackages** The R package Name used for the module