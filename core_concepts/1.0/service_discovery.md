# Service Discovery

- [Introduction](#introduction)
- [Implementations](#implementations)
    - [Consul](#consul)
    - [File based](#file_based)

## Introduction
To enable dynamic extension and customization of other DCP installations, modules are managed as services outside the application code. The process of configuring and aggregating these services at runtime is known as the **service discovery problem**.  

In DCP, service discovery is abstracted, allowing different implementations to be configured as needed. A service discovery mechanism must provide access to:  

- A **list** of currently active services, including tags and additional metadata.  
- A **shared configuration** for service settings.  


## Implementations
The DCP framework implements the following drivers for service discovery:

- HashiCorp [Consul](https://www.consul.io/)
- File based implementation 

As a default DCP is configured to use `Consul` as a service discovery provider.

### Consul
Consul is a service networking solution that provides service discovery, configuration management, and health checking for microservices and distributed systems. It enables automatic service registration and discovery, allowing services to dynamically find and communicate with each other. Consul also includes features like key-value storage, secure service-to-service communication via mTLS.

#### Service Registration  
Service registration can occur at two points in the process, depending on whether vertical scaling is expected.  

- If **vertical scale-out** is expected, service registration must take place during the application's startup.  
- If **no vertical scale-out** is expected, registration occurs during deployment as part of the CI/CD process.  

#### Security  
HTTP traffic to the Consul instance is disabled, enforcing HTTPS as the only allowed protocol. The Consul instance uses an Access Control List (ACL) with role-based policies. The following policies are defined:  

- **`service-management`**: Grants read access to the shared configuration and read/write access to the service prefix.  

### Conventions

**Naming**
The services should be named using the following convention of tags: `${tier}-${module}-${category}`. Names should be all lower-case. An example: `frontend-mvda-microfrontend`.

The service Id should be assigned based on the following routes:

- if vertical scale out is not expected, e.g. frontend components the service Id, is assigned based on `${service-name}-${env}-${GxP status}`, e.g. `frontend-mvda-microfrontend-dev-gxp`
- if vertical scale out is might happen, service names should be appended by a md5 hash. The hash depends on the address and the port where the service is deployed to and should be calculated using the following convention:

```sh
#!/bin/bash

address="dcp.example.com"
port="7104"

# Combine strings and calculate the MD5 hash
combined="${string1}${string2}"
echo -n "$combined" | md5sum | awk '{print $1}'
```

An example would be backend-mvda-dev-gxp-frontend-microfrontend-mvda-dev-gxp

**Tagging**
In order to query the service registry and provide some additional information, consul provides the concept of tags and meta data. Tags are a simple list of strings, while meta data offers the option to store more complex object structures. Every service in DCP registered by the service provider should provide the following tags:

> {warning} All tags are compared case insensitive, nevertheless tags should be all lowercase by convention.

* the environment should be indicated by the slag, it needs to be one of: `dev`,`stage`,`test`,`prod`,`open`,`edu`
* the GxP status of the component, should be one of: `gxp`, `nongxp`
* the tier of the service, currently the following tiers are identified: `frontend`,`backend`,`devops`  
* the module slag, which identifies the module group, one of, e.g. `framework`, `mvda`,`ccheck`,`chromta`,...

Depending on the service tier, some additional tags are mandatory:

* Services in the frontend tier:
    * indicating the category of the registered component, this is one of: `web-component`,`microfrontend`

**Meta Data**
The meta data is used to provide information on the functionality or requirements of a particular module. The following meta data is mandatory for all registered services:

| Key          | Description                       | Conventions                        |
|--------------|-----------------------------------|------------------------------------|
|Version       |The version number of the registered component | Use [semantic versioning](https://semver.org/) in a three digit format e.g. 1.2.1 |


The following meta data is mandatory for all services in the frontend tier:

| Key          | Description                       | Conventions                        |
|--------------|-----------------------------------|------------------------------------|
|Url           |The url/route where the main.js is located, which can be used by single spa to load the component | use case-insensitive routes e.g. `/app-chrom-ta` |

The following meta data is mandatory for all services in the backend tier:

| Key          | Description                       | Conventions                        |
|--------------|-----------------------------------|------------------------------------|
| DataTypes    | The list of logical data types which are mandatory/optional for the module | use the following structure: e.g. `[{"Type":1,"Mandatory":true,"AllowMultiselection":false},{"Type":4,"Mandatory":true,"AllowMultiselection":false}]`  |
| RPackages   | The list of libraries used by the calculation node, implementing the business functionality | use the following structure: e.g. `["acdcMvda"]`  |
| Resources   | The list of resources provided by the functionality, examples are: batch-subscription, audit-trail, etc. | use the following structure: e.g. `[{"Type":1,"ResourceURL":"api/BatchEvent/GetBatchEvents"}]`  |
