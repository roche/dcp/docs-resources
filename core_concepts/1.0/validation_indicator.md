# Validation indicator
- [Introduction](#introduction)
- [Implementation](#implementation)
    - [Backend](#backend)
    - [Frontend](#frontend)

## Introduction

Due to it's platform nature, DCP hosts GxP and non-GxP modules, one major requirement is to indicate the GxP status to the user. GxP status depends on the following items:

* The validation of the data provider /data source when the query is executed. In cases where records are stored in DCP permanently (this is the exception) the data source check has to be performed based on the data source configuration at data request time. The configuration is stored as an additional flag in the data source table and can be configured by an administrator user via the UI.

* The qualification of the infrastructure includes the hosting of the DCP application servers and the AC-DC calculation nodes. The configuration is stored as an additional flag in the modules table and can be configured by an administrator user via the UI. The qualification status of the calculation node is only considered if the instance is used in the dedicated request.

* The validation of the code base/module. As packaging of FE and BE may be different, the validation status is individual set for the app (FE) and the API (BE). In the BE this configuration is added to the web.config file with the key IsModuleValidated. In FE apps the definitions are inside the environment specification (e.g. src/environments/environment.dev.ts) where the property name is isAppValidated.

* If DCP stores raw data as official records (e.g. MVDA model) instead of storing only the configuration (e.g. notifications). The validation status of the entity has to be considered. Validation indicators of entities, apply to the following entities from the data design e.g. Model, BatchCalculation

All the factors above are connected using a boolean AND operation, as soon as a single checked factor is false the response is considered to contain non-validated data. For requests containing mixed entities e.g. an overview of batch calculations, the status is aggregated on runtime checking all the state returned.

## Implementation

### Backend
Every backend has the responsibility to provide information about the validation status as an additional header on every request. The header needs to contain the following elements:  [ IsModuleValid, IsDataSourceValid, IsRValid, IsEntityValid ]. While the module status always needs to be returned the other three properties (datasource, calculation engine, entity) can be set to undefined as they might be not in-use for the current request.

### Frontend

The presentation of the GxP label in the frontend relies on 2 data sources:

* It checks if the application is marked as a validated in the environment.ts file.
* The NGRX State Object with Keys that represent loaded URLs. The values contain a data array with 4 boolean elements that represent validation status of a component:  [ IsModuleValid, IsDataSourceValid, IsRValid, IsEntityValid ]

On every change of the URL a new key is added into the store object with a new array using saved data from GetUserSite or SetUserSite IsModuleValid element.

On every API call the GxpInfoInterceptor checks if the response headers contain isValidated data, and if there are some True/False values the state object for the current URL key is updated using the URL as a key.

If the app is not validated or in the state for the current URL contains a false element, the validation indicator is set to non-GxP. If the app is validated and the state for the current URL contains no false elements, the validation indicator is set to GxP.

On some specific pages, the label is set to Undefined, as the page can contain elements that are validated and some that are not *e.g: Dashboard, notifications etc*.