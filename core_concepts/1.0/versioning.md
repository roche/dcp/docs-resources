# Versioning
- [Introduction](#introduction)
- [Frontend versioning](#frontend-versioning)
- [Backend versioning](#backend-versioning)
- [Branching strategy](#branching-strategy)

## Introduction
As backend and frontend are decoupled, the components maintain their individual version numbers. DCP uses 3 digit versioning following the semantic versioning approach. Documentation updates for individual builds are not maintained.

## Frontend versioning
Package versions of an individual app is maintained inside the package.json of the individual app. During build, the file fe-module-versions.json file on the document root needs to be updated. This file contains all active/currently deployed frontend version numbers. The update is happening automatically inside continuous-integration. If needed individual frontend apps can consume the version numbers from the file at the document root.

## Backend versioning
Backend versions are maintained using the conventions of the corresponding programming language (.NET uses the AssemblyProperties.cs file). Every backend app needs to implement an endpoint returning the version number. The version endpoint should be exposed at /api/Version/GetBEVersion.


## Branching strategy
In DCP we use 3 main branches:

*  `develop` branch for new development
* `test` branch for automation testing
* `main` branch for Production deployment

Also, we use two secondary branches:

* `stage` branch for stable environment that can be used for automation test development
* `education` branch that is used as a training environment

```mermaid
						%%{init: {  'theme': 'base', 'gitGraph': {'mainBranchName': 'develop', 'showCommitLabel': false}} }%%
gitGraph
      commit
						branch new-feature
						commit
						checkout develop
						commit
						branch stage
						commit
						branch education
						commit
						checkout stage
						branch test
						commit
						checkout new-feature
						commit
						checkout test
						branch main
						commit
						checkout develop
						commit
						checkout new-feature
						commit
						checkout develop
						merge new-feature
						commit
						checkout test
						checkout main
						checkout develop
						commit
						commit
						checkout stage
						merge develop
						checkout education
						merge stage
						checkout test
						merge stage
						checkout main
						merge test
						checkout test
						checkout develop
						commit
						checkout stage
						merge develop
						checkout education
						merge stage
						checkout test
						merge stage
						checkout main
						merge test
						checkout test
						checkout main
						checkout develop
						commit
						commit
```