# Operations

- [Introduction](#introduction)
- [Logging](#logging)
    - [API](#api)
    - [Log forewarding](#log-forewarding)

## Introduction

## Logging

### API
To satisfy security requirements, non-custom error messages are not forwarded to the user in a productive environment, instead API Log messages are written to LogFiles on the server. This is implemented by overwriting the UnhandledExceptionFilter class. In the overwrite unhandled exceptions, error messages are checked and replaced with the message "There is an error" for the user, while the stack trace is written to the log file. In order to allow easier debugging, an error ID is generated. This error ID is written to the log file and is returned to the user, this way the operation team can link error reports from users to actual error messages.

In the development environment, detailed error are exposed. This behavior is controlled by the DetailErrorReturn property in the web.config, which is set based on the environment using web.config transforms.

### Log forwarding
Log files need to remove from the runtime environment to comply with validation information. Log forewarding is not implemented in DCP directly, third party tools should be used. We recommend the usage of [Grafana Loki]() which can be used as a operational source for logfiles.

### Service
Service log information is written to the Windows Event Viewer. Every DCP service is registered with an individual event source. The eventsource is named using the {{ServiceName}}DCPServiceLog. Events logged by the service contain the error message and the stack trace. Sensitive information (e.g. credentials) is removed from the log information. Information and Debug messages can be added to the logs if considered useful by the developer.