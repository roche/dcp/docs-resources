# Installation

- [Introduction](#introduction)
- [Install Calculation Engine](#install-calculation-engine)
    - [Recommended Calculation Engine Hardware Specifications](#recommended-hardware-specifications)
    - [Additional Preparation Calculation Engine](#additional-preparation-calculation-engine)
    - [Calculation Engine Installation Steps](#calculation-engine-installation-steps)
    - [Deploy required Packages](#deploy-required-packages)
- [Install Application](#install-application)
    - [Recommended Hardware Specifications](#recommended-hardware-specifications)
    - [Server Preparation](#server-preparation)
    - [Additional Preparation](#additional-preparation)
    - [IIS Installation](#iis-installation)
    - [.NET Installation](#net-installation)
    - [Optional: php Installation](#optional-php-installation)
    - [Service Installtion](#service-deployment)
    - [Frontend Installation](#frontend-installation)
- [Environment configuration](#environment-configuration)
    - [Configuration File](#configuration-file)
    - [Angular specifics](#angular-specifics)

## Introduction

In the following, we embark on a journey to deploy our application, navigating through a landscape marked by the intricacies of a microservice architecture. As we delve into the installation process, be prepared for a slightly more technical endeavor, where each step is meticulously crafted to ensure the seamless integration of our application's components. Embrace the challenge, for with it comes the opportunity to harness the power of modern infrastructure and unlock the full potential of our distributed system.

## Install Calculation Engine

The following steps are required to install and configure the underlying Calculation Engine for the Data Computation Platform.

### Recommended Calculation Engine Hardware Specifications

- LINUX server, preferred with RHEL version 8.2
- Disk space of 50 GB
- 2 core CPU
- 8 GB RAM
- Home drive (mount point /home/users/{userid}) with min 7GB of free space

### Additional Preparation Calculation Engine

- Create SSL Certificate with available public key (`.pem`)
- Ensure that uer has root previleges
- Ensure that git is installed on server

### Calculation Engine Installation Steps

Perform the following steps to install the calculation engine.

1. Login to server as admin
2. CD to home directory
3. Download latest source code files
  `git clone http://gitlab.com/digitalclone/acdc/core.git acdc`
4. Copy the certificates to `<sourceFileDir>/acdc/certs`.
5. Switch into the `acdc` folder: `cd <sourceFileDir>/acdc`.
6. Execute the installation script: `sudo bash build.sh`.
7. Provide the requested `username`, `password` and `email` during the installation process.
8. Optional: Update trusted IP addresses by modifying `/etc/httpd/conf/httpd.conf`.
9. Install R client via 
  `R –e ‘install.packages("acdcClient", type = "source", repos = list("https://gitlab.com/artifactory/AC-DC-Universe", "https://gitlab.com/artifactory/acdc-local"))’`
10. Install Reporting Engine Templates via
  `export PATH="/usr/local/texlive/2021/bin/x86_64-linux:$PATH"`
  `tlmgr conf auxtrees add /usr/lib/acdc/templates/texmf`
  `tlmgr conf auxtrees add /usr/share/R/texmf`
11. Optional: Set environment variables edit `/etc/opencpu/Renviron` and add the following lines:
  `CURL_SSL_BACKEND = "openssl"`
  `ACDC_CA_PATH = "<sourceFileDir>/acdc/certs/{certname}.pem "`
12. Restart server by executing `systemctl restart httpd`.

Now you should be ready to continue with the installation of backend and frontend of the Data Computation Platform.

### Deploy required Packages

Depending on your desired installation scope, none, one or multiple R packages need to be deployed to the prepared calculation engine instance. Therefore, the following steps need to be completed:

1. Login to server
2. Execute the following command after replacing `{{packageName}}` with the corresponding package name:
 `R –e 'install.packages("{{packageName}}", type = "source",repos = list("https://domain.com/artifactory/AC-DC-Universe","https://domain.com/artifactory/acdc-local"))'`
4. Restart server by executing `systemctl restart httpd`.

The following official packages for DCP are available:

- Basic:
- ChromTA:
- DReAM:
- MVDA:
- SAW:

## Install Application

The following steps are required to install and configure the Data Computation Platform and its components.

### Recommended Hardware Specifications

The following infrastructure components are required:

- Microsoft Windows Server 2019 (or newer) Operating System for GxP and non-GxP instances

### Server Preparation

- Install SSL Certificate(s) for GxP, non-GxP and user facing DNS
- Ensure that IIS is installed and IIS User (most often IIS_IUSERS)
- Ensure that IIS User has access to private keys of SSL Certificate(s)
- Ensure that executing user has administration previleges
- Optional: Install OpenSSH server and configure, if deployment via CI/CD is desired

### Additional Preparation

- Setup the desired identity provider and configure application integration by adjusting the `web.config` file.
- Setup secret configuration provider (Hashicorp Vault)
- Setup message broker (RabbitMQ)

### IIS Installation

Open the Windows Server Manager Application and add the following roles and features.

In the Server Roles:

-	Web Server (ISS)

In the Web Server Roles:

-	Basic
-	Default Document
-	HTTP Errors
-	Static Content
-	Health and Diagnostics
-	HTTP logging
-	Request Monitor
-	Tracing
-	Performance
-	Static Content Compression
-	Dynamic Content Compression
-	Security
-	Request Filter 
-	Windows Authentication 
-	Application Development (everything)
-	Management Tools
-	IIS Management Console

### .NET Installation

### Optional: php Installation

> {info} The installation of php is required for the Simplified Analytics Workbench (SAW) backend only.

## Environment configuration

Environment configuration is applied during the build, my modifying the build settings for frontend applications located at `project-root/settings.json`.

This file contains a few strings and booleans defining the environment specific logic. See an example below:

```json
{
    "apiUrl": "https://api-domain.com", 
    "baseAppUrl": "https://domain.com/", 
    "production": true, 
    "privacyPolicyUrl": "https://domain.com/privacy_policy.htm", 
    "backendPortCheckUrl": "domain.com:"
}
```

This file needs to be generated before triggering the `npm build` command (either manually or within the CI). During **local development** this file need to be **added and configured manually**. The file is excluded via `.gitignore`, therefore not included into repositories.

### Configuration file

The configuration file `settings.json` has the following impact:

| Property | Type | Angular Apps | VueJS Apps | Description                                                                                                          |
|------|-----|-----------------:|---------------:|----------------------------------------------------------------------------------------------------------------------|
|apiUrl| string |          &#9745; |        &#9745; | Current application BE URL like `"https://domain.com"`                                                               |
|baseAppUrl| string |          &#9745; |        &#9744; | Base app deploy url (with trailing slash) like `"https://domain.com/"`                                                |
|privacyPolicyUrl| string |          &#9745; |        &#9744; | Url to applicable privacy policy "privacyPolicyUrl": `"https://domain.com/privacy_policy.htm"` used inside Login page. |
|backendPortCheckUrl| string |          &#9745; |        &#9744; | Needs to be in format `"domain.com:"` so we can use it in interceptor, to assume if there is an port part of the url. |
|production| string |          &#9745; |        &#9744; | Base app deploy url (with trailing slash) like `"https://domain.com/"`                                                |

### Angular specifics

There is one place in Angular Applications where `settings.json` cannot be used as the configuration source, tis is the `angular.json` file. Therefore placeholders which are replaced during the build are used in the file. The placeholders are replaced prior to the build. This can be done manually or using scrips e.g. sed. The current replacement can be done using the following commands:

```sh
sed -i "s|CLI_DEPLOY_URL|https://${DCP_SERVER}/${APP_NAME}/|g" ${CI_PROJECT_DIR}/angular.json
```

The following placeholders are used:
* `CLI_DEPLOY_URL` which is assigned at `configuration.production.deployUrl` containing the application root, where the app should be deployed.
