# Introduction

- [Data Computation Platform](#data-computation-platform)
- [Documentation Structure](#documentation-structure)
    - [General Concept](#general-concept)
    - [Modular Approach](#modular-approach)
    - [Testing & Containerization](#testing-amp-containerization)
- [Licenses](#licenses)
- [Next Steps](#next-steps)

## Data Computation Platform

The [Data Computation Platform (DCP)](https://open-dcp.ai) allows for advanced analytics, reporting and real-time monitoring of manufacturing processes, scaled across the global manufacturing network of pharmaceutical companies. [DCP](https://open-dcp.ai) was initally developed internally at Roche/Genentech and became the first of its kind open source data analytics tool designed to meet the requirements in the regulated pharmaceutical environment.

> {warning} We want to emphasize that we are currently in the early phases of our open-sourcing process. As such, the content provided is subject to change over time as we refine and expand our offerings. Your feedback and contributions are invaluable in shaping the evolution of this project. Thank you for your understanding and support as we work towards creating a more collaborative and transparent environment.

## Documentation Structure

Understanding the intricate architecture of our application requires a comprehensive documentation structure, where some level of business logic unavoidably intertwines with the app's documentation to ensure clarity and coherence, vital for comprehension.

### General Concept

Our documentation aims on offering a seamless journey into our platform, starting with a simple [Installation Guide](/{{section}}/{{version}}/installation) designed to swiftly onboard users. Delve deeper, and you'll find a thorough introduction to core concepts, laying a solid foundation for understanding our application's architecture. Exploring further, we illuminate our powerful calculation engine and framework, essential components driving the functionality of our system.

### Modular Approach

Diving into the intricacies, we've structured individual sections per module ([ChromTA](), [DReAM](), [MVDA](), [SAW]()), providing in-depth insights into each aspect of our platform's functionality. Whether it's understanding specific features or grasping the nuances of various modules, our documentation aims to help you further.

### Testing & Containerization

Recognizing the importance of robust testing framework, we dedicate a section to [testing framework](/testing/2.4/getting_started), ensuring reliability and stability in every release. Additionally, we shed light on containerization techniques, showcasing how our platform can be efficiently deployed and managed in diverse environments.

## Licenses

Prior open-sourcing DCP and its modules, we investigated under which licenses the packages used to develop the applications were published. A summary of packages and their corresponding licenses is provided as summaray in the following [document](https://docs.google.com/spreadsheets/u/2/d/e/2PACX-1vSzlr6X2m7xQUYgqEArivK5khTSxecWM4rrYVE6ZNX41tCLyWaS4p9JHkU87-rrCT-AMAn0LwbkY-aK/pubhtml?widget=true&headers=false). Luckily, the majority of packages and libraries used are published utitlizing standard open-source licenses and can be used and modified. However, there is one important exception, the Kendo UI Framework of [Telerik](https://www.telerik.com/) used to create the Frontend application. This is a paid library and requires each developer who is compiling the frontend application to own a corresponding license.

> {info} Even though a paid license is required to compile the frontend components, using DCP & modules as well as customization is free of charge.

## Next Steps

Depending on your needs you can follow different routes. If you just want to customize the visualization of DCP, continue with [Customization](/{{section}}/{{version}}/customization). In case you want to contribute to existing features or you want to create a new module from scratch continuing reading the [Installation](/{{section}}/{{version}}/installation) might be helpful.

> {success} With DCP, we are pioneering open-source GMP analytics for a more accessible, enjoyable, and productive data-driven future. If you are interested in contributing to DCP, please refer also to our [Contribution Guide](/contribution_guide/1.0/code_of_conduct).