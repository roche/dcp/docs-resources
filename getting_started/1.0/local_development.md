# Local development

- [Introduction](#introduction)
- [Backend](#backend)
- [Frontend](#frontend)
    - [Setup](#setup)
    - [Usage](#usage)

## Introduction

Explore our local development options, crafted to streamline your development experience with a suite of curated tips and tricks. Dive into a world where convenience meets efficiency, empowering you to harness the full potential of our platform while honing your coding skills in a familiar environment.

## Backend

Usually, the backend only provides callable endpoints via REST API. Sometimes it can be more convienent to have the UI pointing to the locally hosted backend. Therefore, we started exploring the option to route the API calls from the Frontend to a different url in the SAW module. To activate this feature, just add the javascript variabele

## Frontend
For local development we can use 2 approaches.

1. First is to load any application with `npm start` which for Angular application will run `ng serve`. The problem with this approach is that you need to have API option for loading different from SSO that will be used only for localhost. This option is not possible with ~~VueJS applications~~.
2. Second option is setup the server, build and upload all applications, and then load needed one from `localhost` 

### Setup

Depending on where we get the project code in **Base App** we can have or not ready to use [**Import map overrides**](https://github.com/single-spa/import-map-overrides#readme) setup.

If this setup is missing you can just add this two lines:

```html
<!--index.html-->
<script src="js/import-map-override.js"></script>
<import-map-overrides-full show-when-local-storage="devtools"></import-map-overrides-full>
```

`import-map-override.js` is already part of the project repository files. 
`import-map-overrides-full` is an HTML Web Component, that will add to your screen button and popup as per documentation of  [**Import map overrides**](https://github.com/single-spa/import-map-overrides#readme).

### Usage

Open the DCP application () in the browser and open the DevTools Console. Inside the console run the following command:

```js
localStorage.setItem('devtools', true)`
```
After page reload, there is one additional button in bottom right corner available, by clicking on the button the **Import Map Overrides** can be opened.

To use the overwrite you can run the application locally, using the following command:
```js
npm run start:spa
```

This will run the application with `extra-webpack` `single-spa` configuration ready. to be used inside the import map overwrites. 

In every application repository you can find correct port that the application will be started.

To import **Angular** application you need to enter `http://localhost:4201/main.js` (e.g. Administration application) or `https://localhost:8081/js/app.js` (e.g. SAW VueJS application) depending on after build naming and folder structure.

**Currently used ports for local development**

| Application            | port   | Full override path              |
|------------------------|--------|---------------------------------|
| Login                  | `4200` | `http://localhost:4200/main.js` |
| Administration         | `4201` | `http://localhost:4201/main.js` |
| Framework              | `4202` | `http://localhost:4202/main.js` |
| ChromTA                | `4203` | `http://localhost:4203/main.js` |
| MVDA                   | `4204` | `http://localhost:4204/main.js` |
| Compliance check       | `4205` | `http://localhost:4205/main.js` |
| DReAM / Reporting      | `4208` | `http://localhost:4208/main.js` |
| Top Navigation         | `4221` | `http://localhost:4221/main.js` |
| SAW                    | `8081` | `https://localhost:8081/js/app.js` |
| Side Menu              | `4220` | `http://localhost:4220/main.js` |
| API Service            | `4207` | `http://localhost:4207/main.js` |
