# Customization

- [Introduction](#introduction)
- [Concept](#concept)
- [Directory Structure](#directory-structure)
    - [Gulp Files](#gulp-files)
    - [Replacer Assets](#replacer-assets)
    - [Customizer](#customizer)
- [Modification Options](#modification-options)
    - [Colors](#colors)
    - [Logo & Favicon](#logo-amp-favicon)
    - [Login Image](#login-image)
    - [Font](#font)
- [Run Customizer](#run-customizer)

## Introduction

We understand that each company has its unique culture and branding. To catalyze the adoption of DCP, an easy to use customization script was developed. This scripts supports you in adjusting the look and feel to meet your and your companies requirements.

## Concept

In order to make the customization as easy as possible, the customization operation is performed as post-processing of the compiled frontend components. Luckily, adjustments in the backend are typically not required to modify the theme and presentation of DCP. To perform the modifications, multiple [Gulp](https://gulpjs.com/) tasks are defined and get executed that allow us to override some of the values that we are using in the DCP frontend.

## Directory Structure

The project is setup as follows

```
customizer
└── customizer.js
└── replacer-assets
└── gulpfile.js
    └── index.js
    └── replacers.js
    └── config.js
    └── helper.js
```

### Customizer

The `customizer.js` is the entry file that is typically executed from the CI/CID. There is one **optional** parameter: [`PROJECT_TYPE`](#run-customizer). If is not presented it will call the default replacement function.

### Replacer Assets

The folder `replacer-assets` contains all files that we want to replace in the final build. Files can be changed as you want, but they **must be** with same name and extensions as they are currently.

### Gulp Files

The following four files are present:

- The `index.js` file contains all gulp tasks
- The `replacers.js` file contains all functions that the gulp tasks needed
- The `config.js` file contains all the configurations that we are using in the gulp tasks like colors, paths etc.
- The `helper.js` file contains some helper functions

## Modification Options

All modification options can be adjusted in the `replacers.js` file.

### Colors

The entire color theme is defined by the use of seven color code in the `config.js` file.

```js
    // other content of config.js file    
    COLORS: {
        PRIMARY_COLOR: '#17c5a8',
        PRIMARY_COLOR_ENCODED: '%2317c5a8',
        SECONDARY_COLOR: '#19d1b2',
        PRIMARY_SHADOW1: '#c3fdd8', 
        PRIMARY_SHADOW2: '#f7fef7',
        PRIMARY_SHADOW3: '#d0f5ec',
        PRIMARY_SHADOW4: '#fafafa' // page background color
    },
```

### Logo & Favicon

The entire color theme of the DCP logo and the DCP loading animation is defined by three colors in the `config.js` file.

```js
    // other content of config.js file    
    LOGO: {
        PRIMARY_COLOR: '#19d1b2', //
        SECONDARY_COLOR: '#17c5a8', // e.g. highlighted font color
        TERTIARY_COLOR: '#15ab92', // e.g . background color of buttons
    },
```

The company logo and the favicon need to be replaced in the `replacer-assets` folder.

> {info} The defined colors are also used to adjust the color gradient of the `.svg` DCP logo presented in the upper left corner and on the login screen.

### Login Image

The image (`logo_login.png`) presented next to the login needs to be adjusted in `replacer-assets` folder.

### Font

The entire application appears with one standard font, which can be adjusted in the `config.js` file.

```js
    // other content of config.js file    
    FONT: {
        URL: 'https://fonts.googleapis.com/css?family=Mulish',
        NAME: 'Mulish'
    }
```

## Run Customizer

All tasks are accessible via calling the gulp, for example `gulp colorReplacer` or using the described in the `package.json` npm commands `npm run replace:colors` which is the recommended way to execute tasks.

Run node customizer.js `PROJECT_TYPE` where the `PROJECT_TYPE` can be one of the following:

- `base` (customizing components of the framework, such as the top navigation)
- `angular` (customizing all Angular based frontend components)
- `vue` (customizing all Vue based frontend components)
- `web-component`  (customizing all components which are provided as web compontents)