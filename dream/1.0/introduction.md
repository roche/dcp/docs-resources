# Introduction
- [Introduction](#introduction)
- [More Information](#more-information)
- [Basic structure](#basic-structure)


## Introduction
DReAM, or Dynamic Reporting for Advanced Manufacturing, revolutionizes manufacturing data management. The module streamlines data compilation, eliminating inconsistencies and manual processes. It effortlessly generates summaries such as the Normal Operating Ranges, providing standardized insights across diverse manufacturing environments. 

From a data engineering perspective DReAM can help to address the following problems:

* **Managing parameter sets**: Embedding specific report configurations (e.g. which parameters to be reported) into source datasets is problematic for multiple reasons:
    * Some report requirements may be specific, encoding these requirements into the data model increases the complexity and hinders re-usability. With the increase of complexity multiple views to the source data needs to be maintained, this increases maintenance efforts required by the technical teams.
    * Report requirements may change more frequently than the data model used to store these values. By coupling the report to the data source configuration a change in the report always requires a change on the data source level. Which involves different teams, requires coordination with other consumers of the data model and therefore generally takes longer time.

* **Manage mappings**: DReAM enables process SMEs to perform mappings in 2 dimensions:

    * Report to data source: Quite often reports have special reporting requirements in terms of names/units of measure etc. By setting up a template process SMEs define in a first step how the reporting parameter should be displayed e.g. Backpressure (mbar) and then links the unique identifier on the data source e.g. V1000-PSIA1001 (psi). Different units are converted.
    * Across different data sources: If specifications and actual values are stored in different systems, it's not a rare thing that they are using different naming conventions. In DReAM both sources are mapped against the report template integration the different sources.

* **Manual interventions**: In real world manufacturing processes manual interventions are unwanted but still happening on a day-to-day basis. Especially in the early phase of the process development and scale out, manual interventions are expected, these need to be reflected in the reporting as well. DReAM offers the users to do these interventions on the reporting layer, so that data exclusions and correction can be made in a traceable, documented fashion complying with the health authority requirements.

For a comprehensive overview of the capabilities offered by the DReAM module, please visit [open-dcp.ai](https://open-dcp.ai/module/dream).

## Basic structure

The DReAM module uses four different logical elements to structure the application and the elements. They are:

* **Template Types** template types are predefined, configurable RMarkdown documents which define the building block of each report.
* **Templates** are based on a template type, data source agnostic and define the structure/sections elements configured by the user.
* **Reports** link the user definitions within the template to external data sources, enabling the data retrieval.

* **Report Execution**: this is the actual rendered report document containing the values as defined by the template, gathered from the data source as linked in the report for the user selected set of batches/event frames.