# Installation and setup
- [Introduction](#introduction)
- [R packages](#r-packages)
- [Backend](#backend)
    - [Windows Services](#windows-services)
    - [API](#api)
    - [Virtual Directories](#virtual-directories)
- [Frontend](#frontend)
    - [Micro Frontend](#micro-frontends)
- [Module specific steps](#module-specifics-steps)

## Introduction
DReAM uses the [standard installation path](/framework/1.0/installation_and_setup), described in the DCP Framework. For the variables use the sections below.

## R packages

DCP DReAM functionality is implemented in a single package, which depends on other acdc packages which are installed via the package dependencies.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| packageName                   | acdcDReAM                                |


## Backend

### Windows Services

**Event Bus Service**
This service is used to wire against the events emitted from dcp framework and have an implementation in the DReAM module.

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| ServiceName                   | DReAMEventBusDCPService                  |
| ModuleName                    | DReAM                                    |
| FolderName                    | EventBusService                          |
| DisplayName                   | DCP DReAMEvent Bus Service               |
| Startup Type                  | Automatic                                |
| ExecutableName                | DCP.DReAM.WorkerService.EventBus.exe     |

### API
The DReAM uses the .NET Core API path, all module specific functionality is provided by a single IIS site:

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| IIS Site Name                 | DReAM                                    |
| ModuleName                    | DReAM                                    |
| HTTP-Port                     | 8092                                     |
| HTTPS-Port                    | 8192                                     |
| NET CLR Version               | No managed code                          |
| Subfolder                     | N/A                                      |
| Hostname                      | dc-{env}-gxp.domain.com, for prod dcp-gxp.domain.com   |


### Virtual Directories

|    Alias                      | Physical Path                            |
|-------------------------------|------------------------------------------|
| DCPData                       | E:/DCPData/                              |


### PHP extensions
N/A - using .NET Core route therefore this section does not apply.

### Scheduled Tasks
N/A - no scheduled tasks required

## Frontend

### Micro frontends
All DReAM functionality is bundled in a single microfrontend, which uses the following configuration:

|    Variable                   |                 Value                    |
|-------------------------------|------------------------------------------|
| Appname                       | DReAM                                    |
| Hostname                      | dc-{env}-gxp.domain.com, for prod dcp-gxp.domain.com   |

## Module specific steps
DReAM does not require any module specific installation steps.