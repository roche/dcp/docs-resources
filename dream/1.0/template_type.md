# Template Type 

- [Introduction](#introduction)


## Introduction
Technically speaking a template type is a Rmarkdown document. The set of template types is defined by the code basis. The template types are managed within the *acdcDReAM R package*. Following the R package development practice the Rmarkdown documents are stored in /inst/rmd/ folder of the package. Every template type uses a dedicated Rmarkdown document named ${template_type}-report.Rmd.

All template types have access to the same arguments, how these arguments are used (or if they are even ignored) is implemented in the specific template type. The following main arguments are passed:

* **metaData** this argument contains the report execution meta data, the author, execution time, etc.
* **processes** contains a list of eventframes/batches for which the report is 
* **template** contains the user configured sections, including with the already resolved mappings to the external data sources.

Template types are rendered using the reporting functionality provided by [AC-DC](calculation_engine/1.0/reports.md). The resolution of the pointers to the external data source happens on the backend level. The R package itself implements only layout/formatting/plotting and rendering responsibility. Within DReAM all integration of external data sources are happening within the backend, therefore the R package does not interface with the other datasources.