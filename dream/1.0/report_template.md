# Report template

- [Introduction](#introduction)
- [Report Template Records](#report-template-records)

## Introduction
Report templates are designed to be data source agnostic, focusing exclusively on defining the skeleton (layout and structure) of the report. The process of using a template begins with selecting the appropriate template type. Once the template type is chosen, users can configure various sections provided by that template. Typically, this configuration process involves:

* **Defining Process Steps**: Users specify which process steps should be included as distinct sections in the report. Each step represents a key component or milestone within the overall process, ensuring that all relevant aspects are covered in the report.

* **Configuring Process Parameters**: For each process step, users configure the parameters that need to be reported. This includes setting up:

    * **Limit Definitions**: Establishing upper and lower bounds for each parameter to highlight any deviations or issues.
    * **Summary Statistics**: Determining which statistical measures (such as averages, medians, or standard deviations) should be included to provide a concise overview of the data.

* **Customizing Section Layouts**: Within the constraints of the chosen template type, users can adjust the arrangement and organization of sections to suit their reporting needs. This flexibility allows users to tailor the report layout to better align with their specific requirements while maintaining a consistent structure.

The configuration process is designed to be independent of the data source, meaning that the same template can be used regardless of the underlying data. The primary role of the template is to define, how information is structured and presented in the report. The actual formatting of the layout, such as font styles, colors, and visual elements, is predetermined by the template type and cannot be customized by the user. This ensures that reports maintain a consistent appearance and structure across different uses.

Additionally, report templates may include predefined sections for common elements, such as title pages, table of contents, and executive summaries, which can be included or excluded based on user preferences. The templates are designed to streamline the reporting process, reduce errors, and enhance the clarity and professionalism of the final report.


## Report Template Records

```mermaid{.mermaid width=1000}
erDiagram

ReportTemplate  {
 	uniqueidentifier(16) ID 
	nvarchar Name 
	bit(1) IsPublic 
	tinyint(1) Type 
	int(4) SiteId 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who is owning the record - may have special privileges"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportTemplateProcessStep  {
 	uniqueidentifier(16) ID 
	nvarchar Name 
	tinyint(1) StepNumber 
	uniqueidentifier(16) ReportTemplateId 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who is owning the record - may have special privileges"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportTemplateSharedUsers  {
 	int(4) ID 
	uniqueidentifier(16) ReportTemplateId "The shared model template"
	int(4) UserId "The contributor userId - allowed to perform edits"
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who is owning the record - may have special privileges"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportTemplateLock  {
 	uniqueidentifier(16) ID 
	uniqueidentifier(16) ReportTemplateId 
	bit(1) IsLocked 
	int(4) LockedBy 
	datetime2(8) LockedOn 
}

ReportTemplate ||--o{ ReportTemplateProcessStep: "has"
ReportTemplateProcessStep ||--o{ ReportPhaseExecution: "has"
ReportTemplateProcessStep ||--o{ ReportTemplateProcessParameter : "described"
ReportTemplate ||--o{ ReportTemplateSharedUsers: "shared with"
ReportTemplate ||--|| ReportTemplateLock: "locked by"
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  Report template definition (data source agnostic)                                                       |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |
