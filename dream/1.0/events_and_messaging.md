# Event Lifecycle

- [Introduction](#introduction)
- [Event Bus](#event-bus)
- [Internal Events](#internal-events)

## Introduction
Following the general pattern of events in DCP, the DReAM module listens to the following types of asynchronous events in DCP:

* User interactions in framework which needs/can be implemented in the DReAM module/database as well (e.g. site deletion)

The section gives an overview of the message subscriptions and internal messages omitted by the DCP DReAM module. It does not implement any additional queue or exchangers which should be consumed by other modules.

## Event Bus

The DReAM module subscribed to the following events - distributed by the DCP Framework.

* messages for a [deleted site](/framework/1.0/administration#site-management)

## Report Generator

The DReAM module uses an internal Event to decouple the user interaction from the potential long running task for the report rendering. 

## Internal Events

> {danger} The messages in this section should not be used for any subscription in other modules, there is no guarantee for format or other changes during the deployment lifecycle.

The DReAM module has a single internal event, which is emitted when the user is triggering the rendition of a report. The message is sent via a direct publish (no routing, etc.) into the *ReportGeneratorQueue*. Messaging is used at this place to decouple the user interaction from the potential long running task for the report rendering. The message has the following format:

```json
{
    "ReportExecutionId": "781e55a3-833f-11ec-90fd-005056a7fed6",
    "Processes":  [
        {
            "Id": "5a6bef25-408f-4574-a8d9-e08d07d2130b",
            "StartTime": "2024-08-23T00:00:00Z",
            "EndTime": "2024-08-23T02:00:00Z",
            "BatchId": "BatchID 371",
            "DeviceWebId": "I1EmMr97kyj9o0-TFm03LBSKaAJO9rWjM8OE6nh0ERPuoDIw",
            "DevicePath": "Area - 42 |> Area - 95 |> Device - 56",
            "StepId": "12e67061-e786-42e9-872c-894239b0cd84",
            "ReportTimeFrameDisplayIdentifier": "BatchID",
            "Rationale": null
        }
    ],
    "Periods": [
        {
            "StepId": "12e67061-e786-42e9-872c-894239b0cd84",
            "StartTime": "2024-08-23T01:44:20Z",
            "EndTime": "2024-08-23T05:44:20Z",
            "ExcludedTimeFrames": [
                {
                "Id": "5a6bef25-408c-4ba7-a0c8-39fb0873cc84",
                "StartTime": "2024-08-23T02:00:00Z",
                "EndTime": "2024-08-23T04:00:00Z",
                "BatchId": null,
                "DeviceWebId": "I1EmMr97kyj9o0-TFm03LBSKaAJO9rWjM8OE6nh0ERPuoDIw",
                "StepId": "00000000-0000-0000-0000-000000000000",
                "ReportTimeFrameDisplayIdentifier": null
                },
            ]
        }
    ],
    "SiteId" : 1
}
```

The properties of the message are explained below:

| Field           | Datatype  | Description |
|-----------------| ----------|-------------|
| ReportExecutionId    | Guid    | The unique identifier of the report execution. |
| SiteId          | Integer   | The siteId for which the rendition should be created. |
| Periods         | Object[]  | The reporting periods per process step, which should be shown in the report. |
| Processes       | Object[]    | The periods to be rendered in the report. |