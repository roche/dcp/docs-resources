# Report Execution

- [Introduction](#introduction)
- [Manual intervention](#manual-intervention)
	- [Exclusions](#exclusions)
- [Manual Records](#manual-records)
- [Running a report](#running-a-report)
- [Resolution of Parameters and Specifications](#resolution-of-parameters-and-specifications)
- [Report execution records](#report-execution-records)

## Introduction

## Manual intervention

One of the key features of DReAM is its ability to address contextualization issues within the external data source. By default, an event frame is provided by the external data source. However, DReAM allows users to manually establish context within the system itself. This manual contextualization includes:

* **Defining Context Boundaries**: Users can specify the start and end times for the context they are interested in.
* **Linking to Report Device**: Users can associate the context with a specific report device.
* **Entering Identifiers**: Users input identifiers to accurately reference the context. Those are used for display purposes only, with no guarantee for uniqueness, internal Ids are used in the application code. 

This manual approach is particularly useful for handling irregular cases, such as time-based anomalies, without requiring changes to the external data source.

### Exclusions
In addition to manual contextualization, DReAM incorporates a second layer of intervention through the concept of exclusions. Exclusions act as data masks applied to the time series data. These masks must be justified with user comments and are necessary for addressing specific issues like ramp-down periods or temperature spikes. DReAM distinguishes between two types of exclusions:

* **Parameter-Specific Exclusions**: These exclusions are valid for a specific attribute or parameter. They allow users to apply masks to particular data points relevant to a specific sensor or measurement.

* **Generic Exclusions**: These exclusions are applied across all sensors within the defined time range. They provide a broader approach to masking data for general issues affecting multiple sensors.

Exclusions are organized into groups, each identified by an ExclusionGroupKey. By design, a single time range can be associated with different exclusion sets. Users have the option to select:

* **Original Data**: The unmasked time series data, which reflects the raw measurements.
* **Exclusion Groups**: One of potentially multiple exclusion groups, each containing masked data sets. Users can choose from these groups, which may contain further masked data, to be used as processes or event frames in the final report.

This flexibility allows users to tailor the report data according to specific needs and contextual requirements, ensuring that the final report accurately reflects the desired insights while accommodating necessary data adjustments.

**Exclusion Suggestion**
In this design, exclusions are managed through a rule-based mechanism. The process initiates with the RuleParser class, which converts an expression into an array of rules. Each rule is a subclass of the abstract base class ExclusionRule. To introduce new rules, developers must create additional classes derived from ExclusionRule. Each rule subclass is required to implement the ParseExpression method, which is responsible for parsing a rule-specific string into the necessary arguments, storing them in private fields. Furthermore, each rule must implement the GetExclusion method, which applies the rule to a given set of values, generating and returning the applicable exclusions.

The following diagram gives an overview of currently available rules:

```mermaid
classDiagram

class ExclusionRule{
	+string RuleName
	-Guid _id
	-JRuleSettingParameter[] parameters 
	+string GeneratorName
	+InputParameters()
	+DisplayString()
	+RuleDescription()
	+ConfigurationForm()
	+GetExclusions()
}

class RampUpExclusionRule{
	-double _thresholdValue
	-string _thresholdType
	-Guid _parameterId
}

class TimeDelayExclusionRule{
	-double _timespan
	-string _timespanUnit
	-string _referenceTime
}

ExclusionRule <|-- RampUpExclusionRule
ExclusionRule <|-- TimeDelayExclusionRule
```

The definition of a rule and its corresponding parameters are fully managed in the backend. This is achieved using the `DCP.Framework.FormBuilder` NuGet package on the backend and the `FormBuilder` library on the frontend. The backend provides the complete form definition, including validation rules. The frontend captures user input, applies form validation, and then transmits the data as key-value pairs to the backend for further processing.  

In the case of exclusion rules in DReAM, they are simply persisted in the database using the following schema:  


```mermaid
erDiagram

ExclusionRule  {
 	uniqueidentifier(16) ID 
	nvarchar RuleName "The name of the rule"
	nvarchar RuleSettings "A json object storing the rule configuration parameters as key value pairs"
	uniqueidentifier(16) ReportProcessStepId "The FK to the template process step for which the rule is valid"
	uniqueidentifier(16) ReportId "The FK to the report for which the rule is valid"
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportTemplateProcessStep ||--o{ ExclusionRule: "has"
Report  ||--o{ ExclusionRule : "has"

```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  User configured exclusion rules to generate exclusions |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |


## Manual Timerange Records

```mermaid
erDiagram

ReportTimeRangeExclusion  {
 	uniqueidentifier(16) ID 
	datetime2(8) StartTime 
	datetime2(8) EndTime 
	nvarchar EventFrameId 
	nvarchar ExclusionGroupKey 
	nvarchar GeneratorRuleName "The rule name from which the exclusion was derived in case of manual settings - set to manual"
	nvarchar Comment "The user added comment for adding the manual time range" 
	uniqueidentifier(16) ReportDeviceId "The report device - used to select the set of sensors"
	uniqueidentifier(16) ReportTemplateProcessParameterId 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who is owning the record - may have special privileges"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportManualTimeRange  {
 	uniqueidentifier(16) ID 
	datetime2(8) StartTime "The start time of the user defined timerange in UTC "
	datetime2(8) EndTime "The end time of the user defined timerange in UTC "
	nvarchar BatchId "The human readable identifier of the user defined timerange in UTC "
	nvarchar Exclusions "The exclusions added to the timerange"
	uniqueidentifier(16) ReportDeviceId 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportDevice ||--o{ ReportManualTimeRange: "has"
ReportDevice  ||--o{ ReportTimeRangeExclusion : "has"

```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  User entered manual exclusions and time ranges|
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |


## Running a report
Running a report involves applying the report's specifications to a selected set of processes. This process can be broken down into the following steps:

```mermaid
sequenceDiagram
    participant User
    participant Frontend
    participant Backend
    participant Worker
    participant ExternalDataSource
    participant ReportingEngine

    User->>Frontend: Selects Event Frames
    Frontend->>Backend: Sends RunReport request
    Backend->>Worker: Sends message to worker
    Worker->>Backend: Performs request against renderReport endpoint
    Backend->>ExternalDataSource: Resolves data required for rendering
    Backend->>ReportingEngine: Triggers report rendering
```


* **Selection of Event Frames**: The user selects event frames manually or allows the system to semi-automate the selection based on predefined conditions configured in the report template. This step determines which data sets will be included in the report.

* **Resolution of Parameters and Specifications**: The backend layer processes the report configuration to resolve parameters and apply specifications. This involves translating the report's requirements into actionable queries and data manipulations to extract and format the necessary information.

* **Rendering the Report**: The resolved data is then rendered into a report. Depending on the configuration, a single report can be produced in multiple formats. Each format serves different purposes and user needs.

The report execution is initiated as soon as the user activates the command or clicks button to generate the report. If the report generation process encounters any issues and fails, the report execution is logged with a status indicating failure, and no report file is produced.

**Report Storage and Output Formats**
Report executions are saved as files on the hard disk, with each file using an extension corresponding to its output format. Supported output formats include:

* **PDF**: A static, non-editable format ideal for compliance and formal documentation. It preserves the report's layout and content integrity, making it suitable for official use and sharing.

* **Word**: An editable format that allows users to further extend or comment on the report. This format is useful for collaborative work, editing, or adding additional notes and annotations.

* **JSON**: A machine-readable format that includes the formatted dataset. This format is designed for integration with data pipelines and further processing, making it suitable for automated data handling and analysis.

## Resolution of Parameters and Specifications
Data resolution is implemented through the ReportDataResolver class, which serves as the base class for all report types. Each specific report type implements a derived class, such as SummaryTableReportDataResolver. These derived classes are required to implement the GetRequestParameters method, which returns the report-type-specific arguments necessary for the rendering process. Shared functionalities, such as resolving exclusions, are provided in the parent DataResolver class. The structure is visualized in the diagram below:

```mermaid
classDiagram
    ReportDataResolver <|-- BatchReviewReportDataResolver
    ReportDataResolver <|-- NormalOperatingRangesReportDataResolver
    ReportDataResolver <|-- SummaryTableReportDataResolver

    class ReportDataResolver{
      -GetExclusions()
      -GetOutline()
      +ResolveLimits()
    }
    class BatchReviewReportDataResolver{
      +GetRequestParameters()
    }
    class NormalOperatingRangesReportDataResolver {
      +GetRequestParameters()
    }
    class SummaryTableReportDataResolver{
      +GetRequestParameters()
    }
```

## Report Execution Records

A report execution involves fetching data from the configured references and generating various report formats. For each format, a dedicated file is saved to disk, and an entry in the database links the UNC path on the hard disk to the report execution. Additionally, the report execution includes `ReportExecutionPeriod` and `ReportExecutionSelectedTimeRange` entries, which capture the user's selection. This information is stored to allow for re-running the report with the same settings, enabling corrections (such as additional exclusions). Report executions can be tagged, and these tags can be used by third-party systems to query report executions for further integration (cherry-picking).

```mermaid
erDiagram

ReportExecution  {
 	uniqueidentifier(16) ID 
	tinyint(1) Status "The status of the report execution, some status are: processing, completed, failed, ..."
	int(4) ReportVersion "The report version used to render the format"
	uniqueidentifier(16) ReportId "The report definition used to render the format"
	tinyint(1) IsValidated "Indicator if all components in use were validated - leading to a validated report execution"
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportExecutionFiles  {
 	uniqueidentifier(16) ID 
	nvarchar ReportUrl "The full URL path on the disk, used to access the report"
	nvarchar FileFormat "The report execution format of the present data"
	uniqueidentifier(16) ReportExecutionId 
}

ReportExecutionTag  {
 	int Id 
	nvarchar TagValue "The name of the tag added to the report"
	uniqueidentifier(16) ReportExecutionId
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportExecutionPeriod {
 	uniqueidentifier(16) ID 
	uniqueidentifier(16) StepId 
	datetime2(8) StartTime "The start time of the user defined timerange in UTC "
	datetime2(8) EndTime "The end time of the user defined timerange in UTC "
	uniqueidentifier(16) ReportExecutionId 
}

ReportExecutionSelectedTimeRange {
 	uniqueidentifier(16) ID 
	uniqueidentifier(16) TimeRangeId "The uniqueId of the timeframe on the (external) datasource"
	nvarchar DeviceWebId "The uniqueId of the timeframe on the (external) datasource"
	uniqueidentifier(16) ExclusionGroupKey "The set of exclusions used for this time range"
	uniqueidentifier(16) ReportExecutionPeriodId 
}

Report ||--o{ ReportExecution: "has"
ReportExecution ||--o{ ReportExecutionTag: "has"
ReportExecution ||--o{ ReportExecutionFiles : "provides"
ReportExecution ||--|{ ReportExecutionPeriod: "has"
ReportExecutionPeriod ||--|{ ReportExecutionSelectedTimeRange: "has"
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  Report Execution log                         |
|Data classification    |  Convenience records                          |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |
