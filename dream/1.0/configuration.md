# Configuration
- [Introduction](#introduction)
- [Frontend](#frontend)
- [Backend](#backend)
    - [API](#api)
    - [EventBus Worker](#eventbus-worker)
    - [ReportGenerator Worker](#reportgenerator-worker)
    - [Configuration Sources](#configuration-sources)
    - [WebConfig Transformers](#configuration-sources)

## Introduction

## Frontend

Frontend configuration is using the angular default configuration set-up, which is outlined in the [getting started](/framework/1.0/configuration.md) section.

## Backend

### DReAM - API

The API is configured using `appsettings.json`. The configuration keys and their meanings are shown below:

| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
|General|
| CleanHistoryTables | Boolean | Flag if SQL temporal tables`data needs to be deleted | false
| MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| DevelopmentMode | Boolean | Should the app, been started on localhost | false |
| ActiveLogDebug | Boolean | | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| EnableSwagger | Boolean | Should a swagger with all endpoints should be supported by the system | false |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| FileUploadPath | String | UNC on the disk where report execution should be stored | E:\\DCPData\\DReAM\\Executions\\ |
| ForbiddenDatabaseKeyWords| String | forbidden commands when trying to enter used defined queries| ALTER; CREATE; DROP; GRANT; RENAME; REVOKE; TRUNCATE; DELETE;INSERT; UPDATE; COMMIT; ROLLBACK; SAVEPOINT; SET TRANSACTION |
| TimeoutReport TemplateLockMinutes | Integer | Timeout for locking the template | 10|
| SendFeedbackTo | String | The sender e-mail address used when sending messages to users | E:\\DCPData\\DReAM\\Executions\\ |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGateway SwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGateway HeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultServerUri | String | The address for the vault instance | |
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultDataSourceMountPoint| String | The mount point of the vault kv2 secret engine storing the external secrets | ext-datasource/ |
| VaultDataSourcePath | String | Vault path containing the external data source secrets | prod |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultClientPath | String | The path to the client ID/client secret pairs| prod/authorizationServer/dream |
| VaultDatabasePath | String | Vault path containing the database secrets | prod/db/dream |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | prod/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | prod/headers |
| VaultTokenPath | String | Vault path containing the token secrets | prod/token |
| VaultAuthMethod | String | Used authentication method against vault | Certificate |
| Memory cache setup |
| AudienceDetailsExpireDays | Integer | Number of days until the memory cache for the audience is invalided | 1 |
| ConnectionString DetailsExpireDays | Integer | Number of days until the memory cache for the database configuration string is invalided | 1 |
| HeadersDetails ExpireDays | Integer | Number of days until the memory cache for the header is invalided | 1 |
| AuthorizationServer DetailsExpireDays | Integer | Number of days until the memory cache for the authorization is invalided | 1 |
| RabbitMQMessageBus DetailsExpireDays | Integer | Number of days until the memory cache for the message bus config is invalided | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debug level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\DReAM\\Messages.log |
| Serilog.WriteTo .Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |



### EventBus Worker
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
 MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| DevelopmentMode | Boolean | Should the app, been started on localhost | false |
| ActiveLogDebug | Boolean | | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGateway SwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGateway HeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPDReAMBaseUrl | String | Host of the DReAM service | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultServerUri | String | The address for the vault instance | |
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultDataSourceMountPoint| String | The mount point of the vault kv2 secret engine storing the external secrets | ext-datasource/ |
| VaultDataSourcePath | String | Vault path containing the external data source secrets | prod |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultClientPath | String | The path to the client ID/client secret pairs| prod/authorizationServer/dream |
| VaultDatabasePath | String | Vault path containing the database secrets | prod/db/dream |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | prod/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | prod/headers |
| VaultTokenPath | String | Vault path containing the token secrets | prod/token |
| VaultAuthMethod | String | Used authentication method against vault | Certificate |
| Memory cache setup |
| Audience DetailsExpireDays | Integer | Number of days until the memory cache for the audience is invalided | 1 |
| ConnectionString DetailsExpireDays | Integer | Number of days until the memory cache for the database configuration string is invalided | 1 |
| Headers DetailsExpireDays | Integer | Number of days until the memory cache for the header is invalided | 1 |
| AuthorizationServer DetailsExpireDays | Integer | Number of days until the memory cache for the authorization is invalided | 1 |
| RabbitMQMessageBus DetailsExpireDays | Integer | Number of days until the memory cache for the message bus config is invalided | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debug level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\DReAM\\Messages.log |
| Serilog.WriteTo. Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |

### ReportGenerator Worker
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
 MachineName | String | DNS for the validated server node | |
| IsModuleValidated | Boolean  | Whether the module is validated or not | true |
| DevelopmentMode | Boolean | Should the app be started on localhost | false |
| ActiveLogDebug | Boolean | | false |
| DetailErrorReturn | Boolean | Should error details be returned to end user | false |
| DCPLogoUrl | String | URL where the DCP logo can be read | ./app-framework/assets/images/dcp-logo.png |
| Headers |
| DCPMicroserviceHeaderName | String | The header used for microservice internal authentication | X-DCPMicroservice-Key |
| CSRFHeaderName | String | The name of the Header to be added to each HTTP request in order to configure CSRF | X-Requested-With |
| DCPAPIGateway SwaggerHeaderName | String | The name of the Header used for read Swagger config | X-DCPGateway-SwaggerKey |
| DCPAPIGateway HeaderName | String | The name of the Header used for check if HTTP request is received from DCP API Gateway service | X-DCPGateway-Key |
| Service setup |
| DCPCoreBaseUrl | String | Host of the core service | |
| DCPDReAMBaseUrl | String | Host of the DReAM service | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultServerUri | String | The address for the vault instance | |
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultDataSourceMountPoint| String | The mount point of the vault kv2 secret engine storing the external secrets | ext-datasource/ |
| VaultDataSourcePath | String | Vault path containing the external datasource secrets | prod |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultClientPath | String | The path to the client ID/client secret pairs| prod/authorizationServer/dream |
| VaultDatabasePath | String | Vault path containing the database secrets | prod/db/dream |
| VaultRabbitMQPath | String | Vault path containing the RabbitMQ secrets | prod/rabbitmq |
| VaultHeadersPath | String | Vault path containing the headers | prod/headers |
| VaultTokenPath | String | Vault path containing the token secrets | prod/token |
| VaultAuthMethod | String | Used authentication method against vault | Certificate |
| Memory cache setup |
| AudienceDetailsExpireDays | Integer | Number of days until the memory cache for the audience is invalided | 1 |
| ConnectionString DetailsExpireDays | Integer | Number of days until the memory cache for the database configuration string is invalided | 1 |
| Headers DetailsExpireDays | Integer | Number of days until the memory cache for the header is invalided | 1 |
| AuthorizationServer DetailsExpireDays | Integer | Number of days until the memory cache for the authorization is invalided | 1 |
| RabbitMQMessageBus DetailsExpireDays | Integer | Number of days until the memory cache for the message bus config is invalided | 1 |
| DCP Core Authentication |
| DCPCoreTokenEndPoint | String | Route to the token endpoint inside core service | /Token |
| AccessTokenExpireMinutes | Integer | Time (in minutes) how long the token is valid | 14 |
| AuthenticationScheme| String | The authentication schema to be used | bearer |
| Audience.Issuer | String | The name of the token issuer - from which tokens are accepted | |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debug level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\DReAM\\Messages.log |
| Serilog.WriteTo.Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |


### CleanUp Worker
| Variable       |   Data Type   | Description        | Default Value  |
|----------------|---------------|--------------------|----------------|
 MachineName | String | DNS for the validated server node | |
| ISecretConfigurationProvider using Hashicorp Vault|
| VaultServerUri | String | The address for the vault instance | |
| VaultNamespace | String | The namespace in vault where the secrets are stored | dcp |
| VaultAppConfigMountPoint | String | The mount point of the vault kv2 secret engine storing the app configuration secrets | app-config/ |
| VaultDatabasePath | String | Vault path containing the database secrets | prod/db/dream |
| VaultAuthMethod | String | Used authentication method against vault | Certificate |
| Service setup |
| LoopTimerInDays | Integer | Number of days used for scheduling/timing the cleaning service | 7 |
| Serilog|
| Serilog.Using | String[] | The  Serilog sinks to be used to log to | Serilog.Sinks.File |
| Serilog.MinimumLevel.Default | String | the debug level to which log files should be generated | Warning |
| Serilog.WriteTo.Name | String | The authentication schema to be used | bearer |
| Serilog.WriteTo.Args.path | String | The filepath of the logfile to be used | E:\\Logs\\DReAM\\Messages.log |
| Serilog.WriteTo. Args.outputTemplate | String | The log template to be used for the log file creation | "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3} {Username} {Message:lj}{Exception}{NewLine}" |

### Configuration Sources
In addition to the `appsettings.json` the DReAM module uses the following configuration objects:

The DReAM section inside the `ConfigurationItems.json` file inside the core project, has the following values. With the configuration, DReAM requires the following mandatory datatypes:

* Equipment Data (DataTypes.Type: 1)

And supports resources, for:

* Audit trail implementation (Resources.Type: 2)
* Admin actions (Resources.Type: 3)

```json
{
        "Name": "DReAM",
        "Host": "https://#Env#-#IsValidated#.domain.com",
        "IsValidated": false,
        "DataTypes": [
            {
                "Type": 1,
                "Mandatory": true,
                "AllowMultiselection": false
            }
        ],
        "Port": "8192",
        "Resources": [
            {
                "ResourceURL": "api/AuditTrail/GetModuleAuditTrail",
                "Type": 2
            },
            {
                "ResourceURL": "/mvda/administration/actions/create",
                "Type": 3
            }
        ],
        "RPackages": [
            "acdcDReAM"
        ]
    }
```

The following admin actions, are available in the DReAM module, configured by `AdminActions.json` at the root level in the project. All admin actions are implemented in the `SiteController`, the intended business implementation can be taken from the requirement specification.

```json
[
  {
    "Path": "get-all-pi-web-ids-per-site",
    "DisplayText": "Get All PI Web Ids Per Site",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-all-global-filters",
    "DisplayText": "Replace All Global Filters",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-all-web-ids",
    "DisplayText": "Replace All Web Ids",
    "Permissions": [ "GLOBAL_ADMIN" ]
  },
  {
    "Path": "replace-sensor-names",
    "DisplayText": "Replace All Sensor Names",
    "Permissions": [ "GLOBAL_ADMIN" ]
  }
]


```
