# Report

- [Introduction](#introduction)
- [Mapping of Parameters](#,apping-of-parameters)
- [Report Records](#report-records)


## Introduction

A report establishes a connection between the definitions specified in the template and an external data source. In the DReAM framework, the EquipmentData data type is employed to supply data for reports. To manage different connection providers and align them with a unified business data model used within the application, DReAM utilizes the factory pattern. This pattern facilitates the resolution of various storage formats into a common logical data type with a unified format.


## Mapping of Parameters

The connection to an external data source can be realized through three conceptual source types:

* **Direct Sensor/Attribute**: In this approach, the equipment data source provides sensor or attribute values as a time series directly. Aggregation, if feasible, is performed at the source level before the data is fed into the DReAM. This method ensures that the amount of data transferred over the network is minimized.

* **Equation**: This method involves using a formula builder, as defined within the framework. Values are derived by configuring formulas that may incorporate sensor data or hard-coded values. These formulas are evaluated within the DReAM system to compute the required values for the report. This approach allows for flexible data manipulation and the calculation of derived metrics based on predefined formulas, providing customized insights that are not directly available from the data source.

* **Event Frame/Batch Attribute**: In this scenario, data aggregation or integration from other sources is completed within the layer that provides the event frame context, typically through the EquipmentData interface. DReAM utilizes these pre-aggregated values as they are presented by the data source, ensuring that the data reflects any contextual or historical processing that has been applied before it reaches the reporting layer.

Each of these source types caters to different needs and scenarios, allowing for a range of data integration and processing strategies. By leveraging these conceptual source types, DReAM ensures that reports can be generated with accurate, timely, and relevant data, tailored to the specific requirements and constraints of each reporting scenario.

The process of mapping a report template begins with the report device, which serves as the central point of entry. The report device acts as a bridge to the asset hierarchy within the external data source. Each device encompasses:

* **Physical Measurements**: These are various measurements that provide time series data, which are captured as trajectories. This data reflects the real-time or historical performance of the device.

* **Event Frames**: These represent discrete processes or operational states that need to be analyzed. Event frames encapsulate relevant information about specific occurrences or batches of activity.

In addition to mapping parameters, users must configure how event frames are handled for the report. This configuration involves several key steps:

* **Event Frame Relevance**: Users determine which event frames are pertinent to the report by configuring condition values. These condition values create filter expressions based on the event frame's metadata. This filtering process ensures that only the relevant event frames are included in the report, based on predefined criteria.

* **Metadata Attribute Selection**: Users select which metadata attributes will be used as identifiers for reporting purposes. While the internal system relies on a unique identifier from the data source to track and identify processes, the reporting layer translates this identifier into a human-readable format. Note that this translation does not guarantee uniqueness in the human-readable form, but it facilitates easier interpretation and analysis of the report.


## Mapping of Limits

To include limits or specifications in the rendered report, the connection to the data source is configured at the report level. The configuration process varies depending on the type of data source connection used.

**SQL Derived Connections**
For SQL-based connections, users can write direct SQL queries. In these queries, parameters enclosed in {{}} refer to values from the event frame metadata. These parameter placeholders are dynamically replaced with actual values at runtime when the report is executed. The resolution process involves two steps:

* Dynamic Value Replacement: The placeholders in the query are substituted with real-time values from the event frame metadata.
* Query Execution: The modified query is then executed against the external data source. The resulting data is used within the report.

Prior to the storage the query is validated by executing it for the latest event frame matching the condition to ensure that the query specification is valid.

**Time Series Providers**
For time series providers, such as the PI system, the configuration involves linking to the time series identifier and specifying the retrieval mode. The retrieval mode determines how limits are applied. Two sub-types of retrieval modes are supported:

* Dynamic Limits: These limits can change during the execution of the process (e.g., due to different phases or ramp-up periods). In this case, the resolution results in an array of values representing the dynamic changes throughout the process.

* Static Limits: The process step has a constant limit. The retrieval method in this scenario is equivalent to the aggregation method used to map the time series data into a single value. This could be the value at the start, the minimum value, or another aggregate measure that remains constant throughout the process over time.

## Report Records

```mermaid
erDiagram

Report  {
 	uniqueidentifier(16) ID 
	nvarchar Name "The name of the report"
	int(4) Version "The report version - incremented when edits are added"
	bit(1) IsPublic "Flag indicating if the report is publicly shared"
	bit(1) IsArchived "Flag indicating if the report has been deleted"
	uniqueidentifier(16) ReportTemplateId 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

ReportDevice  {
 	uniqueidentifier(16) ID 
	nvarchar DeviceWebId "The unique identifier on the external datasource"
	nvarchar DeviceName "The human readable name of the device on the external datasource"
	nvarchar Filter "The global filter - representing the hirachey to the device"
	uniqueidentifier(16) ReportId "The report where this device is used"
	uniqueidentifier(16) ReportTemplateProcessStepId "The report template process step which is executed on the device"
	nvarchar(500) ReportTimeFrameDisplayIdentifier "The report display Id used to identify an eventframe on the rendered eventframe"
	nvarchar Conditions 
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
}

ReportLock  {
 	uniqueidentifier(16) ID 
	uniqueidentifier(16) ReportId 
	bit(1) IsLocked "Flag indicating whether the report is locked"
	int(4) LockedBy "UserId of the user who locked the report"
	datetime2(8) LockedOn "Timestamp in UTC when the report has been locked"
}

ReportParameterLimitMapping  {
 	uniqueidentifier(16) ID 
	nvarchar Configuration "The mapping configuration - storing the limit defintion on the external datasource"
	nvarchar LimitType "The limit type - of the mapping"
	tinyint(1) Type 
	uniqueidentifier(16) ReportDeviceId "The device for which the mapping is valid"
	uniqueidentifier(16) ReportTemplateProcessParameterId "The template parameter the limit is valid to"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
}

ReportParameterMapping  {
 	uniqueidentifier(16) ID 
	tinyint(1) Type 
	nvarchar Configuration "The mapping configuration - storing the parameter defintion on the external datasource"
	uniqueidentifier(16) ReportTemplateProcessParameterId "The template for which the mapping is valid"
	uniqueidentifier(16) ReportDeviceId "The device for which the mapping is valid"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who performed the last change - used for audit trail"
}


ReportSharedUsers  {
 	int(4) ID 
	uniqueidentifier(16) ReportId "The shared report"
	int(4) UserId "The contributor userId - allowed to perform edits"
	int(4) LastModifiedBy "The userId, who performed the last change - used for audit trail"
	int(4) OwnerUserId "The userId, who is owning the record - may have special privileges"
	datetime2(8) SysEndTime 
	datetime2(8) SysStartTime 
}

Report ||--o{ ReportDevice: "uses"
Report ||--o{ ReportExecution: "has"
ReportExecution ||--o{ ReportExecutionFile : "provides"
Report ||--|| ReportLock : "is"
Report ||--o{ ReportSharedUsers: "shared with"
Report ||--|| ReportLock: "locked by"
ReportDevice ||--o{ ReportParameterLimitMapping: "has"
ReportDevice ||--o{ ReportParameterMapping: "has"
ReportTemplateProcessParameter ||--o{ ReportParameterLimitMapping: "on source"
ReportTemplateProcessParameter ||--o{ ReportParameterMapping: "on source"
```

**Records classification and audit trail**

| Specification         | Value                                         |
|-----------------------|-----------------------------------------------|
|Content/Overview       |  Report definition - with the mappings to the data source                                                      |
|Data classification    |  Official records                             |
|Change Tracking        |  SystemVersioned table features inside SQL    |
|Audit Trail            |  Module specific audit trails                 |
|Retention period       |  10 years                                     |
