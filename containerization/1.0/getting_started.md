# Getting Started

- [Introduction](#introduction)

## Introduction

In this section, we delve into the world of containerization, a game-changer in software deployment. Containerization offers a method of packaging applications, ensuring lightweight, portable, and consistent environments across diverse platforms. Recognizing the importance of simplicity for our users, we are starting to dockerize the DCP Framework and all associated modules. Our goal is to enhance convenience and efficiency in shipping and deploying applications.