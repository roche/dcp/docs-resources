# DCP Documentation

You can find the online version of the DCP documentation at [https://docs.open-dcp.ai](https://docs.open-dcp.ai)

## Contribution Guidelines

We are always welcoming updated documentation. Thanks for contributing.

## Conventions

- Each page needs to have an `Introduction` section.
- Each page needs to have a `Table of Content` at the top, which links to the corresponding headlines.
- Only the page title is presented as h1 (#) and does not translate into an anchor.
- All subsequent titles are not allowed to be h1 and need to be h2 (##) or h3 (###).
- Do not add sub-sub-headlines (only two levels are allowed).
- Anchors are automatically defined for h2 and h3 on the page. To create the anchor id, Str::slug() of Laravel is used, which URL friendly "slug" (e.g. replacing whitespace with "-").

## Shortcodes

Inside markdown files, the following shortcodes are available:

`{{section}}` will return current section name (e.g. `module_slag_1`).

`{{version}}` will return current version name (e.g. `1.0`).

`{{section` and `{{version}}` can be used to create relative links inside same section and version. For example link `[data design]({{section}}/{{version}}/data_storage)` created in `mvda/1.0/directory_structure.md` will be rendered to `[data design](/mvda/1.0/data_storage)`.