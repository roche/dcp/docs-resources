# Bug Reports

- [Reporting Bugs](#reporting-bugs)
- [Reporting Process](#reporting-process)
- [How to Report](#how-to-report)

## Reporting Bugs
If you encounter a bug or issue within the Data Computation Platform (DCP), we encourage you to report it on our GitHub repository. Clear and detailed bug reports help us improve the platform. When reporting a bug, please include the following information:

- **Description:** A concise and clear description of the issue encountered.
- **Steps to Reproduce:** Detailed steps to reproduce the bug. This helps us isolate the problem quickly.
- **Expected Behavior:** Explanation of what you expected to happen.
- **Actual Behavior:** Description of what actually happened.
- **Environment:** Include details such as the operating system, browser version, or any specific configurations relevant to the bug.
- **Screenshots or Code Snippets:** If applicable, provide screenshots, error messages, or code snippets that illustrate the issue.

## Reporting Process
Report bugs promptly and efficiently by following these guidelines for clear and effective communication.

- Check Existing Reports: Before submitting a new bug report, check if the issue has already been reported on our GitHub repository's issue tracker to avoid duplicates.
- Use a Clear Title: Title your bug report descriptively to give a quick overview of the issue.
- Report with Detail: Provide as much information as possible. The more detailed the report, the easier it is to address the problem.
- Be Courteous: Remember to be respectful in your bug report. Clear communication helps everyone involved.

## How to Report
To report a bug, please create a new issue on our GitHub repository's issue tracker, following the guidelines mentioned above.