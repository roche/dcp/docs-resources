# Coding Style

- [Introduction](#introduction)
- [Backend](#backend)
    - [C# (.NET and Core)](#c-net-and-core)
    - [php (Laravel)](#php-laravel)
- [Frontend](#frontend)
    - [Angular](#angular)
    - [Vue](#vue)
- [Calculation Engine](#calculation-engine)

## Introduction
Maintaining a consistent coding style across different technologies within our project fosters readability, collaboration, and maintainability. Below are the recommended coding style guidelines for C# (.NET and CORE), PHP (Laravel), Angular, and Vue, ensuring a cohesive and uniform approach to development practices.

## Backend

### C# (.NET and Core)

**General Principles**
- Follow the Microsoft C# Coding Conventions.
- Use meaningful and descriptive names for variables, functions, and classes.
- Keep methods concise and focused on a single responsibility.
- Encapsulate logic appropriately and favor composition over complex inheritance hierarchies.

**Naming Conventions**
- Use PascalCase for class names and method names.
- Use camelCase for parameter names and local variable names.
- Prefix interfaces with "I" (e.g., `IMyInterface`).

**Formatting**
- Use four spaces for indentation.
- Keep lines reasonably short (around 80-120 characters) for better readability.
- Use braces consistently: place opening braces on the same line as the declaration.

### PHP (Laravel)

**General Principles**
- Follow the PSR-12 Coding Style Guide.
- Utilize Laravel naming conventions for models, controllers, and routes.
- Leverage Laravel's built-in features and best practices for authentication, routing, and middleware.

**Naming Conventions**
- Use camelCase for variable and function names.
- Use PascalCase for class names.
- Follow Laravel's conventions for database table and column naming.

**Formatting**
- Use four spaces for indentation.
- Maintain consistency in file organization and folder structure as per Laravel's recommended layout.
- Document API endpoints, function signatures, and code blocks using clear comments and annotations.

## Frontend

### Angular

**General Principles**

- Follow the Angular Style Guide for Angular.

**Naming Conventions**
- Use kebab-case for component names in templates.
- Use PascalCase for component names in code.

**Formatting**
- Use two spaces for indentation.
- Organize code structure and components following the recommended folder structures for Angular.

### Vue

**General Principles**

- Follow the Angular Style Guide for Angular.

**Naming Conventions**
- Use kebab-case for component names in files.
- Use PascalCase for component names in JavaScript/TypeScript files.

**Formatting**
- Use two spaces for indentation.
- Organize code structure and components following the recommended folder structures for Vue.
- Use single-file components in Vue for better modularity.

## Calculation Engine

**General Principles**
- Follow the Google R style guide.
- Write units tests using the `testthat` library.
- Plotting functions should support multiple output formats

**Naming Conventions**
- Adapt the C# naming rules, variables, functions are named using camelCase, property names in lists and column names are named using  PascalCase.

**Formatting**
- The maximum line length is set to 120 characters
- Organize code structure and functions following the recommended folder structures for R packages.
