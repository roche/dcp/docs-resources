# Code of Conduct

- [Our Pledge](#our-pledge)
- [Our Standards](#our-standards)
- [Our Responsibilities](#our-responsibilities)
- [Attribution](#attribution)

## Our Pledge
We, the contributors and maintainers of the Data Computation Platform (DCP), pledge to foster an open and welcoming environment. We strive to make participation in our project a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

## Our Standards
- **Respectful Collaboration:** Embrace diverse perspectives and treat others with kindness and professionalism.
- **Constructive Feedback:** Offer and receive feedback graciously, focusing on improvement.
- **Zero Tolerance:** Harassment, disrespectful behavior, and disruption are not tolerated.

## Our Responsibilities
Maintainers will enforce these standards fairly and take appropriate action against unacceptable behavior.

## Attribution
This Code of Conduct is adapted from the [Contributor Covenant, version 2.0](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).