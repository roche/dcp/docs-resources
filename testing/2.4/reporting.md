# Reporting

- [Introduction](#introduction)
- [Create Report](#create-report)
- [Integration in Pipeline](#integration-in-pipeline)

## Introduction

In the realm of software testing, insightful and automatically generated reports are invaluable assets, offering a comprehensive view of executed test case steps. This section elucidates the power of our reporting system, detailing how it automatically generates comprehensive reports based on executed test cases.

## Create Report

To create a pdf and html report based on the automatically created report folder execute

```sh
node dcp_test createReport "path_to_json_folder"
```


## Integration in Pipeline
