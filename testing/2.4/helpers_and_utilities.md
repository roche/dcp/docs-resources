# Helpers & Utilities

- [Introduction](#introduction)
- [Module Specifics](#module-specifics)
- [Helpers Folder](#helpers-folder)
    - [Administration](#administration)
    - [API](#api)
    - [Audit Trail](#audit-trail)
    - [Core](#core)
    - [Download Baseline](#download-baseline)
    - [Downloads](#downloads)
    - [Notifications](#notifications)
    - [SQL](#sql)
    - [Token](#token)
    - [Uploads](#uploads)
- [Helpers Functions](#helpers-functions)
    - [Kendo Dropdown](#kendo-dropdown)
    - [Global Filter Selection](#global-filter-selection)
    - [Time Range Selection](#time-range-selection)
    - [Chart Interactions](#chart-interactions)
    - [Chart Data Manager](#chart-data-manager)
    - [Batch Grid Selection](#batch-grid-selection)
    - [Formula](#formula)
- [DCP Test Utilities](#dcp-test-utilities)
    - [Delete Report Files](#utilities-delete-report-files)
    - [Clear Files](#utilities-clear-files)


## Introduction


## Module Specifics


## Helpers Folder


### Administration

To avoid saving credentials in the code repository, the connection information which are used in the administration test cases are injected via variable from the pipeline.


### API

JSON file provides list of all expeced endpoints that should be available via DCP API ordered by module.


### Audit Trail

Collection of JSON files which will be used to compare audit trail downloads against.


### Core

Module agnostic files, which will be required during test execution.


### Download Baseline

For some test cases, downloads need to be checked (e.g. images or CSV files). The corresponding baselines are stored in this folder.


### Downloads

Download target folder. Files downloaded via browser will end up in this directory and remain as artifacts at the end of the pipeline.


### Notifications

The steps to be executed in the creation of notification via DCP are all the time the same (for the same notification type). Therefore, the steps are just defined in the corresponding notification type sub-folder, the expected mails are provided and the mail template is stored.


### SQL

Collection of SQL scripts to be exectued in the site cleaning step during the pipeline execution.


### Token

Kind of stored procedure to get access token of a user logged in at DCP. User for clearing the sites.


### Uploads

Storage for generic upload files.


## Helpers Functions

Some supporting functions should be highlighted to ensure smooth use and test development.


### Kendo Dropdown

Comparison between preloaded data and dropdown options in Kendo is simplified by using

```js
it('should see identical options in dropdown and from API call', () => {

    //Click in input to open kendo-popup
    $('kendo-textbox-container[ng-reflect-floating-label="Level 1"').click();

    //Check values
    env.kendoDropdown('compare', '{name_of_preload_value}').then((kendoCompare) => {
        expect(kendoCompare).toBe(true);
    });

    //Select value by presented name
    env.kendoDropdown('select', '{presented_option_string}');

});
```

### Global Filter Selection

Load global filter automatically. Property name needs to be the final dropdown description. Values can be string or string array, if array is provided, for reach element the selection will be performed on the corresponding level.

```js
it('should select sensors', function(){
    env.globalFilter({
        "Level 1": "Modul 1",
        "Level 2": "Autoklav (Belimed)",
        "Sensor group": "ProcessValue",
        "Sensor": [
            "Druck_P1.U",
            "Druck_P2.U"
        ]
    }); // Optional parameter to specify which global filter to use can be added {..., 1}. By default = 0, which is the first one or only one
});
```

### Time Range Selection

Perform a time range selection using the kendo element

```js
it('should select a time range', function(){
    env.dateRange("2020-05-01 10:15", "2020-05-06 24:00") // Optional parameter to specify which global filter to use. By default = 0, which is the first one or only one
    //Keep in mind to provide some browser.sleep to make sure that data load and reaction on change can be performed
});
```

### Chart Interactions

Interact with a plotly chart. It is possible to chain multiple interactions. Chart Mode is equal to the tooltip/description info presented. The drag function expects an array of x/y values representing the relative movemenent. The first element indicates the starting point (x:0, y:0 -> upper left corner of element)

```js
it('should interact with chart', function(){
    env.chartMode('Zoom').chartDrag([{x: 0, y: 0}, {x: 100, y: 100}]);
    browser.sleep(2000);
    env.chartMode('Pan').chartDrag([{x: 0, y: 0}, {x: 200, y: 0}]);
    browser.sleep(2000);
});
```

### Chart Data Manager

In multiple presentations, the user has the option to adjust the axis on which the data should be presented in a chart. The order of which the traces are presented in the chart are not directly linked to the order of which those are selected. Instead, the plotly chart legend order is equal to the order in the data manager. Therefore, the following function `getDataManagerPostion('legend_name')` can be used to find the correct dropdown/delete for a specific trace.

```js
it('should make changes in data manager, function(){
    $$('app-continuous-data-monitoring-manager div.dataset-manager div.row').get(env.getDataManagerPostion('Pressure')).$('mat-select').click();
    browser.sleep(env.kendoSleep);
    $('.cdk-overlay-pane').element(by.cssContainingText('mat-option','Left axis 2')).click();
    expect($$('app-continuous-data-monitoring-manager div.dataset-manager div.row').get(env.getDataManagerPostion('Pressure')).$('div.mat-select-value').getText()).toEqual('Left axis 2')
});
```

### Batch Grid Selection

Checking the checkbox in a batch grid or opening the menu in a batch grid.

```js
it('should select four batches', function(){
    env.batchGrid([
        '5306', '5307', '5308', '5311'
    ]) // additional parameters: action = 'select', target = 'app-batch-grid'
});

it('should open the menu of one batch grid entries', function(){
    env.batchGrid([
        '5308'
    ], {
        action: 'menu', //by default is select, which is then shown above
        item: 1, //index of menu element to be clicked
        subsequent: async function(){ //function is (more or less) a callback which will be called after clicking the menu element; it does not need to be async ... in this case just done to ...
            expect(await browser.imageComparison.checkScreen('loadingChart', {})).toEqual(0);	// ... make sure that the picture is taken
            await browser.sleep(1000);		
            await $('kendo-dialog-actions').element(by.buttonText('Close')).click() // ... before closing the overlay
        },
    })
});
```

### Formula

Formula is not a standard kendo element. Therefore, formula needs to be handled using the following functions

```js
it('should get all currently available formula options', function(){
    env.formulaEditor().then(options => { //formulaEditor called with default parameters: action = 'options', target = 'app-formula-builder input'
        console.log(options)
    })
});

it('should insert a formula', function(){
    env.formulaEditor({
        action: "insert",
        formula: [  //Array of elements that should be added, everything that is string will be used to click on the element, while numerical values will be added as number
            '(',
            'Druck_P1.U',
            '-',
            'Druck_P2.U',
            ')',
            '*',
            1000.1    
        ]
    })
});
```

## DCP Test Utilities

The `dcp_test.js` entry point provides some utilities which can be helpful while creating tests locally.

### Delete Report Files

To remove the recent created report (automatically generated report folder, pdf and html report) you can execute

```sh
node dcp_test deleteReport
```

### Clear Files

To remove the screenshots captured during the tests use. This will also remove the (local) baselines.

```sh
node dcp_test clear
```
