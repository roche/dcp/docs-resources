# Directory Structure

- [Introduction](#introduction)
- [Skeleton](#skeleton)
    - [Baseline Folder](#baseline-folder)
    - [Confs Folder](#confs-folder)
    - [Core Folder](#core-folder)
    - [Gmail Folder](#gmail-folder)
    - [Helpers Folder](#helpers-folder)
    - [Node Modules Folder](#node-modules-folder)
    - [PDF Report PNG Folder](#pdf-report-png-folder)
    - [Report Folder](#report-folder)
    - [Screenshots Folder](#screenshots-folder)
    - [Specs Folder](#specs-folder)
- [File Relations](#file-relations)
    - [Configuration Files](#configuration-file)
    - [Specification Files](#specification-file)


## Introduction

It is essential to understand the folder structure, to keep the project maintanable and also to make best use of the testing framework.

## Skeleton

In the subsequent section, we will discuss all essentail folder. But, the project is set up as follows:

```
dcp_test
└── baseline
└── confs
│   └── Framework
|   │   └── example1.js
│   |   └── example2.wido.js
│   └── Modules
|   │   └── example3.js
│   |   └── example4.wdio.js
└── core
│   └── dcp
│   └── protractor
│   └── wdio
└── gmail
│   └── client_secrets
│   └── tokens
│   └── functions.js
└── helpers
│   └── administration
│   └── api
│   └── download_baseline
│   └── notifications
│   └── sql
│   └── ...
└── node_modules
└── PdfReportPng
└── Report
└── screenshots
└── specs
│   └── Framework
│   │   └── main.js
│   │   └── two.js
│   └── Module
│       └── MVDA
│           └── main.wdio.js
│           └── two.wdio.js
└── config.js
└── credentials.js
└── dcp_test.js
└── environment.js
└── ...
```

> {info} It is recommended to follow with the folder structure the structure which is defined by the test script. The test scripts are provided in the [GMP Docs](https://gmp.open-dcp.ai).

### Baseline Folder

The `basline` folder contains all screenshots against which should be verified during the testing. Local and pipeline testing can show differences due to rendering differences. The local baseline images are not uploaded to the repository and get ingnored by git. The folder structure should follow the test script definition in [GMP Doc](https://gmp.open-dcp.ai).

> {info} On local execution it will be verified against local baseline images indicated by `...-local.png`.

### Confs Folder

The `confs` folder contains all configuration files. The folder structure should follow the test script definition in [GMP Docs](https://gmp.open-dcp.ai).

### Core Folder

The `core` folder contains utilities and supporting function for the following three core operations:
- `dcp`: Supporting test execution.
- `protractor`: System and utitlities related to Protractor testing.
- `wdio`: System and utitlities related to WebdriverIO testing.

### Gmail Folder

The `gmail` folder contains all required files to interact with Gmail, such as client secrets and tokens. Additionally, the `functions.js` implements the functions to query Gmail. Details about this are provided in [Notifiation Verification](/{{section}}/{{version}}/notification_verification).

> {info} Client secrets and tokens are ignored by git and will be injected via variable in the pipeline execution.

### Helpers Folder

The `helpers` folder contains a collection of multiple helper files and function to support the reusability of files and code. Additionally, it should be used as default downloads and uploads path, thus, it will be available as artifacts in pipeline executions.

The `download_baseline` folder should be used to store baselines for files downloaded during testing, such as rendered charts as image or CSV files.

> {info} The `tmp` directory, is ignored by git and will just be created if a function requires it.

### Node Modules Folder

The `node_modules` folder contains all external modules of the project installed via npm.

### PDF Report PNG Folder

The `PdfReportPng` folder will contain the generated report files in PNG format. However, this folder must not be present on first run and will be automatically created by the system.

> {info} The PNG files are getting created based on the generated PDF report.

### Report Folder

The `Report` folder contains all execution step report files created during test execution. To generate the report for each step, the `protractor-beautiful-reporter` provided by SauceLabs is used.

> {info} The plugin provides a HTML rendered report. However, for the final rendering we are interested in the step report files located in `Report/jsons` only.

### Screenshots Folder

The `screenshots` folder contains all acquired screenshots within the last runs. It automatically stores the actual screenshot and a difference between baseline and screenshot, if applicable. 

### Specs Folder

The `specs` folder contains all specification files. The folder structure should follow the test script definition in [GMP Docs](https://gmp.open-dcp.ai).

## File Relations

For each test, a configuration file needs to be created as `confs/{name_of_your_test}.js` and a correpsonding folder and specs-file(s) in `specs/{name_of_your_test}/{name_of_your_specs}.js`. A simple example of how to create a test is provided in [Getting Started - Write your first test](/{{section}}/{{version}}/getting_started#write-your-first-test).

The general relationship between configuration file and specification file is derived from Protractor and resulted in the directory structure described above. The same relationship is valid for WebdriverIO, which allowed for easy extension of the framework to also support WebdriverIO next to Protractor.

### Configuration File

The configuration file defines the general required information about a test case. Details for each section are provided below.

- **Selenium Server Address:** `seleniumAddress` - URL defining the entry point for the selenium server to be used.
- **Capablities:** `capabilities` - Required for example, to define the download path.
- **Framework:** `framework` - By default, the `jasmin` framework.
- **Specifiction Files:** `specs` - Array of paths to specification files to be executed.

> {success} The test cases will be executed in the order as those are defined in the `specs` array. This enable the "chaining" of test cases which are bundled within one configuration file.

- **Imported Files:** `importCode` - Array of paths to files which are required to execute the test case and should become part of the hash value.
- **Prepare Test:** `onPrepare` - Using default hook from Protractor to execute funtion prior starting test case execution. Typically the login is handled in this function.
- **On Completion:** `onComplete` - Clean up after test case execution, e.g. deleting temporary files.

> {info} The configuration file structure deviate slightly between WebdriverIO and Protractor. However, multiple supporting functions are available for both frameworks.

**Naming Convention**

The naming of configuration files should be as close as possible to the structure defined in [GMP Docs](https://gmp.open-dcp.ai). However, due the use of the commit message to trigger specific test cases or suites, the file name needs to be unique across all folders. To check the uniqueness of the filenames you can execute `node dcp_test updateEnum`. This funtion will throw errors if names are used twice.

### Specification File

The `specs` files define which steps should be executed and verified during the automated testing. Typically, the file skeleton looks as follows:

``` js
const env = require('./environment.js');

describe(env.describeFile(__filename) + ': ...', function() {

   it('should ', function(){
      
   });

});
```

The `env` file provides multiple [support functions](/{{section}}/{{version}}/helpers_and_utilities#helpers-functions) which make testing in DCP significantly easier.

The `describe` should always start with the DCP Test Case ID (e.g. DCP-DCP_TC-123) and ends with the title of the test case defined in [GMP Docs](https://gmp.open-dcp.ai).

The `it` blocks define the steps to execute.

> {info} We are not requiring that each test step in [GMP Docs](https://gmp.open-dcp.ai) for a given test cases results in an individual `it` block.

**Naming Convention**

Each `specs` file should be named with the DCP Test Case ID (e.g. DCP-DCP_TC-123) which it is representing.