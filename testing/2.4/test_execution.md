# Test Execution

- [Introduction](#introduction)
- [Local](#local)
- [Pipeline](#pipeline)
    - [Concept](#concept)
    - [Validation Packages](#validation-packages)
    - [Test Suites](#test-suites)
    - [Configuration Files](#configuration-files)

## Introduction

Efficient software testing, whether conducted locally or integrated within your pipeline, serves as the cornerstone of ensuring product quality. This section aims to provide insights and best practices for effective testing related to DCP.

## Local

After installation, protractor including its plugins will be installed. In a first step, you need to start the selenium server. The "dcp_test" is developed to act as easy-to-use entrence point and will be used to trigger all actions.

```sh
node dcp_test init
```

To start the selenium server on a different port (e.g. 8080) use

```sh
node node_modules/protractor/bin/webdriver-manager start --seleniumPort 8080
```

Test can be started using the dcp_test file. If the Test Case to be executed is a WDIO test then also `.wdio` need to bee added to the test file path.

```sh
node dcp_test run "path_your_test_file"  [-dev|stage|test]
```

## Pipeline

### Concept

To automate job creation in the CI/CD pipeline execute

```sh
node dcp_test generatePipeline
```
This function will loop through all folders in "confs" and check in each folder if a configuration file is present. If so, a new job will be added based on the job-template and stored in the "generate-pipeline.yml" file.

_Note:_ The mail catcher configuration is on purpose excluded from this file and needs to be added as individual step at the end of the pipeline.

### Validation Packages

Overview of available validation packages:

- |~|framework
- |~|framework_4_4_0
- |~|mvda

### Test Suites

 Overview of available collections:

- @actions (Framework/Core/Administration/Actions)
- @administration (Framework/Core/Administration)
- @api_service (Framework/Core/General/API Service)
- @audit_trail (Framework/Core/Administration/Audit Trail)
- @batch_cluster_analysis (Framework/Basic/Batch Cluster Analysis)
- @batch_data_studio (Framework/Basic/Batch Data Studio)
- @ccheck (Modules/CCheck)
- @chromta (Modules/ChromTA)
- @continuous_data_monitoring (Framework/Basic/Continuous Data Monitoring)
- @dashboard (Framework/Core/Dashboard)
- @demo (Modules/MVDA/Demo)
- @dream (Modules/DReAM)
- @framework_user_roles (Framework/Core/General/User Roles/Framework User Roles)
- @general (Framework/Core/General)
- @method_selection (Framework/Core/Administration/Actions/Method Selection)
- @model_manager (Modules/MVDA/Model Manager)
- @notifications (Framework/Core/Notifications)
- @pls (Modules/MVDA/Model Manager/Model Wizard/PLS)
- @reusable_components (Framework/Core/General/Reusable Components)
- @saw (Modules/SAW)

### Configuration Files

Overview of available configuration files:

- =>actions_general (Framework/Core/Administration/Actions)
- =>additional_dataset_creation (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>admin_actions (Framework/Core/Administration/Audit Trail)
- =>admin_api (Framework/Core/Administration)
- =>admin_overview (Framework/Core/Administration)
- =>admin_user_role (Framework/Core/General/User Roles/Framework User Roles)
- =>api_key (Framework/Core/General/API Service)
- =>api_overview (Framework/Core/General/API Service)
- =>api_service_global_admin (Framework/Core/General/API Service)
- =>audit_trail_general (Framework/Core/Administration/Audit Trail)
- =>axis_selection_dropdown (Framework/Core/General/Reusable Components)
- =>basic (Framework/Core/Administration/Audit Trail)
- =>basic_methods (Framework/Core/Administration/Actions/Method Selection)
- =>batch_conditions (Framework/Core/General/Reusable Components)
- =>batch_grid (Framework/Core/General/Reusable Components)
- =>bds_active_batches (Framework/Basic/Batch Data Studio)
- =>bds_csv_download (Framework/Basic/Batch Data Studio)
- =>bds_general (Framework/Basic/Batch Data Studio)
- =>calculated_data (Framework/Core/General/Reusable Components)
- =>carousel (Framework/Core/Dashboard)
- =>ccheck (Modules/CCheck)
- =>change_reason_confirmation (Framework/Core/General/Reusable Components)
- =>chart (Framework/Core/General/Reusable Components)
- =>chromta (Modules/ChromTA)
- =>compare_model_report (Modules/MVDA/Demo)
- =>configuration (Framework/Core/Administration/Audit Trail)
- =>create_edit_api_service (Framework/Core/General/API Service)
- =>create_model (Modules/MVDA/Demo)
- =>create_pre-selected_tab (Framework/Basic/Batch Data Studio)
- =>crud_scenarios (Framework/Core/Administration/Audit Trail)
- =>csv_download (Framework/Basic/Continuous Data Monitoring)
- =>dashboard_tab (Framework/Core/Dashboard)
- =>data_manager (Framework/Basic/Continuous Data Monitoring)
- =>dataset_creation (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>dataset_manager (Framework/Basic/Batch Data Studio)
- =>device_mapping (Modules/MVDA/Model Manager)
- =>dream (Modules/DReAM)
- =>duration_as_y-variable_selection (Framework/Basic/Batch Cluster Analysis)
- =>equation_batch (Framework/Core/Notifications)
- =>equation_continuous (Framework/Core/Notifications)
- =>error_handling (Framework/Core/General)
- =>favorites (Framework/Core/General)
- =>feedback (Framework/Core/General)
- =>formula_editor (Framework/Core/General/Reusable Components)
- =>frame (Framework/Core/General)
- =>general (Framework/Core/Notifications)
- =>global_filter (Framework/Core/General/Reusable Components)
- =>help (Framework/Core/General)
- =>historical_data (Framework/Basic/Continuous Data Monitoring)
- =>layout (Framework/Core/Dashboard)
- =>login (Framework/Core/General)
- =>logout (Framework/Core/General)
- =>mail_receiver (Framework/Core/Notifications)
- =>manual_input_-_y-variable_setup (Framework/Basic/Batch Cluster Analysis)
- =>model_configuration (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>model_setup (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>model_testing (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>multiple_values (Framework/Core/Dashboard)
- =>mvda_dataset_manager (Modules/MVDA/Model Manager/Model Wizard/PLS)
- =>mvda_model_review (Modules/MVDA/Model Manager)
- =>mvda_overview (Modules/MVDA/Model Manager)
- =>none_as_y-variable_setup (Framework/Basic/Batch Cluster Analysis)
- =>overview (Framework/Basic/Batch Cluster Analysis)
- =>pi_tag_-_y-variable_setup (Framework/Basic/Batch Cluster Analysis)
- =>platform_versions (Framework/Core/General)
- =>real_time (Framework/Basic/Continuous Data Monitoring)
- =>response_template (Framework/Core/General/API Service)
- =>saw (Modules/SAW)
- =>saw.wdio (Modules/SAW)
- =>security (Framework/Core/Administration/Audit Trail)
- =>share_link_popup (Framework/Core/General/Reusable Components)
- =>single_parameter (Framework/Core/Dashboard)
- =>single_parameter_batch (Framework/Core/Notifications)
- =>single_parameter_continuous (Framework/Core/Notifications)
- =>sites (Framework/Core/Administration)
- =>table_filtering (Framework/Core/General/Reusable Components)
- =>tile (Framework/Core/Dashboard)
- =>time_range_selection (Framework/Core/General/Reusable Components)
- =>time_zone (Framework/Core/General)
- =>univariate_analysis (Framework/Basic/Batch Data Studio)
- =>user_settings (Framework/Core/General)
- =>validation_status_indication (Framework/Core/General/Reusable Components)
- =>viewer_user_role (Framework/Core/General/User Roles/Framework User Roles)
