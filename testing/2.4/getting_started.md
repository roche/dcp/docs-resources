# Getting Started

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Installing](#installing)
- [Write your first test](#write-your-first-test)


## Introduction

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Those tests are integrated into the CI/CD pipeline of DCP and automatically run before each deployment. The automated tests are part of the official testing required for keeping the validation status of the tool.


## Prerequisites

Protractor and WebdriverIO are based on `nodejs`. Therefore, ensure that `nodejs`, `git` and Google Chrome Browser are installed on you machine. As part of the project, also Selenium Standalone will be installed.

> {info} Currently, we recommend to use the `nodejs` version **v18.8.0** for the test automation.


## Installing

The following step-by step-series of examples tells you how to get a development env running

**Clone repository to your local system**

Use git to clone the files to your local system.

```sh
git clone https://.../test.git
```

**Install node module package**

Use `nodejs` package manager `npm` to install the node modules.

```sh
npm install
```

Rename `credentials.js.example` to `credentials.js`, located in the root folder of the project on your local machine and insert/update the credentials.

If required, create a local copy (will not be pushed to repository) of the `config.js` file and rename it to `config_local.js` to fit your needs. In this `config_local.js` you can adjust multiple settings (e.g. default timeouts) which will be used in the tests. Typically, the environment selection can be provided as flag as last comment (e.g. -dev)

> {warning} Do not change the `config.js` file as well as the `environment.js` file, because those are contributing to the *hash* values used to track the integrity of the test cases. More details are provided in [Compliance](/{{section}}/{{version}}/compliance).

**Update Webdriver**

Download and install the required Chromedriver for your locally installed Google Chrome Browser version.

```sh
node dcp_test update
```

> {info} Details about managing the test case execution are provided in [Test Execution - Local](/{{section}}/{{version}}/test_execution#local). Keep in mind, that the test case execution is managed via commit message in the [pipeline](/{{section}}/{{version}}/test_execution#pipeline).


## Write your first test

To start the development of automated test cases, always at least two files are required - no matter if it will be a Protractor or WebdriverIO test case. In the following example we are creating a Protractor test case to verify that we can visit the Batch Data Studio after login.

> {info} Test Cases which should be executed with WDIO need to be named `specs/{name_of_your_test}/{name_of_your_specs}.wdio.js`.

**Creating the Confs File**

We are starting by creating a new file `Our New Tests.js` in the `confs` folder. Following the convetion, this will become a test case of the *Framework* and, therefore, we need to locate the file in

```
dcp_test
└── confs
    └── Framework
        └── Basic
            └── Batch Data Studio
                └── Our New Tests.js
```

In the `Our New Tests.js` file, we are adding the following content. *(You can also use the [VS Code Snippet](/{{section}}/{{version}}/vscode_snippets) `dcp>protractor>it`, if installed to inject the skeleton.)*

```js
    const env = require('../../../../environment.js'); // Ensure that you define the correct path to the environment file

    exports.config = {
        capabilities: {
            browserName: env.browser,
            chromeOptions: env.headlessReturn(env.headless())
        },
        jasmineNodeOpts: {
            defaultTimeoutInterval: env.jasmineTimeout
        },
        framework: env.framework,
        seleniumAddress: env.seleniumAddress(),
        specs: [
            '../../../../specs/Framework/Basic/Batch Data Studio/Our New Tests/DCP-DCP_TC-9999.js', // Name of TC
        ],
        importCode: [
            //
        ],
        onPrepare: async () => {
            browser.ignoreSynchronization = true;
            browser.driver.manage().window().maximize();
            global.EC = protractor.ExpectedConditions;

            jasmine.getEnv().addReporter(new env.HtmlReporter(env.ReporterSettings).getJasmine2Reporter()); // Adds reporter which creates json files in each step, to generate the pdf document at the end of the pipeline

            env.preloadInfo['importCode'] = module.exports.config.importCode

            return env.startUpProcedure('test_user'); // Adjust to the test user you need and as defined in the credentials.js file
        }
    };
```

**Creating the Specs File**

With this, we are set to create our test case steps in the specifications file. Therefore, we are creating the `DCP-DCP_TC-9999.js` file in the following directory:

```
dcp_test
└── specs
    └── Framework
        └── Basic
            └── Batch Data Studio
                └── Our New Tests
                    └── DCP-DCP_TC-9999.js
```

In the `DCP-DCP_TC-9999.js` file, we insert the following content: *(You can also use the [VS Code Snippet](/{{section}}/{{version}}/vscode_snippets) `dcp>protractor>spec`, if installed to inject the skeleton.)*

```js
    const env = require('../../../../../environment.js');

    describe(env.describeFile(__filename) + ': Test Case Description', function() {

        it('should navigate to Batch Data Studio', function(){
            let sidebarElements = element.all(by.css('.dc-sidebar_link'));        
            sidebarElements.get(3).click();   
            browser.wait(EC.elementToBeClickable(env.stdLoc('gfilter:first')), env.maxDcpLoad);

            expect($('h1.dc-content_title').getText()).toEqual('Batch Data Studio')
        });

    });
```