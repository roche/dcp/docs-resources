# Visual Studio Code Snippets

- [Introduction](#introduction)
- [Installation](#installation)
- [Using Snippets](#using-snippets)

## Introduction

Code snippets are templates that make it easier to enter repeating code patterns, such as loops or conditional-statements.

## Installation

To add the DCP Testing replated snippets, select **Configure User Snippets** under **File** > **Preferences**, and then select the language (by language identifier) for which the snippets should appear. The snippets can be found in the following folder:

```
dcp_test
└── helpers
    └── vsc_snippets
```

## Using Snippets

As soon as the snippets are added to the User Snippets, those can be called by starting to type `dcp>`. Based on the currently opened file, the options of the corresponding language are provided. A simple example is provided below.

By typing `dcp>protractor>specs` the standard template will be added and translates to the following code.

``` js
var env = require('./environment.js');

describe(env.describeFile(__filename) + ': ...', function() {

    it('should ...', function(){
        
    });

});
```
