# Notification Verification

- [Introduction](#introduction)
- [Gmail Setup](#gmail-setup)
- [Execute mail catcher](#execute-mail-catcher)

## Introduction

It is an essential part of the application to notify users on given events within DCP. Typically those notifications are sent via mail. By default the testing framework assumes that a Google Mail account is used and, therefore, the setup requires the configuration of tokens.

## Gmail Setup

According to the [Directory Structure - Gmail Folder](/{{section}}/{{version}}/directory_structure#gmail-folder), token will be stored in the following folders:

```
dcp_test
└── gmail
    └── client_secrets
    └── tokens
```

The specific client secrets and tokens will not be pushed to the repository and need to be injected in the pipeline via variables.

To receive a Gmail Account specific token, you first need to put the corresponding client secret into the `client_secrets` folder. The file needs to be named according to the user role (e.g. test_user_1) Afterwards, you can execute 

```sh
node dcp_mailcatcher
```

Because no match token for the user role should be found at the first execution, the system (command window), will guide you through the process of querying the token from Google.

## Execute mail catcher

The mail catcher should be executed after the other test were executed. This tool is intended to check on automated basis if all emails which should be send during the test are received. The tool is started with the following comment
```sh
node dcp_mailcatcher
```

It is based on the assumption that the email receiver inbox is a GMail account. Therefore, the credentials need to be provided in the "gmail" folder. In this folder the "client_secret.json" and "token.json" need to be stored.

During the execution, the mail catcher will dynamically generate specifications, based on the information provided coming from the mail account.
