# Configuration Interface
- [Introduction](#introduction)
- [Credential Cache](#credential-cache)

## Introduction
AC-DC supports multiple configuration sources. The following sources are known:

*   Vault: vault:::/path/to/secret e.g. `app-config/data/dev/db/core`
*   File: local:::/path/to/secret e.g. `app-config/data/dev/db/core` 

The configuration interface uses the argument *x* to perform a split by *"::"* characters, the first path is used for the S3 method dispatch pointing to the correct implementation based on the secret/configuration backend specified.

***Local files*** are stored in an encrypted format -- for encryption node dependent values are used as encryption keys. After decryption a valid JSON string is assumed.

Reading from ***HashiCorp Vault*** is performed using the HTTP API -- authentication is performed using TLS certificate. Vault configuration is relying on environment variables:

*   **VAULT_NAMESPACE** - the namespace in vault - you are using to secure your secrets
*   **CURL_SSL_BACKEND** - this needs to be hard coded to `openssl` in order to use certificates in curl - used to authenticate against vault
*  **ACDC_CA_PATH** - the path to the certificate used for authentication     in vault - make sure the TLS auth method is enabled and you are allowed to read the kv in vault

## Credential Cache
If secrets are withdrawn from a secret backed e.g. vault. Credentials can be cached on disk. AC-DC uses the user app directory as a cache location. The credential cache can be cleared using the `cleanCredentialCache()` function. The credential cache is re-using the method dispatch to local files internally. Caching is enabled by default - and can be overwritten using the useCache argument in the config call.