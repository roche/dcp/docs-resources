# Reports

- [Introduction](#introduction)

## Introduction
Reports inside DCP are generated on an AC-DC instance. Reports are written in the R markdown markup (Rmd) in order to allow data dependent report sections. Report files are included in the /inst/rmd/ package directory. Reports are rendered with parameters using [rmarkdown::render]() function. The parameter object is build on a calling function exposed to the backend. In order to render the report with security directory, report files are copied to the session working directory before calling the render command. Consistent style across the platform is achieved by starting from a common report template provided by the acdcReport package. R markdown generated md files. With [Pandoc]() shipped by the default AC-DC installation, conversion to different output formats (pdf, docx, tex, html) is possible. The output format is specified by the calling part. For non-interactive reports the output formats pdf and docx are used. Reports are saved to the disk in the DigitalClonePlatformData directory. The UNC filepath is stored in the database to link an object stored on the disk with a database recorded.

Report themes and templates are provided as pandoc templates. During installation the templates, are moved to the following directories:

*   LaTeX: `/usr/lib64/R/library/acdcReport/pandoc/dcp-template.tex`
*   Word: `/usr/lib64/R/library/acdcReport/pandoc/dcp-template.docx`

The included images and fonts, etc. are saved in the R package directory: `/usr/lib64/R/library/acdcReport/pandoc/dcp`. This directory is added to the LaTeX search directory by adding it to the auxtree.

The used LaTeX version is changed by adding the TeXLive2021 binaries to the path variable of the openCPU R environment (`/etc/opencpu/Renviron`)