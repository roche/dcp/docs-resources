# Introduction
- [Introduction](#introduction)
- [General description](#general-description)
- [Block View](#block-view)

## General description 
The system executes R functions, scripts or renders markdown documents based on incoming HTTP(S) requests. The result of the execution and the location to any objects/files/outputs are returned using HTTP(S).

The system is a generic component to execute custom/user defined R code over HTTP(S). During the execution AC-DC can interface to validated and not-validated systems. The interface to external systems needs to be defined and assess by the third party applications.

For authorization, the configuration file based method `htaccess` and `htpasswd` in-built in the apache webserver are used.

Requests are executed in a request session. In order to provide concurrent request handling using the single threatened language R, the apache module mod_multiprocessing is used. For every incoming request a preloaded R processes (including the preloaded packages in *server.conf*) is forked using unix based process forking. The R code is executed and evaluated in this request session. The working directory and results from the evaluation are written to the tmp directory of the hard drive. This process is outlined on high level in Figure 2.

Session can be used as input parameter for further requests until they are removed from the session cache. This way AC-DC is implementing functional state, requests can be chained. A mapping between the stateless request specification using REST and a state full R session is established.

Session clearance is performed using shell scripts. The shell scripts are scheduled using the linux in-built cron daemon.

## Block View 

The following diagram shows the block-view of AC-DC, the system
boundaries are indicated by the blue dotted lines:

```mermaid
title: The system boundaries of AC-DC
```

The following diagram is showing the blockview of the open source software openCPU:

```mermaid
title: The design and used process forking in openCPU
```