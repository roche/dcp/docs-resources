# Configuration
- [Introduction](#introduction)
- [Administration](#administration)
- [Global Settings](#global-settings)

## Introduction
The following section gives an overview of AC-DC / openCPU configuration. More details can be found in the [openCPU server manual](https://opencpu.github.io/server-manual/opencpu-server.pdf). For configuration of the apache webserver, see the [official documentation](https://httpd.apache.org/docs/). For configuration of the PostgreSQL database see the [official documentation](https://www.postgresql.org/docs/13/index.html).

## Administration
Administrative access requires root access to the AC-DC instance. The Access is managed by the third-party application utilizing AC-DC.

## Global Settings 

[The following configuration variables can]{.mark} be set in the *server.conf* file which is responsible for the openCPU server configuration (located at `/etc/opencpu/server.conf`):



| Variable Name      | File Location | Data Type | Description   |  Default Value |
|--------------------|---------------|-----------|---------------|----------------|
| enable.api.library | server.conf   |   Bool    | Enablement/Disablement of the library API  | true |     
| enable.api.apps  | server.conf   |   Bool    | Enablement/Disablement of the app API | true |    
| enable.api.tmp  | server.conf   |   Bool    | Enablement/Disablement of the user API | true |    
| Enable.cors     | server.conf   |   Bool    | If CORS Request are allowed or not | true | 
| Enable.post.code| server.conf   |   Bool    |    | true |  
| Enable.rlimits  | server.conf   |   Bool    |  If rlimits should be used to limit the request resources | true |  
| Error.showcall   | server.conf   |   Bool    |  If the debug message in case of an R error should be returned | true |  
| Smtp.server    | server.conf   |   String    |  The SMTP server used by openCPU | localhost |  
| Httpcache.post | server.conf   |   Integer    | The cache time of the apache webserver for POST requests | 300 |  
| Httpcache.libs | server.conf | Integer |  The cache time of the apache webserver for the library API  | 86400 |
| Httpcache.apps | server.conf | Integer | The cache time of the apache webserver for the apps API  | 900 |                                                  
| Httpcache.tmp  | server.conf | Integer | The cache time of the apache webserver for tmp API  | 86400 |                                                                 
| Httpcache.static  | server.conf | Integer | The cache time of the apache webserver for serving static content | 31536000 |  
| Key.length  | server.conf | Integer | The key length used for the sessionId   | 13 |  
| Rlimit.as   | server.conf | Integer | This is the maximum size of a process' total available memory, in bytes.| 4e9 |                   
| Rlimit.fsize   | server.conf | Integer | This is the maximum size  of a file, in bytes,  that may be created by a process. | 1e9 |  
| Rlimit.nproc   | server.conf | Integer | The maximum number of  processes (or, more precisely on Linux, threads) that can be created for the real user ID of the calling process. | 100 |  
| Timelimit.get    | server.conf | Integer | TThe time limit in seconds after which a HTTP GET request is terminated | 120 |  
| Timelimit.post    | server.conf | Integer | The time limit in seconds after which a HTTP POST request is terminated  | 180 |  
| Timelimit.webhook    | server.conf | Integer | The time limit in seconds after which a webhook is terminated  | 180 |         
| preload    | server.conf | string[] | The R packages which are preloaded in the processes, which is forked   | [lattice] |                

[The following configuration variables can]{.mark} be configured in the `cleanocpu.sh` script, located at `/usr/lib/opencpu/scripts/cleanocpu.sh` which is responsible for the cleaning the openCPU session cache:


| Variable Name | File Location | DataType | Description | Default Value |
|---------------|---------------|----------|-------------|---------------|
|`-m flag in rm --rf ocpu-store `|  Cleanocpu.sh | Int |  Session lifetime in ocpu-store (terminated  sessions) in minutes|  1440 |
|`-m flag in rm --rf ocpu-tmp `|  Cleanocpu.sh | Int |  Session lifetime in ocpu-stmp (currently active  sessions) in minutes|  60 |                                         


The following configuration variables can be configured in the openCPU crontab, located at `/etc/cron.d/opencpu` which is responsible for the cleaning the openCPU session cache:

| Variable Name | File Location | DataType | Description | Default Value |
|---------------|---------------|----------|-------------|---------------|
| mailto        | opencpu       |String    | Mail-address which is used for sending error messages | set during installation (user input) |
